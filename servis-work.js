
var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
 '/hola.html'
];

self.addEventListener('install', function(event) {
 event.waitUntil(
   caches.open(CACHE_NAME)
     .then(function(cache) {
       return cache.addAll(urlsToCache);
     })
 );  
});

self.addEventListener('activate', function(event) {
 console.log('Finally active. Ready to start serving content!');

});
self.addEventListener('fetch', function(event) {
 //event.respondWith(
   //caches.match(event.request)
     //.then(function(response) {
       // Cache hit - return response
       //if (response) {
         //return response;
   //    }
   //    return fetch(event.request);
    // }
 //  )
//  );
});

// self.addEventListener('push', function(event) {  
//   var title = 'Machu Picchu.';  
//   var body = 'Queremos enviarte ofertas.';  
//   var icon = '/images/icons/ico.png';  
//   var tag = 'simple-push-example-tag';
//   event.waitUntil(  
//     self.registration.showNotification(title, {  
//       body: body,  
//       icon: icon,  
//       tag: tag  
//     })  
//   );  
// });