




//Funcion para mostrar el itinerario de los tours
  let itinerario = document.querySelectorAll("#itinerary > .itinerary-details ol li h2");
  for (const dias of itinerario) {
   dias.addEventListener('click', function() {
     if(this.parentElement.classList.contains('active')){
       this.parentElement.classList.remove('active')
     }else{
        this.parentElement.classList.add("active");
     }
   })
  }

// include
//Funcion para mostrar el itinerario de los tours
   let includes = document.querySelectorAll("#included  ol  >li");
   for (const include of includes) {
    include.addEventListener('click', function() {
      if(this.classList.contains('active')){
        this.classList.remove('active')
      }else{
        
         this.classList.add("active");
      }
    })
   }


    $('#booking_people_field').on('click',function(){
      $('#pricing_category').css("display","block");
    });
    let ReservaGlobal ={
        pnn: [
          Nombre= " ",
          Cantidad = 0
        ],
        pjv : [
          Nombre = "",
          Cantidad = 0
        ],
        pad : [
          Nombre = "",
          Cantidad = 0
        ]
      };
      $('#pricing_category .category .people-selector-container .icon-minus').on('click',function(){
        let categoria = $(this).parent().attr("data-category");
        let categoryname = $(this).parent()[0].parentElement.childNodes[1].childNodes[1].innerHTML;
        if(ReservaGlobal[categoria][1] > 0){ReservaGlobal[categoria][1]--;}
        ReservaGlobal[categoria][0]=categoryname;
        if(categoria=="pad" && ReservaGlobal['pad'][1]==0 ){
          ReservaGlobal[categoria][1]=1;
        }
        $(this).parent().children().eq(1).attr("value",ReservaGlobal[categoria][1]);
        $(this).parent().children().eq(1).val(ReservaGlobal[categoria][1]);
        ImprimirCategorias();
      });

      $('#pricing_category .category .people-selector-container .icon-plus').on('click',function(){
        let categoria = $(this).parent().attr("data-category");
        let categoryname = $(this).parent()[0].parentElement.childNodes[1].childNodes[1].innerHTML;
        ReservaGlobal[categoria][1]++;
        ReservaGlobal[categoria][0]=categoryname;
        $(this).parent().children().eq(1).attr("value",ReservaGlobal[categoria][1]);
        $(this).parent().children().eq(1).val(ReservaGlobal[categoria][1]);
        ImprimirCategorias();
      });

      function ImprimirCategorias(){
          let texto="";
        if(ReservaGlobal['pad'][1]>0){
           texto+=" "+ReservaGlobal['pad'][0]+" X"+ReservaGlobal['pad'][1];
        };
        if(ReservaGlobal['pjv'][1]>0){
           texto+=", "+ReservaGlobal['pjv'][0]+" X"+ReservaGlobal['pjv'][1];
        };
        if(ReservaGlobal['pnn'][1]>0){
           texto+=", "+ReservaGlobal['pnn'][0]+" X"+ReservaGlobal['pnn'][1];
        };
        $('#booking_peoples').val(texto);
      }


// Creacion del Resivo Automatico
  let precioTotal = 0;
  $('#verify_avaible').on('click',function(){
    if(ReservaGlobal['pnn'][1]==0 && ReservaGlobal['pjv'][1]==0 && ReservaGlobal['pad'][1]==0){
      console.log("seleccione almenos 1 participante");
    }else{
      ImprimirpreVacuher();
    }

  });



 function ImprimirpreVacuher(){
   let precioAD = 0;
   let precioJV = 0;
   let precioNN = 0;
   let precioADI = 0;
   let precioJVI = 0;
   let precioNNI = 0;
   precioTotal = 0;
   let detalle ="";
   if(ReservaGlobal['pad'][1]>0){
     precioADI = document.getElementById('price_ad').value;
     precioAD = precioADI * (ReservaGlobal['pad'][1]);
     detalle +=`<div class="price-category">
       <span class="age-group">${ReservaGlobal['pad'][0]}&nbsp</span>
       <span class="price-calc">${ReservaGlobal['pad'][1]}&nbsp;x&nbsp;${precioADI}&nbsp;US$</span>
       <span class="price-category-total">${precioAD}&nbsp;US$</span>
     </div>`;
   }
   if(ReservaGlobal['pjv'][1]>0){
     precioJVI = document.getElementById('price_yu').value;
     precioJV =  precioJVI * (ReservaGlobal['pjv'][1]);
     detalle +=`<div class="price-category">
       <span class="age-group">${ReservaGlobal['pjv'][0]}&nbsp</span>
       <span class="price-calc">${ReservaGlobal['pjv'][1]}&nbsp;x&nbsp;${precioJVI}&nbsp;US$</span>
       <span class="price-category-total">${precioJV}&nbsp;US$</span>
     </div>`;
   }
   if(ReservaGlobal['pnn'][1]>0){
     precioNNI = document.getElementById('price_ch').value;
     precioNN = precioNNI * (ReservaGlobal['pnn'][1]);
     detalle +=`<div class="price-category">
       <span class="age-group">${ReservaGlobal['pnn'][0]}&nbsp</span>
       <span class="price-calc">${ReservaGlobal['pnn'][1]}&nbsp;x&nbsp;${precioNNI}&nbsp;US$</span>
       <span class="price-category-total">${precioNN}&nbsp;US$</span>
     </div>`;
   }

   //SUMATORIA DE TODO LOS PRECIOS

   precioTotal = (precioAD + precioNN) + precioJV;
   // precioTotal = Math.round(precioTotal);
   // CONCATENANDO PARA MOTRAR EL PRODUCTO



   let data = document.getElementById('bookin_data');
   data.style.display="block";
   data.innerHTML=`<div class="header_booking_data">
     <div class="item">
       <h4>${ document.querySelector('h1').innerHTML}</h4>
     </div>
     <div class="price-tag">
           <span class="label total">Total price</span>
           <span class="price">
               <span class="total-price">USD$ &nbsp;${precioTotal}</span>
           </span>
          
           
     </div>
     
  
   <div class="price_bokking_data 1fr_phone">
    

 
     </div>
     <div class="item" style="margin: 12px;
     color: #3159ba;
     font-size: 15px;    font-weight: 600;">
      *${detalle}
     </div>
   </div>

   <div class="add-to-cart">
     <button id="addtocar" onclick="guardar_datos();" class="icon icon-basket btn-add btn-add-to-cart">Add to cart</button>
     <button onclick="ircarrito();"class="icon icon-credit-card btn-add btn-checkout-now">Buy now</button>
   </div>`;
 }

//---------------- LLamando al carrito -------------//
let idtour = $('#idtour').val();

  function ircarrito(){
    let lang = $('html').attr('lang');
    guardar_datos();
    window.open("/"+lang+"/carrito");
  }
  function guardar_datos(){
        if(!obtener_tour(idtour)){
            let fecha=$('#booking_date').attr('data-format');
            if(!fecha==""){
                let canttext = $('#booking_peoples').val();
                let title = $('h1').text();
                let estorage={"ID":idtour,"TOURNAME":title,"PAX":ReservaGlobal,"FECHA_TRAVEL":fecha,"CANTEXT":canttext,"TOTALMOUT":precioTotal};
                Agregar_local(estorage);
                // Limbipar_Adicionales();
            }else{
                alert("Please indicate the date of the tour ");
            }
         }
  }

 
function obtener_tour(valor){
  
  if(productos){ 
    for(var i = 0; i < productos.length; i++){
        if(productos[i].ID == valor)
        return true;
    }
}
  return false;
  }

  function Agregar_local(val){
    productos.push(val);
    localStorage.setItem("mistours",JSON.stringify(productos));
    mostrarcantidadcarrito();
  }




