<?php

  namespace App\Controllers;
  use App\Models\BookingsModel;
  use App\Models\TourBookingsModel;
  use App\Models\TravelersBookingsModel;
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

    class BookingController extends BaseController{

      function NewBooking($request){
        $PostData = $request->getParsedBody();

        $codebooking = $this->AddBooking($PostData);
        $this->AddTour($PostData,$codebooking);
        $this->AddTraveler($PostData,$codebooking);
        $this->SendEmail($PostData,$codebooking);
        
      }

      function GetPdf($request){
        $data = $request->getParsedBody();
        $title="Eviar Informacio  -".$data['email'];
        $Subject='
          <div>
          <div style="background:#2c303a;height:50px;margin-bottom:10px;display:flex;align-items:center;font-weight:600;font-size:28px">
            <span style="display:block;color:white;margin:auto;">Informacion de los Toures<span>
          </div>
            <div>ENVIAR PFD INFOMACION A : '.$data['email'].'</div>
          <div style="background:#2c303a;margin-top:10px;color:white;padding:10px;text-align:center">
            <span>Opt More Discount Coupons</span> <a href="https://www.typicaltrips.com" style="text-decoration:none;color:aqua">Typical Trips</a>
          </div>
      </div>
        ';
        try {
          $mail = new PHPMailer(true);
          $mail->CharSet = 'UTF-8';
          $mail->SMTPDebug = 0;
          $mail->isSMTP();
          $mail->Host       = 'typicaltrips.com';
          $mail->SMTPAuth   = true;
          $mail->Username   = 'bookings@typicaltrips.com';
          $mail->Password   = EMAIL_PASSWORD;
          $mail->SMTPSecure = 'tls';
          $mail->Port       = 587;
          //RECEPTORES
          $mail->setFrom($data['email']);
          $mail->addAddress('bookings@tipicaltrips.com');
          //CONTENIDO
          $mail->isHTML(true);
          $mail->Subject = $title;
          $mail->Body = $Subject;

          $mail->send();
        } catch (Exception $e) {
          echo 'el mensaje no se pudo enviar', $mail->ErrorInfo;
        }
      }
      function QueryEmail($request){
        $data = $request->getParsedBody();
        $title="Request From Page  -".$data['title'];
        $Subject='
          <div>
          <div style="background:#2c303a;height:50px;margin-bottom:10px;display:flex;align-items:center;font-weight:600;font-size:28px">
            <span style="display:block;color:white;margin:auto;">'.$data['title'].'<span>
          </div>
            <div>'.$data['content'].'</div>
          <div style="background:#2c303a;margin-top:10px;color:white;padding:10px;text-align:center">
            <span>Opt More Discount Coupons</span> <a href="https://www.typicaltrips.com" style="text-decoration:none;color:aqua">Typical Trips</a>
          </div>
      </div>
        ';
        try {
          $mail = new PHPMailer(true);
          $mail->CharSet = 'UTF-8';
          $mail->SMTPDebug = 0;
          $mail->isSMTP();
          $mail->Host       = 'typicaltrips.com';
          $mail->SMTPAuth   = true;
          $mail->Username   = 'bookings@typicaltrips.com';
          $mail->Password   = EMAIL_PASSWORD;
          $mail->SMTPSecure = 'tls';
          $mail->Port       = 587;
          //RECEPTORES
          $mail->setFrom($data['email']);
          $mail->addAddress('bookings@typicaltrips.com');
          //CONTENIDO
          $mail->isHTML(true);
          $mail->Subject = $title;
          $mail->Body = $Subject;

          $mail->send();
        } catch (Exception $e) {
          echo 'el mensaje no se pudo enviar', $mail->ErrorInfo;
        }
      }


      function SendEmail($data,$code){
        $booking =BookingsModel::where('IdBooking',$code)->first();
        $tours = TourBookingsModel::join('tour','tour.IdTour','=','tourbookings.IdTour')->where('tourbookings.IdBooking',$code)->get();
        $travelers = TravelersBookingsModel::where('IdBooking',$code)->get();

        /* html tours for */
        $htmltours = "" ;
        $thmltravelers ="";
        $price = 0;
        $name = "";
        $statrdate ="";
        foreach($tours as $tour ){
          $name = $tour->MetaTitle; 
          $statrdate = $tour->TourStarDate;
          $price = $price + $tour->TourPrice;
          $htmltours.="<tr><td>$tour->MetaTitle</td><td>$tour->TourStarDate</td><td>$tour->CantPaxAd</td><td>$tour->CantPaxJv</td><td>$tour->CantPaxNn</td><td style='font-weight:900'>$ $tour->TourPrice</td></tr>";
        }
        foreach($travelers as $traveler){
          $thmltravelers.="$traveler->NameTraveler $traveler->LastNameTraveler, ";
        }
        $totalmonto= round(($price*6)/100) + floatval($price);
        $adicional = round(($price*6)/100);
        $title="Booking Confirmation  - $name  - $statrdate";
        $Subject='
        <div>
        <div style="background:#2c303a;height:50px;margin-bottom:10px;display:flex;align-items:center;font-weight:600;font-size:28px">
          <span style="display:block;color:white;margin:auto;"> Customer Receipt<span>
        </div>
        <div style="max-width:600px;margin:auto;width:100%">
        <h1 style="margin:auto">Typical trips</h1>
        </div>
          <div style="max-width:700px;margin:auto;width:100%;margin-top:20px;padding:10px">
          <div style="border-bottom:1px solid gray;display:flex">
             <div  style="padding:10px;display:flex;font-weight:900">TRAVELERS: </div>        
             <div class="item" style="padding:10px;text-transform: capitalize">
                '.$thmltravelers.'
            </div>
          </div>
          <table style="width:100%;margin-bottom:10px;color:gray;">
            <thead>
              <tr>
                <th>Tour</th>
                <th>Date</th>
                <th>Joven</th>      
                <th>Niño</th>
                <th>Adulto</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
               '.$htmltours.'
               <tr style="border-top:1px solid"><td  colspan = "5"> Tax (6%)</td><td style="font-weight:900">$ '.$adicional.'</td></tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan ="5">
                  <span style="font-weight:900">Total Monto</span>
                </td>
                <td style="font-weight:900">$ '.$totalmonto.'</td>
              </tr>
            </tfoot>
          </table>
                <a href="https://www.typicaltrips.com/print-booking/'.$code.'" style="padding: 20px 0px;display:block;text-align:center;background:#19bed3;margin-top:10px;font-weight:800;font-size:25px;text-decoration:none;color:white;border-radius:3px">Print Your Booking</a>
            </div>
        </div>
        <div style="background:#2c303a;margin-top:10px;color:white;padding:10px;text-align:center">
          <span>Opt More Discount Coupons</span> <a href="https://www.typicaltrips.com" style="text-decoration:none;color:aqua">Typical Trips</a>
        </div>
      </div>
        ';
          try {
            $mail = new PHPMailer(true);
            $mail->CharSet = 'UTF-8';
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host       = 'typicaltrips.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'bookings@typicaltrips.com';
            $mail->Password   = EMAIL_PASSWORD;
            $mail->SMTPSecure = 'tls';
            $mail->Port       = 587;
            //RECEPTORES
            $mail->setFrom('bookings@typicaltrips.com');
            $mail->addAddress($data['emailbooking']);
            //CONTENIDO
            $mail->isHTML(true);
            $mail->Subject = $title;
            $mail->Body = $Subject;

            $mail->send();
          } catch (Exception $e) {
            echo 'el mensaje no se pudo enviar', $mail->ErrorInfo;
          }
      }
      
      function AddBooking($data){
        $booking = new BookingsModel;
        $code = $this->NewCode('TP-');
        
        $booking->IdBooking = $code;
        $booking->NameBooking = $data['namebooking'];
        $booking->EmailBooking = $data['emailbooking'];
        $booking->PriceBooking = $data['pricebooking'];
        $booking->HotelBooking = $data['hotelbooking'];
        $booking->RequestBooking = $data['requestbooking'];
        $booking->PayKey = $data['payeridpaypal'];
        $booking->PayMethod = $data['paymethod'];
        $booking->save();
        return $code;
      }

      function AddTour($data,$codebooking){

        foreach($data['tourid'] as $key){ $tourid[]= $key; }
        foreach($data['PAXAD'] as $key){ $paxad[]= $key; }
        foreach($data['PAXJV'] as $key){ $paxjv[]= $key; }
        foreach($data['PAXNN'] as $key){ $paxnn[]= $key; }
        foreach($data['traveldate'] as $key){ $traveldate[]= $key; }
        foreach($data['tourprice'] as $key){ $tourprice[]= $key; }
            
        for($k = 0; $k < count($data['tourid']); $k++){
           $tour = new TourBookingsModel;
           $code = $this->NewCode('TB-');

           $tour->IdTourBooking = $code;
           $tour->IdBooking = $codebooking;
           $tour->IdTour = $tourid[$k];
           $tour->CantPaxAd = $paxad[$k];
           $tour->CantPaxJv = $paxjv[$k];
           $tour->CantPaxNn = $paxnn[$k];
           $tour->TourStarDate = $traveldate[$k];
           $tour->TourPrice = $tourprice[$k];
           $tour->save();

        }
        
      }

      function AddTraveler($data,$codebooking){

        foreach($data['nametraveler'] as $key){ $nametraveler[]= $key; }
        foreach($data['lastnametraveler'] as $key){ $lastnametraveler[]= $key; }
        foreach($data['agetraveler'] as $key){ $agetraveler[]= $key; }
        foreach($data['countrytraveler'] as $key){ $countrytraveler[]= $key; }
        foreach($data['passaporttraveler'] as $key){ $passaporttraveler[]= $key; }

        for($t = 0 ; $t < count($data['nametraveler']) ; $t++){
          $traveler = new TravelersBookingsModel;
          $code = $this->NewCode('TV-');

          $traveler->IdTraveler = $code;
          $traveler->NameTraveler = $nametraveler[$t];
          $traveler->LastNameTraveler = $lastnametraveler[$t];
          $traveler->AgeTraveler = $agetraveler[$t];
          $traveler->CountryTraveler = $countrytraveler[$t];
          $traveler->PassaportTraveler = $passaporttraveler[$t];
          $traveler->IdBooking = $codebooking;
          $traveler->save();
        }
        
      }

      function NewCode($prefix){
        $codeAleart = "123QWERTYUIOPA456SDFGHJKLZXCVBNM789";
        $codeInit = $prefix;
        $longitud=8;
        for($i=0;$i<$longitud;$i++){
            $codeInit.= $codeAleart[rand(0, strlen($codeAleart)-1)];
        }
        return $codeInit;
      }
    }
 ?>
