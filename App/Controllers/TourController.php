<?php
  namespace App\Controllers;
  use App\Models\TourPriceModel;
  use App\Models\TourLang;
  use App\Models\TourModel;
  use App\Models\TourCategory;
  use App\Models\TourGallery;
  use App\Models\TourItinerary;
  use App\Models\TourDayItinerary;
  use App\Models\TourIncluded;
  use App\Models\TourNoIncluded;
  use App\Models\TourFaq;
  use App\Models\TourExtra;
  use App\Models\TourRecomendation;
  use App\Models\TourRecomendationList;
  use App\Models\TourAssocCategory;
  use App\Models\TourAssociations;
  use App\Models\SliderHomeModel;


  class TourController extends BaseController{

    public function GetIndex(){
         $data =[
          'title_d'=>'Popular destinations among tourists',
          'title_c'=>'Our Categories',
          'title_cT'=>'Traditionals',
          'title_cA'=>'Adventures',
          'title_cH'=>'Hikings',
          'title_cJ'=>'Jungle',
          'link_c'=>'adventure-tours',
          'link_t'=>'traditional-tours',
          'link_h'=>'hiking-tours',
          'link_a'=>'Arequipa-adventure',
          'link_cu'=>'tours-cusco',
          'link_m'=>'Madre-de-dios',
          'more'=>'out more',
          'link_j'=>'adventure-tours',
          'subtitle_d'=>'Discover unique experiences in the most visited places',
          'duration'=>'Duration',
          'from'=>'From',
          'destinations'=>'Destinations',
          'start_end'=>'Starts/ Ends in',
          'agerange'=>'Age Range',
          'transport'=>'Transport',
          'brochure'=>'Downloand',
          'viewtour'=>'View Trip',
          'find_activities'=>'Find Activities',
          'title_Ban'=>'Enjoy the wonders of nature,',
              'title_Ban2'=>'Book activities and attractions.',
              'title_Sub'=>'AGet new memories',
              'title_Sub2'=>'with amazingly impressive attractions.',
              'Categoria_text'=>'100% Customized Travel',
              'Categoria_text2'=>'Local Expert Guides',
              'Categoria_text3'=>'Travel Without Complications',
              'Categoria_text4'=>'SMALL HOMOGENOUS GROUPS',
              'categoria_parr'=>'We design specially thought itineraries for you to live unforgettable moments. Our goal is to offer you memories that you will keep for a lifetime.',
              'categoria_parr2'=>'Enjoy the support of our Spanish-speaking guides, who will not only teach you the best routes and interesting information, but will always take your safety into account.',
              'categoria_parr3'=>'Planning the trip? Transfer? Accommodation? Forget it, we take care of everything !! Grab your backpack and come with us on the adventure of your life.',
              'categoria_parr4'=>'The trips in small groups allow to live more authentic experiences and ensure a greater coexistence and exchange between the participants.',
              'Regiones'=>'Our Destinations',
          'more_information'=>'More Information',
          'title_duration' =>'Duration',
          'title_region' =>'Our Destinations',
          'Price_de'=>'From',
              'Price_For'=>'Per person',
              'desitntex'=>'Popular destination',
              'subimg'=>'Best price',
              'subperu'=>'The best destination in South America, It has a natural beauty, remarkable archaeological wonders, such as Machu Picchu, gastronomic delights, and a vast ancient culture, there is also a variety of activities and places to be amazed in Peru.',
              'subperu2'=>'You will find the "White City", it has a historic center for the architecture of colonial buildings in white ashlar, highlighting its large houses, churches, temples and monasteries.',
              'subperu3'=>'Also the Great "Capital of the Incas", It has a great history and great archaeological centers left by the Incas like the wonderful Machu Picchu, graceful graceful like Salkantay, La Laguna Humantay and many more.  ',
              'expind'=>'UNFORGETTABLE EXPERIENCES AND AUTHENTIC TRAVEL ',
              'expparr'=>'We work with carefully selected local experts to create unique routes and experiences for you. We go further and show you the most authentic of the country of your choice.             ',
              'whyabout'=>'WHY WE?',
              'whyaboutp'=>'Typical Trips is about you, so that you know the customs, so that you overcome prejudices and your physical and mental limits, living an experience as unique as it is unforgettable.             ',
             'tinews'=>'Subscribe to our newsletter            ',
             'tinews2'=>'Do you want to be the first to know about our promotions? Subscribe to receive discounts and discover new destinations            ',
             'sub'=>'to subscribe',
             'nombresstart'=>'Leonor Nevado',
             'nombresstart2'=>'Elvira Zafra',
             'nombresstart3'=>'Oliver Mejias',
             'nombresstart4'=>'Adriana Prat',
             'nombresstart5'=>'Gorka Barea',
             'nombresstart6'=>'Rayan Sanz',
             'destinostart'=>'Machu Picchu',
             'destinostart2'=>'mountain of colors',
             'destinostart3'=>'sacred Valley',
             'destinostart4'=>'humantay lagoon',
             'destinostart5'=>'Salkantay trek',
             'destinostart6'=>'Inca trail',
             'opinionesstart'=>'It was one of the most spectacular visits I have made so far on my vacation. The place has a different aura and it is beautiful to see how they care to keep it standing and well cared for. I congratulate the Peruvian authorities for making it possible. However, they have to make it a little more accessible because it is so expensive. Beyond that, the place and the small town of Aguas Calientes is very quiet and beautiful. I enjoyed it very much.',
             'opinionesstart2'=>'It is a not so hard trek, but without a doubt it is impressive from the beginning the landscape leaves you speechless. The top is without equal to see how the mountain has different shades is wonderful. This tour I recommend InfoCusco.',
             'opinionesstart3'=>'Throughout our stay we were kindly assisted. Very professional guides. Very well planned excursions. They look for the strategic places to start the hikes.
             Delicious food Very flexible in the menus. You have the possibility to taste varied dishes.
             The architecture of the place is minimalist. Nothing is missing or left over',
             'opinionesstart4'=>'What beauty to find after a walk of an hour and a half such a beautiful lagoon At the foot of a glacier of a snow-capped mountain with wonderful colors it is worth the walk it is more beautiful than many other lagoons that I have visited Do not stop seeing it Typical Trips took us He also took us to the mountain 7 colors very good service I recommend it',
             'opinionesstart5'=>'Super recommended, from the day before in the meeting with our guide Leo Rodríguez, who explained the whole trek step by step.
             The experience was magnificent, I enjoyed it a lot, I met great people and our guide Leo was the wave. Always aware of our needs, he helped us to enjoy the experience, lots of laughs with the whole group.',
             'opinionesstart6'=>'The trip to Peru is spectacular, for its variety in the landscape and the possibility of enjoying its fauna. Great food. The people are very friendly and the guides are especially attentive to our needs and are very affectionate. A highly recommended trip ...',
             'imgstart1'=>'https://i.postimg.cc/3RZZngKN/machupicchu-per.jpg',
             'imgstart2'=>'https://i.postimg.cc/25YmPZph/monta-a-colores-peru.jpg',
             'imgstart3'=>'https://i.postimg.cc/Sx3Y8VMq/Valle-Sagrado.jpg',
             'imgstart4'=>'https://i.postimg.cc/tCN8wn40/lague.jpg',
             'imgstart5'=>'https://i.postimg.cc/wjHGzcvq/salkantay-trek-machu-picchu.jpg',
             'imgstart6'=>'https://i.postimg.cc/gkjC5Knf/salkantay-trek.jpg',   
             'titlecov'=>'Everything ready to travel | Covid 19',     
             'subcov'=>'Typical Trips welcomes the protection measures to give you security in the development of the tour ', 
             'subcov1'=>'All our excursions will be done in small groups for your safety.',
             'Information'=>'More information.'
          ];

         $Lang = TourLang::where('CodLang','en')->first();
         $BestTour = [];
         $slider= SliderHomeModel::where('IdLang',2)->get();
         $Category = TourCategory::join('lang','lang.IdLang','=','category.IdLang')->where('lang.IdLang',$Lang['IdLang'])->get();
         foreach($Category as $key) {
           $Tour_Category = TourModel::where('IdCategory',$key['IdCategory'])->take(12)->get();
              $Info_Category = [];
             foreach($Tour_Category as $val ){
                $preciotour = TourPriceModel::join('pricecategory','tourprices.IdPriceCategory','=','pricecategory.IdPriceCategory')
                ->where('tourprices.IdTour',$val['IdTour'])->orderby('tourprices.PriceMount','DESC')->first();
                $tour = [$preciotour,$val];
                array_push($Info_Category,$tour);
             }
           $aux = [$key,$Info_Category];
           array_push($BestTour,$aux);
         }
        //meta
        $meta =[
          'title'=>$Lang['TitleLang'],
          'url'=>'en',
          'description'=>$Lang['MetaDescription'],
          'prefix'=>'en_GB',
          'img'=>$Lang['bannerImg']

        ];

          return $this->renderHTML('inicio.twig',[
            'code'=>$Lang['CodLang'],
            'language'=>$Lang,
            'tourhome'=>$BestTour,
            'association'=>'es',
            'data'=>$data,
            'meta'=>$meta,
            'tag'=>['Discovery','Adventures','Nature'],
            'sliderhome'=>$slider
          ]);
    }

    public function GetLanguageIndex($request,$atributes){
         $data=[
           'es'=>[

            
             'title_d'=>'Destinos Populares en entre los turistas', 
             'title_duration'=>'Duración',
             'more'=>'saber mas',
             'title_c'=>'Opiniones',
             'title_cT'=>'Tradicionales',
             'title_cA'=>'Aventura',
             'title_cH'=>'Caminatas',
             'title_cJ'=>'Selva',    
              'link_c'=>'tours-de-aventuras',
              'link_t'=>'tours-tradicionales',
              'link_h'=>'tours-de-caminatas',
              'link_j'=>'tours-de-aventuras',   
              'link_a'=>'tours-arequipa',
              'link_cu'=>'cusco-tours',
              'link_m'=>'Madre-de-dios-es',  
             'subtitle_d'=>'Descubre experiencias únicas en los lugares más visitados',
             'duration'=>'Duracion',
             'from'=>'Desde',
             'title_Ban'=>'Disfruta de las maravillas de la naturaleza ,',
             'title_Ban2'=>'Reserva actividades y atracciones.',
             'title_Sub'=>'Adquiere nuevos recuerdos',
             'title_Sub2'=>'con asombrosos atractivos impresionantes.',
             'Categoria_text'=>'100% Viajes Personalizados',
             'Categoria_text2'=>'Guias Expertos Locales',
             'Categoria_text3'=>'Viajes Sin Complicaciones',
             'Categoria_text4'=>'Informacion las 24 horas del dia',
             'Regiones'=>'Nuestros Destinos',
             'destinations'=>'Destinos',
             'start_end'=>'Empieza/ Termina el recorrido',
             'agerange'=>'Rango de Edad',
             'transport'=>'Transporte',
             'brochure'=>'Descargar',
             'viewtour'=>'Ver Tour',
             'find_activities'=>'Encontrar Actividades',
            'title_region'=>'Nuestros Destinos',
             'more_information'=>'Mas Informacion',
             
             'tinews'=>'Suscríbete a nuestra newsletter',
             'tinews2'=>'¿Quieres ser el primero en conocer nuestras promociones? Suscríbete para recibir descuentos y descubrir nuevos destinos             ',
             'sub'=>'suscribirse',
              'Price_For'=>'Por persona',
              'desitntex'=>'Destino popular',
              'subimg'=>'Mejor precio',
              'Price_de'=>'Desde',
              'expind'=>'EXPERIENCIAS INOLVIDABLES Y VIAJES AUTÉNTICOS',
              'expparr'=>'Trabajamos con expertos locales cuidadosamente seleccionados para crear rutas y experiencias únicas para ti. Vamos más allá y te mostramos lo más auténtico del país que elijas.              ',
              'whyabout'=>'¿POR QUÉ NOSOTROS?',
              'whyaboutp'=>'Typicaltrips es sobre ti, para que conozcas las costumbres, para que superes los prejuicios y tus límites físicos y mentales, viviendo una experiencia tan única como inolvidable. ',
              'subperu'=>'El mejor destino de Sudamerica, Cuenta con una belleza natural , notables maravillas arqueológicas, como Machu Picchu, delicias gastronómicas, y un vasto cultura antigua, existe también variedad de actividades y lugares para quedar asombrado en peru.',
              'subperu2'=>'Encontraras la "Ciudad Blanca", cuenta con un centro histórico por la arquitectura de construcciones coloniales en sillar blanco, destacando sus casonas, iglesias, templos y monasterios.',
              'subperu3'=>'Tambien la Grandiosa "Capital de los Incas", Cuenta con una gran historia y grandes centro arqueologicos que dejaron los incas Como el maravilloso Machu Picchu , graciares enormes Como Salkantay , La laguna Humantay y muchos mas.',
              'nombresstart'=>'Napoleon Arcondo Saico',
             'nombresstart2'=>'Martha Quiroga',
             'nombresstart3'=>'Javier Bonet',
             'nombresstart4'=>'Julia Aguilar',
             'nombresstart5'=>'Marisa Andujar',
             'nombresstart6'=>'Alejandro Caceres',
             'destinostart'=>'Salineras Maras',
             'destinostart2'=>'Laguna Humantay',
             'destinostart3'=>'Valle Sagrado de los Incas',
             'destinostart4'=>'Machu Picchu',
             'destinostart5'=>'Cañon Del Colca',
             'destinostart6'=>'Salkantay',
             'opinionesstart'=>'Es uno de los lugares mas increibles en los que he estado. El guia nos explico de la operacion de la sal estuvo muy interesante . Recomendado',
             'opinionesstart2'=>'Contratamos varios tours con TypicalTrips para no estar desprevenidos. El recuerdo mas hermoso fue de la laguna Humantay ,  la laguna mas linda de esta travesia en Peru',
             'opinionesstart3'=>'Experiencia inolvidable, Disfrutamos como niños explorando todas las ruinas , Buen servicio , tranquilo y rodeado de una hermosa naturaleza. ¡Recomendado!',
             'opinionesstart4'=>'Recomiendo bastante Machu Picchu. La experiencia es maravillosa , ademas si cierras los ojos puedes trasladarte a los tiempos antiguos donde aun vivian los incas. Increible lugar',
             'opinionesstart5'=>'El Gran Cañón del colca es una de los lugares más impresionantes, las vistas son increíbles y los colores, sobre todo el cañón . el vuelo del condor mas magestuoso.',
             'opinionesstart6'=>'Ha sido una de las mejores experiencias de mi vida, El trekking es el mejor que hice hasta ahora. siempre atentos a nuestras necesidades... vamos, un disfrute total! Mil gracias a todos los que formáis este equipo',    
             'imgstart1'=>'https://i.postimg.cc/hj9qGm2F/Captura.png',
             'imgstart2'=>'https://i.postimg.cc/vBxpNPcX/laguna-humantay-cusipata.jpg',
             'imgstart3'=>'https://i.postimg.cc/Sx3Y8VMq/Valle-Sagrado.jpg',
             'imgstart4'=>'https://i.postimg.cc/C1R5d0zN/viaje-a-machupicchu.jpg',
             'imgstart5'=>'https://i.postimg.cc/v8q0VBkC/china-machu-pichu-peru-turismo-exterior1-737x413.jpg',
             'imgstart6'=>'https://i.postimg.cc/N0CJR3xF/trail-inca.jpg',  
             'titlecov'=>'Todo listo para viajar | Covid 19',     
             'subcov'=>'Typical Trips acoge las medidas de proteccion para brindarle seguridad en el desarrollo del tour', 
             'subcov1'=>'Todas nuestras escursiones se realizaran en grupos pequeños por su seguridad.'     ,
             'Information'=>'Más Información.'
            ],
           'en'=>[
            'nombresstart'=>'Leonor Nevado',
            'nombresstart2'=>'Elvira Zafra',
            'nombresstart3'=>'Oliver Mejias',
            'nombresstart4'=>'Adriana Prat',
            'nombresstart5'=>'Gorka Barea',
            'nombresstart6'=>'Rayan Sanz',
            'destinostart'=>'Machu Picchu',
            'destinostart2'=>'mountain of colors',
            'destinostart3'=>'sacred Valley',
            'destinostart4'=>'humantay lagoon',
            'destinostart5'=>'Salkantay trek',
            'destinostart6'=>'Inca trail',
            'opinionesstart'=>'It was one of the most spectacular visits I have made so far on my vacation. The place has a different aura and it is beautiful to see how they care to keep it standing and well cared for. I congratulate the Peruvian authorities for making it possible. However, they have to make it a little more accessible because it is so expensive. Beyond that, the place and the small town of Aguas Calientes is very quiet and beautiful. I enjoyed it very much.',
            'opinionesstart2'=>'It is a not so hard trek, but without a doubt it is impressive from the beginning the landscape leaves you speechless. The top is without equal to see how the mountain has different shades is wonderful. This tour I recommend InfoCusco.',
            'opinionesstart3'=>'Throughout our stay we were kindly assisted. Very professional guides. Very well planned excursions. They look for the strategic places to start the hikes.
            Delicious food Very flexible in the menus. You have the possibility to taste varied dishes.
            The architecture of the place is minimalist. Nothing is missing or left over',
            'opinionesstart4'=>'What beauty to find after a walk of an hour and a half such a beautiful lagoon At the foot of a glacier of a snow-capped mountain with wonderful colors it is worth the walk it is more beautiful than many other lagoons that I have visited Do not stop seeing it Typical Trips took us He also took us to the mountain 7 colors very good service I recommend it',
            'opinionesstart5'=>'Super recommended, from the day before in the meeting with our guide Leo Rodríguez, who explained the whole trek step by step.
            The experience was magnificent, I enjoyed it a lot, I met great people and our guide Leo was the wave. Always aware of our needs, he helped us to enjoy the experience, lots of laughs with the whole group.',
            'opinionesstart6'=>'The trip to Peru is spectacular, for its variety in the landscape and the possibility of enjoying its fauna. Great food. The people are very friendly and the guides are especially attentive to our needs and are very affectionate. A highly recommended trip ...',
             'expind'=>'UNFORGETTABLE EXPERIENCES AND AUTHENTIC TRAVEL ',
             'expparr'=>'We work with carefully selected local experts to create unique routes and experiences for you. We go further and show you the most authentic of the country of your choice.             ',
             'whyabout'=>'WHY WE?',
             'whyaboutp'=>'Typical Trips is about you, so that you know the customs, so that you overcome prejudices and your physical and mental limits, living an experience as unique as it is unforgettable.             ',
            'tinews'=>'Subscribe to our newsletter            ',
            'tinews2'=>'Do you want to be the first to know about our promotions? Subscribe to receive discounts and discover new destinations            ',
            'sub'=>'to subscribe',    
            'more'=>'out more',
              'title_d'=>'Popular destinations among tourists',
              'title_c'=>'Feedback',
              'title_cT'=>'Traditionals',
              'title_cA'=>'Adventures',
              'title_cH'=>'Hikings',
              'title_cJ'=>'Jungle',
              'link_c'=>'adventure-tours',
              'link_t'=>'traditional-tours',
              'link_h'=>'hiking-tours',
              'link_j'=>'adventure-tours',
              'link_a'=>'Arequipa-adventure',
              'link_cu'=>'tours-cusco',
              'link_m'=>'Madre-de-dios',
              'subtitle_d'=>'Discover unique experiences in the most visited places',
              'duration'=>'Duration',
              'from'=>'From',
              'title_Ban'=>'Enjoy the wonders of nature,',
              'title_Ban2'=>'Book activities and attractions.',
              'title_Sub'=>'AGet new memories',
              'title_Sub2'=>'with amazingly impressive attractions.',
              'Categoria_text'=>'100% CUSTOMIZED TRAVEL',
              'Categoria_text2'=>'LOCAL EXPERT GUIDES',
              'Categoria_text3'=>'TRAVEL WITHOUT COMPLICATIONS',
              'Categoria_text4'=>'Attention 24 hours a day',
              'Regiones'=>'Our Destinations',
              'more'=>'out more',
              'destinations'=>'Destinations',
              'start_end'=>'Starts/ Ends in',
              'agerange'=>'Age Range',
              'transport'=>'Transport',
              'brochure'=>'Downloand ',
              'viewtour'=>'View Trip',
              'find_activities'=>'Find Activities',
              'title_duration'=>'Duration',
              'title_region'=>'Our Destinations',
              'more_information'=>'More Information',
              'Price_de'=>'From',
              'Price_For'=>'Per person',
              'desitntex'=>'Popular destination',
              'subimg'=>'Best price',
              'subperu'=>'The best destination in South America, It has a natural beauty, remarkable archaeological wonders, such as Machu Picchu, gastronomic delights, and a vast ancient culture, there is also a variety of activities and places to be amazed in Peru.',
              'subperu2'=>'You will find the "White City", it has a historic center for the architecture of colonial buildings in white ashlar, highlighting its large houses, churches, temples and monasteries.',
              'subperu3'=>'Also the Great "Capital of the Incas", It has a great history and great archaeological centers left by the Incas like the wonderful Machu Picchu, graceful graceful like Salkantay, La Laguna Humantay and many more.',
              'imgstart1'=>'https://i.postimg.cc/3RZZngKN/machupicchu-per.jpg',
             'imgstart2'=>'https://i.postimg.cc/25YmPZph/monta-a-colores-peru.jpg',
             'imgstart3'=>'https://i.postimg.cc/Sx3Y8VMq/Valle-Sagrado.jpg',
             'imgstart4'=>'https://i.postimg.cc/tCN8wn40/lague.jpg',
             'imgstart5'=>'https://i.postimg.cc/wjHGzcvq/salkantay-trek-machu-picchu.jpg',
             'imgstart6'=>'https://i.postimg.cc/gkjC5Knf/salkantay-trek.jpg',
             'titlecov'=>'Everything ready to travel | Covid 19',     
             'subcov'=>'Typical Trips welcomes the protection measures to give you security in the development of the tour ', 
             'subcov1'=>'All our excursions will be done in small groups for your safety.',
             'Information'=>'More information.'
              ]
         ];

         $metatag =[
           'es'=>['','Descubrimiento','Aventura','Naturaleza'

           ],
           'en'=>['','Discovery','Adventure','Nature'

           ]
          ];

         $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
         $slider= SliderHomeModel::where('IdLang',$Lang['IdLang'])->get();
         if($Lang){
           $BestTour = [];
           $Category = TourCategory::join('lang','lang.IdLang','=','category.IdLang')
                                     ->where('lang.IdLang',$Lang['IdLang'])
                                     ->get();

           foreach($Category as $key) {
              $Tour_Category = TourModel::where('IdCategory',$key['IdCategory'])
                                    ->take(4)
                                    ->get();
                  $Info_Category = [];
                foreach($Tour_Category as $val ){
                    $preciotour = TourPriceModel::join('pricecategory','tourprices.IdPriceCategory','=','pricecategory.IdPriceCategory')
                    ->where('tourprices.IdTour',$val['IdTour'])
                    ->orderby('tourprices.PriceMount','DESC')
                    ->first();
                    $tour = [$preciotour,$val];
                    array_push($Info_Category,$tour);
                }
              $aux = [$key,$Info_Category];
              array_push($BestTour,$aux);
            }
          
           if($Lang['CodLang'] == "es"){
                $Association = '/en';
                $data_lg=$data['es'];
                $prefix = 'es_ES';
                $tag = $metatag['es'];

           }else{
                $Association = '/es';
                $data_lg=$data['en'];
                $prefix = 'en_GB';
                $tag = $metatag['en'];

           }

            //meta
            $meta =[
              'title'=>$Lang['TitleLang'],
              'url'=>$Lang['CodLang'],
              'description'=>$Lang['MetaDescription'],
              'prefix'=>$prefix,
              'img'=>$Lang['bannerImg']

            ];
            return $this->renderHTML('inicio.twig',[
              'language'=>$Lang,
              'tourhome'=>$BestTour,
              'association'=>$Association,
              'data'=>$data_lg,
              'meta'=>$meta,
              'tag'=>$tag,
              'sliderhome'=>$slider
            ]);
         }else{
          return $this->error404('es');
         }


    }

    public function GetInfoCategory($request, $atributes){

      $data=[
        'es'=>[
          'title_d'=>'Popular destinations among tourists',
          'title_c'=>'Opiniones',
          'title_cT'=>'Tradicionales',
          'title_cA'=>'Aventura',
          'title_Ban'=>'Disfruta de las maravillas de la naturaleza ,',
          'title_Ban2'=>'Reserva actividades y atracciones.',
          'title_Sub'=>'Adquiere nuevos recuerdos',
          'title_Sub2'=>'con asombrosos atractivos impresionantes.',
          'Regiones'=>'Nuestros Destinos',
            'more'=>'saber mas',
          'title_cH'=>'Caminatas',      
          'title_cJ'=>'Selva',
          'link_c'=>'tours-de-aventuras',
          'link_t'=>'tours-tradicionales',
          'link_h'=>'tours-de-caminatas',
          'link_j'=>'tours-de-aventuras',
          'subtitle_d'=>'Descubre experiencias únicas en los lugares más visitados',
          'duration'=>'Duracion',
          'from'=>'Desde',
          'destinations'=>'Destinos',
          'start_end'=>'Empieza/ Termina el recorrido',
          'agerange'=>'Rango de Edad',
          'transport'=>'Transporte',
          'brochure'=>'Descargar',
          'viewtour'=>'Ver Tour',
          'find_activities'=>'Encontrar Actividades',
          'Saf'=>'Recorrido Seguro',
          'Safp'=>'Este Tour tiene las medidas adecuadas que resguardan tu seguridad y la de tu familia , Las excursiones seran en grupos pequeños, no olvides seguir las indicaciones del personal durante la Excursion.'
          
        ],
        'en'=>[
           'title_d'=>'Popular destinations among tourists',
           'Price_de'=>'From',
           'Price_For'=>'Per person',
           'desitntex'=>'Popular destination',
           'subimg'=>'Best price',
           'title_c'=>'Feedback',
           'title_cT'=>'Traditionals',
           'link_c'=>'adventure-tours',
           'title_Ban'=>'Enjoy the wonders of nature,',
           'title_Ban2'=>'Book activities and attractions.',
           'title_Sub'=>'AGet new memories',
           'title_Sub2'=>'with amazingly impressive attractions.',
           'Regiones'=>'Our Destinations',
              'more'=>'out more',
          'link_t'=>'traditional-tours',
          'link_h'=>'hiking-tours',
          'link_j'=>'adventure-tours',
          'title_cA'=>'Adventures',
          'title_cH'=>'Hikings',
          'title_cJ'=>'Jungle',
           'subtitle_d'=>'Discover unique experiences in the most visited places',
           'duration'=>'Duration',
           'from'=>'From',
           'destinations'=>'Destinations',
           'start_end'=>'Starts/ Ends in',
           'agerange'=>'Age Range',
           'transport'=>'Transport',
           'brochure'=>'Downloand',
           'viewtour'=>'View Trip',
           'find_activities'=>'Find Activities'
              ]
      ];

      $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
      if($Lang){
        $Category = TourCategory::join('lang','lang.IdLang','=','category.IdLang')
                                  ->where('lang.IdLang',$Lang['IdLang'])
                                  ->where('category.UriCategory',$atributes['category'])
                                  ->select('category.*')
                                  ->first();
        if($Category){
          $TourInfo = [];
          $BestTour = TourModel::where('IdCategory',$Category['IdCategory'])
                               ->get();
          $Assoc = TourAssocCategory::where('IdCategory',$Category['IdCategory'])->first();
          $Association =TourAssocCategory::join('category','asociations.IdCategory','=','category.IdCategory')
                                        ->join('lang','category.IdLang','=','lang.IdLang')
                                        ->where('asociations.Key',$Assoc['Key'])
                                        ->where('asociations.IdCategory','!=',$Category['IdCategory'])
                                        ->first();
         $AssociationString = $Association['CodLang'].'/'.$Association['UriCategory'];
          foreach ($BestTour as $value ) {
            $PriceTour = TourPriceModel::join('pricecategory','tourprices.IdPriceCategory','=','pricecategory.IdPriceCategory')
                                        ->where('tourprices.IdTour',$value['IdTour'])
                                        ->orderBy('tourprices.PriceMount', 'DESC')
                                        ->first();
            # code...
            $aux = [$PriceTour,$value];

            array_push($TourInfo,$aux);
          }

          if($Lang['CodLang'] == "es"){
            $data_lg=$data['es'];
            $prefix = 'es_ES';
          }else{
            $data_lg=$data['en'];
            $prefix = 'en_GB';
          }
          $url = "{$Lang["CodLang"]}/{$Category["UriCategory"]}";
          //meta
          $meta =[
            'title'=>$Category['MetaTitle'],
            'url'=>$url,
            'description'=>$Category['MetaDescription'],
            'prefix'=>$prefix,
            'img'=>$Category['CategoryImgUri']
          ];

          return $this->renderHTML('categoria.twig',[
            'code'=>$Lang['CodLang'],
            'language'=>$Lang,
            'category'=>$Category,
            'tour'=>$TourInfo,
            'association'=>$AssociationString,
            'data'=>$data_lg,
            'meta'=>$meta
          ]);

        }else{
          return $this->error();
        }
      }else{
        return $this->error();
      }
    }


    public function GetPasarelaIndex($request,$atributes){
      $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
      if($Lang){
        if($Lang['CodLang'] == 'es'){
          $associations = 'en/carrito';
        }else{
          $associations = 'es/carrito';
        }

        $meta =[
          'title'=>'Tour Buying'
        ];

        return $this->renderHTML('pasarela.twig',[
          'language'=>$Lang,
          'association'=>$associations,
          'meta'=>$meta,
          'noindex'=>true
          ]);
      }else{
        return $this->error404('es');
      }
      
    }


    public function GetAboutIndex($request,$atributes){
      $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
      if($Lang){
        if($Lang['CodLang'] == 'es'){
          $associations = 'en/about';
          $title = "Acerca de Nosotros";
        }else{
          $associations = 'es/about';
          $title="About Our";
        }
        $meta =[
          'title'=>$title
        ];
        return $this->renderHTML("components/{$Lang['CodLang']}-about.twig",[
          'language'=>$Lang,
          'association'=>$associations,
          'meta'=>$meta,
          'noindex'=>true
        ]);
      }else{
        return $this->error404('es');
      }
    }
    
    public function GeterrorIndex($request,$atributes){
      $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
      if($Lang){
        if($Lang['CodLang'] == 'es'){
          $associations = 'en/error';
          $title = "Acerca de Nosotros";
        }else{
          $associations = 'es/error';
          $title="About Our";
        }
        $meta =[
          'title'=>$title
        ];
        return $this->renderHTML("components/{$Lang['CodLang']}-error.twig",[
          'language'=>$Lang,
          'association'=>$associations,
          'meta'=>$meta,
          'noindex'=>true
        ]);
      }else{
        return $this->error404('es');
      } 
    }

    public function GetreservapIndex($request,$atributes){
      $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
      if($Lang){
        if($Lang['CodLang'] == 'es'){
          $associations = 'en/reservationpolicy
          ';
          $title = "reservation policy";
        }else{
          $associations = 'es/politicasreserva';
          $title="Politicas de reserva";
        }
        $meta =[
          'title'=>$title
        ];
        return $this->renderHTML("components/{$Lang['CodLang']}-reservap.twig",[
          'language'=>$Lang,
          'association'=>$associations,
          'meta'=>$meta,
          'noindex'=>true
        ]);
      }else{
        return $this->error404('es');
      }  
    }



    public function GetPoliticaIndex($request,$atributes){
      $Lang = TourLang::where('CodLang',$atributes['lang'])->first();
      if($Lang){
        if($Lang['CodLang'] == 'es'){
          $associations = 'en/politicas';
          $title = "Politicas de privacidad";
        }else{
          $associations = 'es/politicas';
          $title="Privacy Policy";
        }
        $meta =[
          'title'=>$title
        ];
        return $this->renderHTML("components/{$Lang['CodLang']}-politicas.twig",[
          'language'=>$Lang,
          'association'=>$associations,
          'meta'=>$meta,
          'noindex'=>true
        ]);
      }else{
        return $this->error404('es');
      }
    
    }

    public function GetInfoTour($request, $attribute){
        //CONSULTA A LA BASE DE DATOS -> SI EL LENGUAGE INGRESADO EN LA URL EXISTE
        $data = [
          'en' =>[
            'Destinosmas'=>'Similar Destinations',
            'duration'=>'Duration',
            'from'=>'From',
            'Itinerary'=>'Route',
            'Incluyed'=>"What's included", 
            'Qllevar'=>'packing list', 
            'Cancelacion'=>'Cancellations', 
            'faqi'=>'Frequently Asked Questions',             
             'more'=>'saber mas',
            'departure'=>'Departure',
            'return'=>'Return',
            'included'=>"What's Included in",
            'noincluded'=>'What to Bring to ',
            'titlefaq'=>'Frequently asked questions about the',
            'Canp'=>'Cancellation Policy',
            'availableview'=>' Reserve now',
            'extratour'=>'Tour Extras',
            'TtileDescription'=>'Description',
            'abouttour'=>'About the tour',
            'shared'=>'Share your experience with other travelers',
            'shortdescription'=>'Short description',
            'recomendation'=>'What do you have to wear',
            'optionaltour'=>'OPTIONAL TOURS THAT MAY INTEREST YOU:',
            'optionalcategory'=>'Do you have other options?',
            'colabory'=>'COLLABORATING COMPANIES:',
            'back'=>'Back To Top',
            'questions'=>'Do you have questions about the tour?',
            'contactus'=>'CONTACT US',
            'private'=>'Quiere realizar un tour privado?',
            'acomodation'=>'Choose your accommodation',
            'extra'=>'Extras available',
            'date'=>'What day do you want to start the tour?',
            'people'=>'Person',
            'review'=>'Reviews',
            'highlight'=>'Highlights',
            'itinerary'=>'Itinerary',
            'incar'=>'You already have this tour in your cart, what do you expect your reservation ends?',
            'finish'=>'Finish reservation',
            'available'=>'Not available',
            'price'=>'Price',
            'priceall'=>'Full Price',
            'addcar'=>'Add To Cart',
            'segurepay'=>'Paga Seguro',
            'days'=>'Day',
            'day'=>'Day',
            'rangeage'=>'Age Range',
            'starttour'=>'Tour start',
            'maxgroup'=>'Maximum Group',
            'tinews'=>'Subscribe to our newsletter            ',
            'tinews2'=>'Do you want to be the first to know about our promotions? Subscribe to receive discounts and discover new destinations            ',
            'sub'=>'to subscribe',
            'trasport'=>'Transport',
            'finishtour'=>'End of Tour',
            'styletour'=>'Tour style',
            'bestprice' =>'Best guaranteed price',
            'localoperator' =>'Organized in Ensglish, Español',
            'payins'=>'Age range from 14 a 50',
            'payconfirm'=>'Immediate confirmation',
            'duration'=>'Duration',
            'Price_de'=>'From',
            'Price_For'=>'Per person',
            'desitntex'=>'Popular destination',
            'subimg'=>'Best price',
            'dias'=>' day',
            'compra'=>'Secure purchase',
            
            'Saf'=>'Safe Route | Covid19',
           'Safp'=>'This tour has the appropriate measures to protect your safety and that of your family, the excursions will be in small groups, do not forget to follow the instructions of the staff during the excursion.'
          ],
          'es'=>[
            'compra'=>'Compra Seguro',
            'tinews'=>'Suscríbete a nuestra newsletter',
             'tinews2'=>'¿Quieres ser el primero en conocer nuestras promociones? Suscríbete para recibir descuentos y descubrir nuevos destinos             ',
             'sub'=>'suscribirse',
            'Destinosmas'=>'Destinos Similares',
            'dias'=>' dia',
            'duration'=>'Duracion',
            'from'=>'Desde',
            'TtileDescription'=>'Descripción',
            'Itinerary'=>'Ruta',
            'Incluyed'=>'Incluido', 
            'Qllevar'=>'Equipaje', 
            'Cancelacion'=>'Cancelaciones', 
            'faqi'=>'Preguntas Frecuentes',
            'departure'=>'Salida',
            'Price_For'=>'Por persona',
              'desitntex'=>'Destino popular',
              'subimg'=>'Mejor precio',
              'Price_de'=>'Desde',
            'return'=>'Regreso',
            'included'=>'Qué está incluido en',
            'noincluded'=>'Qué LLevar a',
            'titlefaq'=>'Preguntas frecuentes sobre ',
            'Canp'=>'Políticas de Cancelación',
            'availableview'=>'Reservar Ahora',
            'extratour'=>'Extras del tour',
              'more'=>'out more',
            'abouttour'=>'Acerca del tour',
            'shared'=>'Comparte tu experiencia con otros viajeros.',
            'shortdescription'=>'Breve descripción',
            'recomendation'=>'Qué tienes que llevar',
            'optionaltour'=>'TOURS OPCIONALES QUE TE PUEDAN INTEREZAR:',
            'optionalcategory'=>'¿Tienes otras opciones?',
            'colabory'=>'EMPRESAS COLABORADORAS:',
            'back'=>'Regresa Al Inicio',
            'questions'=>'¿Tienes dudas acerca del tour?',
            'contactus'=>'CONTACTANOS',
            'private'=>'Quiere realizar un tour privado?',
            'acomodation'=>'Elige tu alojamiento',
            'extra'=>'Extras disponibles',
            'date'=>'¿Qué día quieres empezar el tour?',
            'people'=>'Persona',
            'review'=>'Comentarios',
            'incar'=>'Ya tienes este tour en tu carrito, ¿que esperas finaliza tu reserva?',
            'finish'=>'Finalizar reserva',
            'available'=>'No Disponible',
            'price'=>'Precio',
            'priceall'=>'Precio Total',
            'addcar'=>'Agregar Al Carrito',
            'segurepay'=>'Secure Pay',
            'highlight'=>'Destacados',
            'itinerary'=>'Itinerario',
            'days'=>'Dia',
            'day'=>'Dia',
            'rangeage'=>'Rango de Edad',
            'starttour'=>'Inicio del tour',
            'maxgroup'=> 'Maximo por Grupo',
            'trasport'=>'Transporte',
            'finishtour'=>'Finaliza el Tour',
            'styletour'=>'Estilo del Tour',
            'bestprice' =>'Mejor precio garantizado',
            'localoperator' =>'Organizado en inglés, español',
            'payins'=>'La edad va de 14 a 50 años',
            'payconfirm'=>'Confirmación inmediata',
            'Saf'=>'Recorrido Seguro | Covid19',
            'Safp'=>'Este Tour tiene las medidas adecuadas que resguardan tu seguridad y la de tu familia , Las excursiones seran en grupos pequeños, no olvides seguir las indicaciones del personal durante la Excursion.'
            ]
        ];

        $metatag =[
          'es'=>['','Descubrimiento','Aventura','Naturaleza'

          ],
          'en'=>['','Discovery','Adventure','Nature'

          ]
         ];

        $Lang = TourLang::where('CodLang',$attribute['lang'])->first();

        //CONDICIONAL PARA SAVER SI EXISTE EL LENGUAGE EN LA BASE DE DATOS

        if($Lang){
          $language= $Lang['CodLang'];
          $TourCategory = TourCategory::join('lang','lang.IdLang','=','category.IdLang')->where('category.UriCategory',$attribute['category'])->where('category.IdLang',$Lang['IdLang'])->first();

            if($TourCategory){

              $Tour = TourModel::join('category','category.IdCategory','=','tour.IdCategory')->where('tour.UriTour',$attribute['tour'])->where('tour.IdCategory',$TourCategory['IdCategory'])->select('tour.*')->first();

              if($Tour){
                $Assoc = TourAssociations::where('IdTour',$Tour['IdTour'])->first();
                $ItineraryDay =TourItinerary::join('itineraryday','itineraryday.IdItinerary','=','itinerary.IdItinerary')->where('itinerary.IdTour',$Tour['IdTour'])->get();
                 $TourGallery = TourGallery::where('IdTour',$Tour['IdTour'])->get();
                 $TourExtra = TourExtra::join('tour_has_extra','tour_has_extra.IdExtra','=','extra.IdExtra')
                                      ->where('tour_has_extra.IdTour',$Tour['IdTour'])
                                      ->get();
              $AssociationString="/";
              if($Assoc){
                 $Association = TourAssociations::join('tour','associatons.IdTour','=','tour.IdTour')
                                                ->join('category','category.IdCategory','=','tour.IdCategory')
                                                ->join('lang','category.IdLang','=','lang.IdLang')
                                                ->where('associatons.Key',$Assoc['Key'])
                                                ->where('associatons.IdTour','!=',$Tour['IdTour'])
                                                ->first();
                  $AssociationString = $Association['CodLang'].'/'.$Association['UriCategory'].'/'.$Association['UriTour'];
               }
               
                 $tourinfo = TourModel::join('hightext','tour.IdTour','=','hightext.IdTour')
                                       ->where('tour.UriTour',$attribute['tour'])
                                       ->select('hightext.*')
                                       ->first();
                $preciotour = TourPriceModel::join('pricecategory','tourprices.IdPriceCategory','=','pricecategory.IdPriceCategory')
                                            ->where('tourprices.IdTour',$Tour['IdTour'])
                                            ->get();
                 $BestPrice = TourPriceModel::join('pricecategory','tourprices.IdPriceCategory','=','pricecategory.IdPriceCategory')
                                            ->where('tourprices.IdTour',$Tour['IdTour'])
                                            ->orderby('tourprices.PriceMount','DESC')
                                            ->first();

                 $Tours = TourModel::where('IdCategory',$TourCategory['IdCategory'])->get();
                 $Categorys = TourCategory::where('IdCategory','!=',$TourCategory['IdCategory'])->where('IdLang',$Lang['IdLang'])->get();
                 $tourincluded = TourIncluded::where('IdTour',$Tour['IdTour'])
                                                   ->get();
                 $tournoincluded = TourNoIncluded::where('IdTour',$Tour['IdTour'])
                                                   ->get();
                 $tourfaq = TourFaq::where('IdTour',$Tour['IdTour'])
                                                   ->get();
                  $ItineraryArray=[];

                  foreach ($ItineraryDay as $key ) {

                        $ItineraryDescription = TourDayItinerary::where('IdItineraryDay',$key['IdItineraryDay'])->get();

                        $aux = [$key,$ItineraryDescription];

                        array_push($ItineraryArray,$aux);
                  }

                  if($Lang['CodLang'] == "es"){
                      $data_lg=$data['es'];
                      $prefix = 'es_ES';
                      $tag = $metatag['es'];
                  }else{
                      $data_lg=$data['en'];
                      $prefix = 'en_GB';
                      $tag = $metatag['en'];

                  }

                  $url = "{$Lang["CodLang"]}/{$TourCategory["UriCategory"]}/{$Tour["UriTour"]}";
                  //meta
                  $meta =[
                    'title'=>$Tour['MetaTitle'],
                    'url'=>$url,
                    'description'=>$Tour['MetaDescription'],
                    'prefix'=>$prefix,
                    'img'=>$Tour['TourBannerImg']
                  ];

                   return  $this->renderHTML('tour.twig',[
                                   'code'=>$Lang['CodLang'],
                                   'language'=>$Lang,
                                   'tour'=>$Tour,
                                   'prices'=>$preciotour,
                                   'category'=>$TourCategory,
                                   'itinerary'=>$ItineraryArray,
                                   'slider'=>$TourGallery,
                                   'includes'=>$tourincluded,
                                   'noincludes'=>$tournoincluded,
                                   'faq'=>$tourfaq,
                                   'extra'=>$TourExtra,
                                   'higth'=>$tourinfo,
                                   'tours'=>$Tours,
                                   'association'=>$AssociationString,
                                   'categorys'=>$Categorys,
                                   'data'=>$data_lg,
                                   'pricetour'=>$BestPrice,
                                   'meta'=>$meta,
                                   'tag'=>$tag
                                 ]);
             }else{
                return $this->error404('es');
             }
          }else{
            return $this->error404('es');
          }
      }else{
        return $this->error404('es');
      }
      // $ruta="views/modules/".$attribute['lang']."/".$attribute['categoryTour']."/".$attribute['nameTour'].".php";
      // file_exists($ruta) ? include_once $ruta : include_once "views/modules/error404.php";
    }

    public function error404($lang){
      $Lang = TourLang::where('CodLang',$lang)->first();
      return $this->renderHTML('components/es-error.twig',
      [
        'code'=>$Lang['CodLang'],
        'language'=>$Lang
        ]
      );
    }


  }

 ?>
