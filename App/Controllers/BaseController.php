<?php
 namespace App\Controllers;

 class BaseController {
    protected $templateEgine;

     public function __construct() {
        $loader = new \Twig\Loader\FilesystemLoader('Views');
        $this->templateEgine = new \Twig\Environment($loader, [
            'debug' => true,
            'cache' => false,
        ]);
     }

     public function renderHTML($fileName, $data=[]) {
         return $this->templateEgine->render($fileName, $data);
     }
 }
