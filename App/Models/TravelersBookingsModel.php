<?php
  namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

 class TravelersBookingsModel extends Model{

   // NOMBRE DE LA TABLA EN LA BASE DE DATOS
    protected $table="travelersbooking";
    protected $primaryKey = 'IdTraveler';
    public $timestamps = false;
    protected $created_at = null;
    protected $updated_at = null;
  }

 ?>
