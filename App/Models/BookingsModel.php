<?php
  namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

 class BookingsModel extends Model{

   // NOMBRE DE LA TABLA EN LA BASE DE DATOS
    protected $table="bookings";
    public $timestamps = false;
    protected $created_at = null;
    protected $updated_at = null;
  }

 ?>
