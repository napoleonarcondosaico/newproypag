<?php

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //define("EMAIL_PASSWORD","jP(fZ^f%YNOm");
  define("EMAIL_PASSWORD","typicaltrips2020%%.");
  // Jalando toda las librerias en un auto load

  require_once "vendor/autoload.php";
  // require_once "../App/Models/ToursModel.php";

  //instanciando las librerias de base de datos y manejo de rutas

  use Illuminate\Database\Capsule\Manager as Capsule;
  use Aura\Router\RouterContainer;

  // Conectandoce a la base de datos

  $dotenv = Dotenv\Dotenv::create(__DIR__ . '/');
  $dotenv->load();

  $capsule = new Capsule;

  $capsule->addConnection([
      'driver'    => 'mysql',
      'host'      => getenv('DB_HOST'),
      'database'  => getenv('DB_NAME'),
      'username'  => getenv('DB_USER'),
      'password'  => getenv('DB_PASS'),
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix'    => '',
  ]);

  // Make this Capsule instance available globally via static methods... (optional)
  $capsule->setAsGlobal();
  // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
  $capsule->bootEloquent();

  $request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
  );
  $routerContainer = new RouterContainer();
  $map = $routerContainer->getMap();

  // Creacion de las rutas get y post

 /* seond email*/
 
  $map->post('tour-email_info', '/request-pdf/email/send', [
    'controller' => 'App\Controllers\BookingController',
    'action' => 'GetPdf'
  ]);

  /*--------------rutas de index ---------*/
  $map->get('pasarela', '/{lang}/carrito', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetPasarelaIndex'
  ]);

  $map->post('pasarela-post', '/{lang}/carrito', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetPasarelaIndex'
  ]);


  $map->get('about','/{lang}/about',[
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetAboutIndex'
  ]);
 
  $map->get('politicas','/{lang}/politicas',[
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetPoliticaIndex'
  ]);

  /*--------------rutas de carrito post ---------*/
  $map->post('pasarela.post', '/carrito', [
    'controller' => 'App\Controllers\BookingController',
    'action' => 'AddBooking'
  ]);

  $map->get('inicio', '/', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetIndex'
  ]);

  $map->get('home_index', '/{lang}/', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetLanguageIndex'
  ]);
  
  $map->get('error','/{lang}/error',[
    'controller' => 'App\Controllers\TourController',
    'action' => 'GeterrorIndex'
  ]);
  $map->get('reservationpolicy','/{lang}/politicasreserva',[
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetreservapIndex'
  ]);
  $map->get('homo_index 2', '/{lang}', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetLanguageIndex'
  ]);

  $map->get('tour_category', '/{lang}/{category}', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetInfoCategory'
  ]);


  /*--------------- rutas de tour -----------*/

  $map->get('tour_info', '/{lang}/{category}/{tour}', [
    'controller' => 'App\Controllers\TourController',
    'action' => 'GetInfoTour'
  ]);

  /*--------------rutas de index ---------*/

  /*-------------- Registro de los pasajeros ----*/
  $map->post('new_booking', '/new/traveler/trip/ad', [
    'controller' => 'App\Controllers\BookingController',
    'action' => 'NewBooking'
  ]);

  /*>>>>>>>>>>>>>>>>>> Ruta para las consultas de tours en tiempo real <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
  $map->post('new search', '/search/tour/post', [
    'controller' => 'App\Controllers\SearchController',
    'action' => 'NewSearch'
  ]);



  $matcher = $routerContainer->getMatcher();
  $route = $matcher->match($request);
  if(!$route){
      echo "No se encontro lo que buscas";
  }else{
    $handlerData = $route->handler;
    $controllerName = $handlerData['controller'];
    $actionName = $handlerData['action'];
    // $needAuth = $handlerData['auth'] ?? false;
    // $userID = $_SESSION['UserId'] ?? false ;
    $atributes = $route->attributes;
    $controller = new $controllerName;
    echo $controller->$actionName($request,$atributes);
  }
 ?>
