-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-05-2020 a las 22:36:13
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `typicaltrips`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asociations`
--

CREATE TABLE `asociations` (
  `IdAssociation` int(11) NOT NULL,
  `IdCategory` int(11) NOT NULL,
  `Key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asociations`
--

INSERT INTO `asociations` (`IdAssociation`, `IdCategory`, `Key`) VALUES
(1, 1, 'CAJLD49OD2'),
(2, 4, 'CAJLD49OD2'),
(3, 2, 'CACLI3KRQS'),
(4, 5, 'CACLI3KRQS'),
(5, 3, 'CACT9OK7JU'),
(6, 6, 'CAQ97GPCSS'),
(1, 1, 'CAJLD49OD2'),
(2, 4, 'CAJLD49OD2'),
(3, 2, 'CACLI3KRQS'),
(4, 5, 'CACLI3KRQS'),
(5, 3, 'CACT9OK7JU'),
(6, 6, 'CAQ97GPCSS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `associatons`
--

CREATE TABLE `associatons` (
  `IdAssociation` int(11) NOT NULL,
  `IdTour` int(11) NOT NULL,
  `Key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `associatons`
--

INSERT INTO `associatons` (`IdAssociation`, `IdTour`, `Key`) VALUES
(1, 6, 'AT8SUBLLTR'),
(2, 23, 'AT8SUBLLTR'),
(3, 18, 'ATUFN6X8WN'),
(4, 1, 'ATUFN6X8WN'),
(5, 2, 'ATKZ1B26BP'),
(6, 19, 'ATKZ1B26BP'),
(7, 3, 'ATZAFY8U4C'),
(8, 20, 'ATZAFY8U4C'),
(9, 4, 'ATZCSYLTNP'),
(10, 21, 'ATZCSYLTNP'),
(11, 5, 'AT43XPDRSR'),
(12, 22, 'AT43XPDRSR'),
(15, 7, 'AT84NYBCQJ'),
(16, 24, 'AT1RYRHWJ5'),
(17, 8, 'AT2MEHV3BJ'),
(18, 24, 'AT1NXEM3TB'),
(19, 9, 'ATP61LCZGK'),
(20, 26, 'ATP61LCZGK'),
(21, 10, 'AT8312WKSY'),
(22, 27, 'AT8312WKSY'),
(23, 11, 'ATNEYBOJM2'),
(24, 28, 'ATNEYBOJM2'),
(25, 12, 'ATGHMW697J'),
(26, 29, 'ATGHMW697J'),
(27, 13, 'AT26MP457V'),
(28, 30, 'AT26MP457V'),
(29, 14, 'AT4ASR8DG6'),
(30, 31, 'AT4ASR8DG6'),
(31, 15, 'ATW18BIBPI'),
(32, 32, 'ATW18BIBPI'),
(33, 16, 'AT9T26VEUV'),
(34, 33, 'AT9T26VEUV'),
(35, 17, 'ATV1PBR51Y'),
(36, 34, 'ATV1PBR51Y'),
(37, 25, 'AT2MEHV3BJ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bookings`
--

CREATE TABLE `bookings` (
  `IdBooking` varchar(255) NOT NULL,
  `NameBooking` text NOT NULL,
  `EmailBooking` text NOT NULL,
  `PriceBooking` text NOT NULL,
  `HotelBooking` text NOT NULL,
  `RequestBooking` text NOT NULL,
  `PayMethod` varchar(255) NOT NULL,
  `PayKey` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bookings`
--

INSERT INTO `bookings` (`IdBooking`, `NameBooking`, `EmailBooking`, `PriceBooking`, `HotelBooking`, `RequestBooking`, `PayMethod`, `PayKey`, `status`) VALUES
('TP-1C6S8R6M', 'Lizandro', 'lizandroconde@gmail.com', '3062', 'Mil Hose', 'asd', '', '', 1),
('TP-3PL1YW6W', 'test', 'lizandroconde-buyer@gmail.com', '53', 'Mil House', 'sad', 'Paypal', 'EC-8K923694Y66680634', 1),
('TP-4ERSZ2T2', 'Lizandro', 'lizandroconde@gmail.com', '600', 'ewf', 'sad', '', '', 1),
('TP-4RRYDQH3', 'test', 'lizandroconde-buyer@gmail.com', '127', 'Mil House', 'sadasd', 'Paypal', 'EC-59F44906DV5228907', 1),
('TP-DZDXCYKT', 'test', 'lizandroconde-buyer@gmail.com', '127', 'Mil House', 'asd', 'Paypal', 'EC-25J276486M631511E', 1),
('TP-F1GQ99OV', 'Lizandro', 'lizandroconde@gmail.com', '600', 'Mil HOuse', 'sad', '', '', 1),
('TP-F8G4KJCS', 'test', 'lizandroconde-buyer@gmail.com', '127', 'Mil House', 'sadsad', 'Paypal', 'XAPQUWULA62YJ', 1),
('TP-G5IV8FF4', 'test', 'lizandroconde-buyer@gmail.com', '938', 'Mil House', 'asass', 'Paypal', 'EC-30N55703106892618', 1),
('TP-HPGOJMVN', 'Lizandro', 'lizandroconde@gmail.com', '600', 'Mil HOuse', 'sad', '', '', 1),
('TP-K2J5XQDE', 'Lizandro', 'lizandroconde@gmail.com', '600', 'Mil HOuse', 'sad', '', '', 1),
('TP-KHSR49MQ', 'Lizandro', 'lizandroconde@gmail.com', '600', 'Mil HOuse', 'sad', '', '', 1),
('TP-MI5ANW3I', 'Lizandro', 'lizandroconde@gmail.com', '600', 'ewf', 'sad', '', '', 1),
('TP-QMI2QUZI', 'Lizandro', 'lizandroconde@gmail.com', '600', 'Mil HOuse', 'sad', '', '', 1),
('TP-SWOCUBXQ', 'Lizandro', 'lizandroconde@gmail.com', '600', 'Mil HOuse', 'sad', '', '', 1),
('TP-VUAVB9UO', 'Lizandro', 'lizandroconde@gmail.com', '360', 'mil hose', 'sdfdfs', '', '', 1),
('TP-VVV5LO18', 'Lizandro', 'lizandroconde@gmail.com', '600', 'ewf', 'sad', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `IdCategory` int(11) NOT NULL,
  `UriCategory` varchar(255) NOT NULL,
  `CategoryName` varchar(50) NOT NULL,
  `CategoryImgUri` varchar(255) NOT NULL,
  `CategorytTitle` varchar(50) DEFAULT NULL,
  `CategoryDescription` varchar(255) NOT NULL,
  `MetaTitle` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `IdLang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`IdCategory`, `UriCategory`, `CategoryName`, `CategoryImgUri`, `CategorytTitle`, `CategoryDescription`, `MetaTitle`, `MetaDescription`, `IdLang`) VALUES
(1, 'tours-tradicionales', 'Tradicionales', 'https://i.postimg.cc/9QVRrJVr/pisac-2.jpg', 'Tours tradicionales', 'Viajes grandiosos de mucha historia', 'Reserva Los tours Tradicionales en Cusco', 'Ofrecemos variedades de tours tradicionales y paquetes turisticos a cusco', 1),
(2, 'tours-de-aventuras', 'Aventuras', 'https://previews.123rf.com/images/jkraft5/jkraft51410/jkraft5141000021/32472824-ruinas-y-terrazas-de-pisac-en-el-valle-sagrado-cerca-de-cusco-per%C3%BA.jpg', 'Tours de Aventuras', 'Viajes de mucha adrenalina', 'Reserva los mejores tours de aventura en cusco', 'Tours Aventura Cusco. La ciudadela de Machu Picchu tiene encantos historicos, culturales, arquitectonicos y naturales; los que cautivan a mas de uno', 1),
(3, 'tours-de-caminatas', 'caminatas', 'https://i.postimg.cc/NfxnN40T/camino-inca-machupicchu.jpg', 'Tours de caminatas', 'Grandiosas excursiones de caminata', 'Reserva Los mejores tours de caminata en cusco', 'Reserva los mejores tours de caminata en la ciudad de cusco, caminatas de un dias a 4 dias', 1),
(4, 'traditional-tours', 'Traditionals', 'https://www.theonlyperuguide.com/wp-content/uploads/2014/12/Pisac-Ruins-5.jpg', 'Traditional tours', 'Great journeys of a lot of history', 'Reserve Traditional Tours in Cusco', 'We offer a variety of traditional tours and tourist packages to Cusco.', 2),
(5, 'adventure-tours', 'Adventures', 'https://i.postimg.cc/qqPN4ZSj/atv-69.jpg', 'Adventure Tours', 'Trips of much adrenaline', 'Book the best adventure tours in Cusco', 'Adventure Tours Cusco. The citadel of Machu Picchu has historical, cultural, architectural and natural charms; those that captivate more than one.', 2),
(6, 'hiking-tours', 'Hikings', 'https://i.postimg.cc/Gt6JsK85/salkantay-7.jpg', 'Hiking tours', 'Great hiking excursions', 'Book The best walking tours in cusco', 'Book the best walking tours in cusco city, walks from one day to 4 days.', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extra`
--

CREATE TABLE `extra` (
  `IdExtra` int(11) NOT NULL,
  `ExtraName` varchar(50) NOT NULL,
  `ExtraPrice` float NOT NULL,
  `ExtraType` varchar(200) NOT NULL,
  `ExtraId` varchar(255) NOT NULL,
  `ExtraTagName` varchar(255) NOT NULL,
  `IdLang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `extra`
--

INSERT INTO `extra` (`IdExtra`, `ExtraName`, `ExtraPrice`, `ExtraType`, `ExtraId`, `ExtraTagName`, `IdLang`) VALUES
(1, 'Montaña de Machu Picchu (zona adicional)', 20, 'Extra', 'MachupicchuMontana', 'extraMachupicchu', 1),
(2, 'Montaña Huaynapicchu', 20, 'Extra', 'MachupicchuHuaynapicchu', 'extraMachupicchu', 1),
(3, 'Tren Vistadome / 360 - Ida', 40, 'Extra', 'trenVistadomeRetorno', 'trenVistadome', 1),
(4, 'Almuerzo Buffet en Machupicchu', 40, 'Extra', 'almuerzoBuffetMachupicchu', 'almuerzoBuffetMachupicchu', 1),
(5, 'Tren Hidro a Aguas Calientes', 40, 'Extra', 'trenHidroaAguas', 'trenHidroelectrica', 1),
(6, 'Bus de Subida y Bajada', 24, 'Extra', 'BusSubidayBajadaAguasCalientes', 'busMachupicchu', 1),
(7, 'Boleto Turistico Parcial', 25, 'Extra', 'boletoTuristicoParcial', 'boletoTuristicoParcial', 1),
(8, 'Servicio Privado', 100, 'Servicio', 'ServicioPrivado', 'tipoServicio', 1),
(9, 'Machu Picchu Mountain (additional zone)', 20, 'Extra', 'MachupicchuMontana', 'extraMachupicchu', 2),
(10, 'Huayna Picchu (additional zone)', 20, 'Extra', 'MachupicchuHuaynapicchu', 'extraMachupicchu', 2),
(11, 'Vistadome train | 360º - Return', 40, 'Extra', 'trenVistadomeRetorno', 'trenVistadome', 2),
(12, 'Buffet lunch in Machupicchu', 40, 'Extra', 'almuerzoBuffetMachupicchu', 'almuerzoBuffetMachupicchu', 2),
(13, 'Hydro train to Aguas Calientes', 40, 'Extra', 'trenHidroaAguas', 'trenHidroelectrica', 2),
(14, 'Bus Up and Down ', 24, 'Extra', 'BusSubidayBajadaAguasCalientes', 'busMachupicchu', 2),
(15, 'Private service', 100, 'Servicio', 'ServicioPrivado', 'tipoServicio', 2),
(16, 'Partial Tourist Ticket', 25, 'Extra', 'boletoTuristicoParcial', 'boletoTuristicoParcial', 2),
(17, 'Hotel Golden Sunrise ó similar de 4 estrellas', 80, 'Alojamiento', 'HotelGoldenSunrise', 'hotelAguasCalientes', 1),
(18, 'Waman Hotel ó similar de 3 estrellas', 80, 'Alojamiento', 'WamanHotel', 'hotelAguasCalientes', 1),
(19, 'Hotel Inkaterra ó similar de 5 estrellas', 60, 'Alojamiento', 'HotelInkaterra', 'hotelAguasCalientes', 1),
(20, 'Belmond Sanctuary Lodge de 5 estrellas', 60, 'Alojamiento', 'HotelBelmond', 'hotelAguasCalientes', 1),
(21, 'Hotel Golden Sunrise or similar 4 stars', 80, 'Alojamiento', 'HotelGoldenSunrise', 'hotelAguasCalientes', 2),
(22, 'Waman Hotel or similar 3 stars', 80, 'Alojamiento', 'WamanHotel', 'hotelAguasCalientes', 2),
(23, '5 star hotel Inkaterra or similar', 60, 'Alojamiento', 'HotelInkaterra', 'hotelAguasCalientes', 2),
(24, '5 star Belmond Sanctuary Lodge', 60, 'Alojamiento', 'HotelBelmond', 'hotelAguasCalientes', 2),
(25, 'Rafting(canotaje) dia 1', 30, 'Extra', 'raftingSantaMaria', 'raftingSantaMaria', 1),
(26, 'Zipline(tirolina) dia 3', 30, 'Extra', 'ziplineSantaTereza', 'ziplineSantaTereza', 1),
(27, 'Rafting(canotaje) day 1', 30, 'Extra', 'raftingSantaMaria', 'raftingSantaMaria', 2),
(28, 'Zipline(tirolina) day 3', 30, 'Extra', 'ziplineSantaTereza', 'ziplineSantaTereza', 2),
(29, 'Salida (07:00am - 12:30pm)', 0, 'Horario', 'primerTurnoCuatrimoto', 'horarioCuatrimotos', 1),
(30, 'Salida (13:00pm - 18:30pm)', 0, 'Horario', 'segundoTurnoCuatrimoto', 'horarioCuatrimotos', 1),
(31, 'Departure (07:00am - 12:30pm)', 0, 'Horario', 'primerTurnoCuatrimoto', 'horarioCuatrimotos', 2),
(32, 'Departure (13:00pm - 18:30pm)', 0, 'Horario', 'segundoTurnoCuatrimoto', 'horarioCuatrimotos', 2),
(33, 'Pachamanca lunch', 20, 'Extra', 'AlmuerzoPachamanca', 'AlmuerzoPachamanca', 2),
(34, 'Almuerzo Pachamanca', 20, 'Extra', 'AlmuerzoPachamanca', 'AlmuerzoPachamanca', 1),
(35, 'Ingreso a la laguna', 5, 'Extra', 'ingresoLagunaHumantay', 'ingresoLagunaHumantay', 1),
(36, 'Entrance to the lagoon', 5, 'Extra', 'ingresoLagunaHumantay', 'ingresoLagunaHumantay', 2),
(37, 'Ingreso a la Montaña', 5, 'Extra', 'entradaAlaMontaña', 'entradaAlaMontaña', 1),
(38, 'Caballo Ida y Vuelta', 20, 'Extra', 'irConCaballoAMontaña', 'irConCaballoAMontaña', 1),
(39, 'Entrance to the Mountain', 5, 'Extra', 'entradaAlaMontaña', 'entradaAlaMontaña', 2),
(40, 'Round Trip Horse', 20, 'Extra', 'irConCaballoAMontaña', 'irConCaballoAMontaña', 2),
(41, 'Bolsa de dormir', 20, 'Extra', 'sleepingBag', 'sleepingBag', 1),
(42, 'Sleeping bag', 20, 'Extra', 'sleepingBag', 'sleepingBag', 2),
(44, 'Tren Hidro a Aguas Calientes (ida y vuelta)', 80, 'Extra', 'trenHidroIdayVuelta', 'trenHidroelectrica', 1),
(45, 'Hydro train to Aguas Calientes (raund trip)', 80, 'Extra', 'trenHidroIdayVuelta', 'trenHidroelectrica', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeryimage`
--

CREATE TABLE `galeryimage` (
  `IdGaleryImage` int(11) NOT NULL,
  `UrlImage` text NOT NULL,
  `IdTour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeryimage`
--

INSERT INTO `galeryimage` (`IdGaleryImage`, `UrlImage`, `IdTour`) VALUES
(1, 'https://i.postimg.cc/mDBSXRzn/MORAY.jpg', 1),
(2, 'https://i.postimg.cc/3JdFbtN3/moray-cusco.jpg', 1),
(3, 'https://i.postimg.cc/sxvTFh7L/tour-salineras-cusco-800x600.jpg', 1),
(4, 'https://i.postimg.cc/9fjP0Vpf/Moray-geas.jpg', 1),
(5, 'https://i.postimg.cc/QMLm7cXh/maras.jpg', 1),
(10, 'https://i.postimg.cc/Fzh9s4Cx/Camino-Inca-Valle-Sagrado-typical.jpg', 2),
(16, 'https://i.postimg.cc/qBLCN9NN/calles-plazas-cusco-typicaltrips.jpg', 3),
(17, 'https://i.postimg.cc/RZRMdBzt/coricancha-typicaltrips.jpg', 3),
(18, 'https://i.postimg.cc/nVDp57Jp/sacsayhuaman-cusc-typicaltrips.jpg', 3),
(19, 'https://i.postimg.cc/zXw5DrvQ/cusco-tour-plaza-typicaltrips.jpg', 3),
(20, 'https://i.postimg.cc/nr8nbRxN/Sacsaihuaman.png', 3),
(21, 'https://i.postimg.cc/RFdL28wZ/cusco-tours.jpg', 3),
(22, 'https://i.postimg.cc/GtqDRQrf/Machu-Picchu-By-Car-1.jpg', 4),
(23, 'https://i.postimg.cc/W3VqJK1K/Machu-Picchu-By-Car-2.jpg', 4),
(24, 'https://i.postimg.cc/vBxGKRJW/machu.jpg', 4),
(25, 'https://i.postimg.cc/Dzpfv9RM/machu-picchu-fenomeno-el-nino-fen-defensa-civil-cusco.jpg', 4),
(26, 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 4),
(27, 'https://i.postimg.cc/RFhdktzH/camino-hidroelectrica-machu-picchu.jpg', 4),
(28, 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 4),
(29, 'https://i.postimg.cc/q7VRgspG/987987987oijiho6687.jpg', 4),
(30, 'https://i.postimg.cc/Wb4jXPYV/tren360-1.jpg', 5),
(31, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 5),
(32, 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 5),
(33, 'https://i.postimg.cc/vHFDNCsN/Machu-Picchu-Full-Day-1.jpg', 5),
(34, 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 5),
(35, 'https://i.postimg.cc/Dzpfv9RM/machu-picchu-fenomeno-el-nino-fen-defensa-civil-cusco.jpg', 5),
(36, 'https://i.postimg.cc/gcsmV0D4/Tren-Vistadome-1.jpg', 6),
(37, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 6),
(38, 'https://i.postimg.cc/765bFkhp/Hostel-Aguas-Calientes.jpg', 6),
(39, 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 6),
(40, 'https://i.postimg.cc/X7pBR9Gr/Amanecer-Machu-Picchu-1.jpg', 6),
(41, 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 6),
(42, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 6),
(43, 'https://i.postimg.cc/cJxxNzWB/pisac-mercado-1.jpg', 7),
(44, 'https://i.postimg.cc/dQkTXh4t/pisac-mercado-2.jpg', 7),
(45, 'https://i.postimg.cc/7LM53VcW/Valle-Sagrado-y-Machu-Picchu-4.jpg', 7),
(46, 'https://i.postimg.cc/G2dsVNGR/Almuerzo-Buffet-Urubamba.jpg', 7),
(47, 'https://i.postimg.cc/d01s8ZnN/choquequirao-ollantay.jpg', 7),
(48, 'https://i.postimg.cc/gcsmV0D4/Tren-Vistadome-1.jpg', 7),
(49, 'https://i.postimg.cc/Wb4jXPYV/tren360-1.jpg', 7),
(50, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 7),
(51, 'https://i.postimg.cc/8zvJ5DbD/Valle-Sagrado-y-Machu-Picchu-3.jpg', 7),
(52, 'https://i.postimg.cc/zGhSq3TM/valle_sagrado.jpg', 7),
(53, 'https://i.postimg.cc/jSnvCMYM/cuatratv.jpg', 8),
(54, 'https://i.postimg.cc/mk6NgHXx/Cuatrimoto-cusco.jpg', 8),
(55, 'https://i.postimg.cc/fTY726VH/atv-33.jpg', 8),
(56, 'https://i.postimg.cc/htrLgTbT/atv-32.jpg', 8),
(57, 'https://i.postimg.cc/pT7TJ6WR/atv-2.jpg', 8),
(58, 'https://i.postimg.cc/mDBSXRzn/MORAY.jpg', 8),
(59, 'https://i.postimg.cc/GtpxRQjJ/Marasmoray.jpg', 8),
(60, 'https://i.postimg.cc/rpfzKp7d/atv-72.jpg', 8),
(61, 'https://i.postimg.cc/9XxBp6Wx/cliclismo.jpg', 9),
(62, 'https://i.postimg.cc/8z0ChCTx/biking-6.jpg', 9),
(63, 'https://i.postimg.cc/DzG7Ldjb/biking-17.jpg', 9),
(64, 'https://i.postimg.cc/TYk26P5h/biking-15.jpg', 9),
(65, 'https://i.postimg.cc/GtpxRQjJ/Marasmoray.jpg', 9),
(66, 'https://i.postimg.cc/fbDN6Bw1/Ciclismo.jpg', 9),
(67, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 9),
(68, 'https://i.postimg.cc/65BdVxbP/inci.jpg', 10),
(69, 'https://i.postimg.cc/0js7HTkb/jungleq.jpg', 10),
(70, 'https://i.postimg.cc/VvWXyKv4/junglegiua.jpg', 10),
(71, 'https://i.postimg.cc/hG9DFt1z/jungle-4534-20181114.jpg', 10),
(72, 'https://i.postimg.cc/C1jqDcGY/jut.jpg', 10),
(73, 'https://i.postimg.cc/cC7wrWWr/hakuimg28.jpg', 10),
(74, 'https://i.postimg.cc/ncTq0S6h/kugne.jpg', 10),
(75, 'https://i.postimg.cc/g2T8PXc9/tiro.jpg', 10),
(76, 'https://i.postimg.cc/ncTq0S6h/kugne.jpg', 11),
(77, 'https://i.postimg.cc/cJn5WCSp/rafting-inca-jungle-trek-1.jpg', 11),
(80, 'https://i.postimg.cc/VvWXyKv4/junglegiua.jpg', 11),
(81, 'https://i.postimg.cc/Ss09JBzD/kjungel.jpg', 11),
(82, 'https://i.postimg.cc/ncTq0S6h/kugne.jpg', 11),
(83, 'https://i.postimg.cc/L8bsqHW4/zipline-4.jpg', 11),
(84, 'https://i.postimg.cc/0jq1Hzds/lakehum1.jpg', 12),
(85, 'https://i.postimg.cc/FsNQyJHb/laguine.jpg', 12),
(86, 'https://i.postimg.cc/VkHQPGB4/lakehhh.jpg', 12),
(87, 'https://i.postimg.cc/d3mcMJ4T/lguna1.jpg', 12),
(88, 'https://i.postimg.cc/BvLWSTm3/lakehunt.jpg', 12),
(89, 'https://i.postimg.cc/vZmFjF9c/Coloresmonta-a.png', 13),
(90, 'https://i.postimg.cc/9fr245Fv/colorescol.jpg', 13),
(91, 'https://i.postimg.cc/Pq9jY3CN/cloresmon.jpg', 13),
(92, 'https://i.postimg.cc/wTp9wG0m/camineco.jpg', 13),
(93, 'https://i.postimg.cc/7YnwdnWQ/coloreshumantyay.jpg', 13),
(94, 'https://i.postimg.cc/fyMg9mfJ/Salkantay23.jpg', 14),
(95, 'https://i.postimg.cc/gkYfXFgN/saklu.jpg', 14),
(96, 'https://i.postimg.cc/43brX77c/incatrail-1.png', 14),
(97, 'https://i.postimg.cc/CMZftjfz/mapi-29.jpg', 14),
(98, 'https://i.postimg.cc/8PRsQwkP/mapi-33.jpg', 14),
(99, 'https://i.postimg.cc/7Y0JPw94/mapi-18.jpg', 14),
(100, 'https://i.postimg.cc/sXTqZkJD/incatrail-6.jpg', 15),
(101, 'https://i.postimg.cc/Dw0H6QkG/clasicsalk.jpg', 15),
(102, 'https://i.postimg.cc/7Yjv9yr5/incatrail-4.jpg', 15),
(103, 'https://i.postimg.cc/43brX77c/incatrail-1.png', 15),
(104, 'https://i.postimg.cc/fTB1Y2FZ/Salkantayexp.jpg', 15),
(105, 'https://i.postimg.cc/R0MqvyZV/wayllabamba-to-pacaymayo.jpg', 15),
(106, 'https://i.postimg.cc/R0MqvyZV/wayllabamba-to-pacaymayo.jpg', 15),
(107, 'https://i.postimg.cc/fbgYrmxK/miskay.jpg', 15),
(108, 'https://i.postimg.cc/CLP6RZBx/salkantaymrur.jpg', 16),
(109, 'https://i.postimg.cc/rFFPyGpH/salkanbtaytrtrenk.jpg', 16),
(110, 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 16),
(111, 'https://i.postimg.cc/QNFzVnvS/salkantay-15.jpg', 16),
(112, 'https://i.postimg.cc/nzbWn6sK/salkantyq2.jpg', 16),
(113, 'https://i.postimg.cc/nc7wG9FD/salkantay-14.jpg', 16),
(114, 'https://i.postimg.cc/0jCPzWB8/salkantay-2.jpg', 16),
(115, 'https://i.postimg.cc/Gt6JsK85/salkantay-7.jpg', 16),
(116, 'https://i.postimg.cc/26QKy2vs/tremnksalk.jpg', 16),
(117, 'https://i.postimg.cc/43hMkJ3N/Salkantay.jpg', 16),
(119, 'https://i.postimg.cc/3JHZ7Shk/salkantay-17.jpg', 17),
(120, 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 17),
(122, 'https://i.postimg.cc/VkHQPGB4/lakehhh.jpg', 17),
(123, 'https://i.postimg.cc/3N9fCNt9/trejkingbikic.jpg', 17),
(124, 'https://i.postimg.cc/0jCPzWB8/salkantay-2.jpg', 17),
(126, 'https://i.postimg.cc/BvVqNZVS/salkw1.jpg', 17),
(127, 'https://i.postimg.cc/Z5QgM94M/13asalka.jpg', 17),
(128, 'https://i.postimg.cc/d3qCgXMd/bycar-1.jpg', 18),
(129, 'https://panel.taylorperutravel.com/uploads/images/3bbf878fd72e9d67d6c9801d1aae79fe511eb2af.jpg', 18),
(130, 'https://i.postimg.cc/d1VzWPYg/minas-de-sal-1.jpg', 18),
(131, 'https://i.postimg.cc/wvF9vQqY/centro-textil-chinchero-1.jpg', 18),
(132, 'https://i.postimg.cc/qqtWTsgL/minas-de-sal-3.jpg', 18),
(133, 'https://i.postimg.cc/cJxxNzWB/pisac-mercado-1.jpg', 19),
(136, 'https://i.postimg.cc/MZCj8zF0/pisac-1.jpg', 19),
(137, 'https://i.postimg.cc/G2dsVNGR/Almuerzo-Buffet-Urubamba.jpg', 19),
(138, 'https://i.postimg.cc/qq30LVc4/ollantaytambo-1.jpg', 19),
(139, 'https://i.postimg.cc/N0C5CtXH/ollantaytambo-3.jpg', 19),
(140, 'https://i.postimg.cc/02tRwN1D/chinchero-1.jpg', 19),
(141, 'https://i.postimg.cc/Dzx2fcyk/centro-textil-chinchero.jpg', 19),
(142, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 19),
(149, 'https://i.postimg.cc/GhFNFTK5/city-2.jpg', 20),
(150, 'https://i.postimg.cc/YSqJq21y/qoricancha-1.jpg', 20),
(151, 'https://i.postimg.cc/W4WxDR8p/sacsayhuaman-1.jpg', 20),
(152, 'https://i.postimg.cc/hvgK43Wr/quenqo-1.jpg', 20),
(153, 'https://i.postimg.cc/XYX4vChT/puka-pukara-1.jpg', 20),
(154, 'https://i.postimg.cc/6qRP18XT/tambomachay-1.jpg', 20),
(155, 'https://i.postimg.cc/GtqDRQrf/Machu-Picchu-By-Car-1.jpg', 21),
(156, 'https://i.postimg.cc/W3VqJK1K/Machu-Picchu-By-Car-2.jpg', 21),
(157, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 21),
(158, 'https://i.postimg.cc/765bFkhp/Hostel-Aguas-Calientes.jpg', 21),
(159, 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 21),
(160, 'https://i.postimg.cc/X7pBR9Gr/Amanecer-Machu-Picchu-1.jpg', 21),
(161, 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 21),
(162, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 21),
(163, 'https://i.postimg.cc/Wb4jXPYV/tren360-1.jpg', 22),
(164, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 22),
(165, 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 22),
(166, 'https://i.postimg.cc/vHFDNCsN/Machu-Picchu-Full-Day-1.jpg', 22),
(167, 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 22),
(168, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 22),
(169, 'https://i.postimg.cc/gcsmV0D4/Tren-Vistadome-1.jpg', 23),
(170, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 23),
(171, 'https://i.postimg.cc/765bFkhp/Hostel-Aguas-Calientes.jpg', 23),
(172, 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 23),
(173, 'https://i.postimg.cc/X7pBR9Gr/Amanecer-Machu-Picchu-1.jpg', 23),
(174, 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 23),
(175, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 23),
(176, 'https://i.postimg.cc/cJxxNzWB/pisac-mercado-1.jpg', 24),
(177, 'https://i.postimg.cc/dQkTXh4t/pisac-mercado-2.jpg', 24),
(178, 'https://i.postimg.cc/7LM53VcW/Valle-Sagrado-y-Machu-Picchu-4.jpg', 24),
(179, 'https://i.postimg.cc/G2dsVNGR/Almuerzo-Buffet-Urubamba.jpg', 24),
(180, 'https://i.postimg.cc/0j3jkHMb/Valle-Sagrado-Machu-Picchu-5.jpg', 24),
(181, 'https://i.postimg.cc/gcsmV0D4/Tren-Vistadome-1.jpg', 24),
(182, 'https://i.postimg.cc/Wb4jXPYV/tren360-1.jpg', 24),
(183, 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 24),
(184, 'https://i.postimg.cc/8zvJ5DbD/Valle-Sagrado-y-Machu-Picchu-3.jpg', 24),
(185, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 24),
(186, 'https://i.postimg.cc/nrDLRGr7/atv-1.jpg', 25),
(187, 'https://i.postimg.cc/FHgZR2mf/atv-19.jpg', 25),
(188, 'https://i.postimg.cc/fTY726VH/atv-33.jpg', 25),
(189, 'https://i.postimg.cc/htrLgTbT/atv-32.jpg', 25),
(190, 'https://i.postimg.cc/pT7TJ6WR/atv-2.jpg', 25),
(191, 'https://i.postimg.cc/3wNvT6wQ/atv-52.jpg', 25),
(192, 'https://i.postimg.cc/JtCz9HHF/Quad-Bike-Maras-Moray-1.jpg', 25),
(193, 'https://i.postimg.cc/rpfzKp7d/atv-72.jpg', 25),
(194, 'https://i.postimg.cc/y6mWwBTS/biking-5.jpg', 26),
(195, 'https://i.postimg.cc/8z0ChCTx/biking-6.jpg', 26),
(196, 'https://i.postimg.cc/DzG7Ldjb/biking-17.jpg', 26),
(197, 'https://i.postimg.cc/TYk26P5h/biking-15.jpg', 26),
(198, 'https://i.postimg.cc/3wNvT6wQ/atv-52.jpg', 26),
(199, 'https://i.postimg.cc/JtCz9HHF/Quad-Bike-Maras-Moray-1.jpg)', 26),
(200, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 26),
(201, 'https://i.postimg.cc/25Bq1YFr/jungle-4094-20181113.jpg', 27),
(202, 'https://i.postimg.cc/xTTcKMys/jungle-4153-20181113.jpg', 27),
(203, 'https://i.postimg.cc/jSSPdBFS/hakuimg16.jpg', 27),
(204, 'https://i.postimg.cc/hG9DFt1z/jungle-4534-20181114.jpg', 27),
(205, 'https://i.postimg.cc/2jdZdzp5/jungle-4316-20181114.jpg', 27),
(206, 'https://i.postimg.cc/cC7wrWWr/hakuimg28.jpg', 27),
(207, 'https://i.postimg.cc/9FqsgNpz/jungle-4787-20181116.jpg', 27),
(208, 'https://i.postimg.cc/9FqsgNpz/jungle-4787-20181116.jpg', 27),
(209, 'https://i.postimg.cc/NM65xxzn/jungle-4113-20181113.jpg', 28),
(210, 'https://i.postimg.cc/xTTcKMys/jungle-4153-20181113.jpg', 28),
(211, 'https://i.postimg.cc/jSSPdBFS/hakuimg16.jpg', 28),
(212, 'https://i.postimg.cc/hG9DFt1z/jungle-4534-20181114.jpg', 28),
(213, 'https://i.postimg.cc/2jdZdzp5/jungle-4316-20181114.jpg', 28),
(214, 'https://i.postimg.cc/2jdZdzp5/jungle-4316-20181114.jpg', 28),
(215, 'https://i.postimg.cc/2jdZdzp5/jungle-4316-20181114.jpg', 28),
(216, 'https://i.postimg.cc/T3VFfLB6/jungle-4807-20181116.jpg', 28),
(217, 'https://i.postimg.cc/bJXtGF52/cuesta-santa-ana-cusco.jpg', 29),
(218, 'https://i.postimg.cc/TwNNPrRK/humantay-12.jpg', 29),
(219, 'https://i.postimg.cc/YqGZhyLM/humantay-5.jpg', 29),
(220, 'https://i.postimg.cc/q7LZ9PSX/humantay-9.jpg', 29),
(221, 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 29),
(222, 'https://i.postimg.cc/bJXtGF52/cuesta-santa-ana-cusco.jpg', 30),
(223, 'https://i.postimg.cc/bJXtGF52/cuesta-santa-ana-cusco.jpg', 30),
(224, 'https://i.postimg.cc/Cx27yFxg/colores-8.jpg', 30),
(225, 'https://i.postimg.cc/QCqmv1H4/colores-6.jpg', 30),
(226, 'https://i.postimg.cc/0jvZtc2K/colores-4.jpg', 30),
(227, 'https://i.postimg.cc/Y9rT0rYs/incatrail-3.jpg', 31),
(228, 'https://i.postimg.cc/Y9rT0rYs/incatrail-3.jpg', 31),
(229, 'https://i.postimg.cc/43brX77c/incatrail-1.png', 31),
(230, 'https://i.postimg.cc/CMZftjfz/mapi-29.jpg', 31),
(231, 'https://i.postimg.cc/8PRsQwkP/mapi-33.jpg', 31),
(232, 'https://i.postimg.cc/7Y0JPw94/mapi-18.jpg', 31),
(233, 'https://i.postimg.cc/sXTqZkJD/incatrail-6.jpg', 32),
(234, 'https://i.postimg.cc/hGqkFbjg/incatrail-5.jpg', 32),
(235, 'https://i.postimg.cc/7Yjv9yr5/incatrail-4.jpg', 32),
(236, 'https://i.postimg.cc/43brX77c/incatrail-1.png', 32),
(237, 'https://i.postimg.cc/Y9rT0rYs/incatrail-3.jpg', 32),
(238, 'https://i.postimg.cc/R0MqvyZV/wayllabamba-to-pacaymayo.jpg', 32),
(239, 'https://i.postimg.cc/R0MqvyZV/wayllabamba-to-pacaymayo.jpg', 32),
(240, 'https://i.postimg.cc/fbgYrmxK/miskay.jpg', 32),
(241, 'https://i.postimg.cc/5tDx4g0n/soraypampa.jpg', 33),
(242, 'https://i.postimg.cc/3JHZ7Shk/salkantay-17.jpg', 33),
(243, 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 33),
(244, 'https://i.postimg.cc/QNFzVnvS/salkantay-15.jpg', 33),
(245, 'https://i.postimg.cc/QNFzVnvS/salkantay-15.jpg', 33),
(246, 'https://i.postimg.cc/nc7wG9FD/salkantay-14.jpg', 33),
(247, 'https://i.postimg.cc/0jCPzWB8/salkantay-2.jpg', 33),
(248, 'https://i.postimg.cc/Gt6JsK85/salkantay-7.jpg', 33),
(249, 'https://i.postimg.cc/8zvJ5DbD/Valle-Sagrado-y-Machu-Picchu-3.jpg', 33),
(250, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 33),
(251, 'https://i.postimg.cc/5tDx4g0n/soraypampa.jpg', 34),
(252, 'https://i.postimg.cc/3JHZ7Shk/salkantay-17.jpg', 34),
(253, 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 34),
(254, 'https://i.postimg.cc/QNFzVnvS/salkantay-15.jpg', 34),
(255, 'https://i.postimg.cc/QNFzVnvS/salkantay-15.jpg', 34),
(256, 'https://i.postimg.cc/nc7wG9FD/salkantay-14.jpg', 34),
(257, 'https://i.postimg.cc/0jCPzWB8/salkantay-2.jpg', 34),
(258, 'https://i.postimg.cc/Gt6JsK85/salkantay-7.jpg', 34),
(259, 'https://i.postimg.cc/8zvJ5DbD/Valle-Sagrado-y-Machu-Picchu-3.jpg', 34),
(260, 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 34),
(261, 'https://i.postimg.cc/WpnJKHsc/32ny.jpg', 4),
(262, 'https://i.postimg.cc/Y01Mp6q0/Limamacpampa.png', 3),
(263, 'https://i.postimg.cc/wjWHTTHX/humantaylsg.jpg', 12),
(264, 'https://i.postimg.cc/Ss9pr69d/laguehumanta.jpg', 12),
(265, 'https://i.postimg.cc/1XK2CJ3G/Monta-a.jpg', 13),
(266, 'https://i.postimg.cc/HWQg8tds/monta-a-colores12.jpg', 13),
(267, 'https://i.postimg.cc/mk4Ky0BZ/rabinrqw.jpg', 13),
(268, 'https://i.postimg.cc/g0fCnx7Z/rojomonta.jpg', 13),
(269, 'https://i.postimg.cc/26QKy2vs/tremnksalk.jpg', 17),
(270, 'https://i.postimg.cc/Dw0H6QkG/clasicsalk.jpg', 17),
(271, 'https://previews.123rf.com/images/jkraft5/jkraft51410/jkraft5141000021/32472824-ruinas-y-terrazas-de-pisac-en-el-valle-sagrado-cerca-de-cusco-per%C3%BA.jpg', 19),
(272, 'https://i.postimg.cc/pXkf6Fm0/peru-rail-hidroelectrica-02.jpg', 17),
(273, 'https://i.postimg.cc/fLkpVN6s/machu-picchu-honeymoon.jpg', 17),
(274, 'https://i.postimg.cc/sgGRzxDr/Tour-Valle-sagrado-incas-typical.jpg', 2),
(275, 'https://i.postimg.cc/Fzh9s4Cx/Camino-Inca-Valle-Sagrado-typical.jpg', 2),
(276, 'https://i.postimg.cc/cJckGGYW/ollantaytambo-typical.jpg', 2),
(277, 'https://i.postimg.cc/SxFgXxfq/chinchero-typicaltrips.jpg', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hightext`
--

CREATE TABLE `hightext` (
  `IdHight` int(11) NOT NULL,
  `Higth` text NOT NULL,
  `Description` text NOT NULL,
  `IdTour` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hightext`
--

INSERT INTO `hightext` (`IdHight`, `Higth`, `Description`, `IdTour`, `created_at`, `updated_at`) VALUES
(1, 'La mejor selccion esta por ocurrir;\r\nComo Lllegar a Machu Picchu ;\r\nTodo Podemos Viajr a Machu Picchu;\r\nCusco Tour;\r\nAtractivos Cusco', '<p>Comienza y termina en Cusco.</p>\r\n<p>Con el tour privado<strong> Machupicchu Full Day</strong>, tiene un paquete turístico de 1 día que lo llevará a Cusco, Perú y otros 2 destinos en Perú. Machupicchu Full Day es un tour para grupos pequeños que incluye comidas, transporte.</p>', 3, '2019-10-08 00:00:00', '2019-10-08 00:00:00'),
(2, 'Machu Picchu economico;\r\nMachu Picchu Ruta;\r\nEscala Machu Picchu;\r\nSubida a Machu Picchu', '<p>machu picchu por carro un tour alternativo para todo los viajeros que desean visitar machu picchu y disfrutar en solo 2 días y con un bajo costo.</p>', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Valle Sagrado de los incas; Experimenta los diferentes climas de moray; Minas de Sal; El Valle de los Incas ;Agricultura inca; Mercado Pisac;', '<p>El tour inicia en cusco aproximadamente a las 07:30 am. y finaliza a las 19:00 pm, el trasporte pasara por sus respectivos alojamientos junto a vuestro guia , para luego trasladarnos hacia <strong>valle sagrado</strong>, para tener una visita guiada, nuestra primera vista será la <strong>ciudadela de Pisac</strong> y una visita al típico <strong>mercado de Pisac</strong>, donde tendremos la oportunidad de comprar artesanías  y recuerdos de los pobladores.</p>\r\n<p>Luego nos dirigimos a <strong>la Ciudad de Urubamba</strong> donde podremos disfrutar de un Almuerzo Buffet, después del almuerzo seguiremos con nuestra travesía, viajando rumbo a Ollantaytambo a <strong>orillas del rio Vilcanota</strong>.</p>\r\n<p> </p>\r\n<p>Después de la <strong>visita guía en Ollantaytambo</strong>, los que hayan reservado un tour de<a title=\"Reserva Tour valle sagrado con Machu Picchu\" href=\"../../es/tours-tradicionales/valle-sagrado-y-machupicchu-conexion-2-dias\" target=\"_blank\" rel=\"noopener\"><strong> valle sagrado conexión a Machu Picchu</strong></a> tendrán que dirigirse hacia la estación de tren, los demás del grupo retornaremos a Cusco llegando entre las 18:30 a 18:40 horas</p>', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Machu Picchu Por Un Día;Guiado Por 2 Horas en Machu Picchu; Horarios de trenes personalizados', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Machu Picchu por tren en 1 dia : Esta opci&oacute;n es recomendable para usted que no tiene mucho tiempo en Cusco-Per&uacute;. Conocer&aacute;s Machu Picchu en el mejor tren en 1 d&iacute;a, Mediante un guiado de dos horas, luego continuaras visitando Machu Picchu individualmente, para despu&eacute;s retornar a la ciudad del cusco.</p>\r\n</body>\r\n</html>', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Machu Picchu Historico;\r\nMachu Picchu Altura;\r\nMachu Picchu Paisajes;\r\nCiudadela de Machu Picchu ', '<p>Viaje a través de los bellos paisajes de<strong> cumbres andinas</strong>, <strong>selva tropical</strong>, y del poderoso <strong>río Vilcanota</strong> en una ruta hacia la famosa ciudad perdida de <strong>los Incas</strong>, <strong>Machu Picchu</strong>. Esta excursión dura dos días y es la mejor opción de viaje, ya que te permite dedicar más tiempo a la exploración del complejo arqueológico sin la presencia de multitudes de turistas y así disfrutar de una impresionante vista del amanecer sobre <strong>Machu Picchu</strong>, un paisaje único e imperdible que todo viajero debería conocer!</p>', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Valle Sagrado de los Incas;\r\nMachu Picchu Altura;\r\nValle Sagrado Con Machu Picchu;\r\nConexion de toures', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Valle Sagrado</strong> Y <strong>Machu Picchu</strong> es un tour tradicional del cusco, visitando valles y lugares pintorescos, el primer d&iacute;a visitaremos los sitios arqueol&oacute;gicos sumamente reconocidos. El segundo d&iacute;a visitaremos La ciudadela Inca de <strong>Machu Picchu</strong>. el Tour empieza y termina en la ciudad de <strong>cusco</strong>.</p>\r\n</body>\r\n</html>', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Cuatrimotos en los andes;\r\nAventura Cusco;\r\nSalineras Cuatrimotos;\r\nMoray Cusco Cuatrimotos;\r\nTour Medio dia\r\n', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Visitar los principales destinos tur&iacute;sticos de <strong>Cusco</strong> es siempre una gran aventura. Ahora imagine un recorrido de <strong>Cuatrimotos </strong>a trav&eacute;s de sus <strong>hermosos paisajes</strong>&nbsp;y las &aacute;reas m&aacute;s simb&oacute;licas que la capital del <strong>imperio inca</strong> tiene para ofrecer. Podr&aacute;s hacer esto y mucho m&aacute;s durante este recorrido mientras viajas. Admire las enormes praderas rurales que caracterizan el gran <strong>Valle Sagrado</strong> y llegue a las<strong> Minas de Sal de Maras.</strong>&nbsp;Luego visitar&aacute; el sitio<strong> arqueol&oacute;gico de Moray</strong>, conocido como un laboratorio de investigaci&oacute;n agr&iacute;cola inca, donde a&uacute;n se puede ver claramente el dise&ntilde;o particular de sus plataformas. &iexcl;Deja que la misteriosa ciudad de <strong>Cusco</strong> te sorprenda mientras viajas en quad!</p>\r\n</body>\r\n</html>', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Ciclismo en maras;\r\nCiclismo cusco;\r\nCiclismo moray;\r\nhiking cusco;\r\nhiking maras - moray ', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>En este viaje de un d&iacute;a vas a tener la oportunidad de explorar los fascinantes paisajes del centro <strong>arqueol&oacute;gico de Moray</strong> y las asombrosas minas de sal del <strong>pueblo de Maras</strong>(<strong>salineras</strong>).</p>\r\n</body>\r\n</html>', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Selva inka Caminata;\r\nInka trail;\r\ninka jungle trail;\r\nCaminata Selva;\r\nCaminata 4 dias', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tour <strong>Inca Jungle trek 4 Dias</strong>&nbsp;en bicicleta de monta&ntilde;a, dormir con una acogedora familia local de la selva, y se realiza tirolesa (<strong>zip line</strong>), senderismo (<strong>hiking</strong>) y conoceremos <strong>caminos incas</strong> recientemente descubiertos. Finalmente, el tour termina en una de las siete maravillas del mundo, elegido el mejor destino de viaje por la <strong>National Geographic</strong>: <strong>Machu Picchu</strong>.</p>\r\n</body>\r\n</html>', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Canotaje en la Selva Inca;\r\nTirolesa en la Selva Inca;\r\nBicicleta en la selva Inca;\r\nVisite Machu Picchu', '<p>El Recorrido de 3 Días en la <strong>Selva Inca</strong>, es una alternativa con deportes de aventura como ciclismo, canotaje, tirolesa que nos llevará a <strong>Machu Picchu</strong>. </p>\r\n<p>Visitando <strong>plantaciones de coca</strong> y <strong>café</strong>. También podremos apreciar animales de diversas especies. Esta ruta fue pensada para aquellos a los que les gusta caminar, la <strong>aventura</strong> y la <strong>adrenalina</strong>.</p>', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Caminata;\r\nVistas unicas;\r\nNaturaleza;\r\nDesafío', '<p>La laguna Humantay, está rodeado de imponentes glaciares que forman parte de la Cordillera de los Andes, entre estos el majestuoso Nevado Salkantay y Humantay que al deshielo da origen a la esplendida laguna Humantay, tan solo verlo transforma tu forma de ver la naturaleza, disfrutando de la flora y fauna que quedará registrada en tus maravillosas fotografías en el lugar con el aire puro que tu cuerpo te lo agradecerá.</p>', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Montaña de Colores altitud;\r\nMontaña de Colores Peru;\r\nMontaña de Colores Vinicunca;\r\nMontaña de Colores altitud\r\n', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>La <strong>monta&ntilde;a de los siete colores</strong> (<strong>vinicunca</strong>) unas de las mejores atracciones del <strong>Per&uacute;</strong>. Su mayor atractivo tur&iacute;stico son sus laderas y cumbres, las cuales adquirieron diversos colores y matices debido al deshielo y la erosi&oacute;n de una diversidad de minerales formadas hace millones de a&ntilde;os. Te invitamos conocer la monta&ntilde;a con nosotros.</p>\r\n</body>\r\n</html>', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Camino Inca mapa;\r\nCamino Inca altura;\r\nCamino Inca arquitectura;\r\nCamino del inca bicicleta\r\n\r\n', '<p>En estos dos días recorreremos la <strong>ruta de senderismo</strong>: un sendero con más de 500 años de antigüedad. En la caminata apreciaremos un entorno único y nuestros pasos nos llevaran a la <strong>zona Arqueológica</strong> de <strong>Wiñaywayna</strong> y <strong>la Puerta del Sol</strong>, la entrada al recinto sagrado de la ciudadela Inca. A la mañana siguiente, finalizaremos nuestro Inka Trail, con uno de los amaneceres más hermosos, la salida del sol en <strong>Machu Picchu</strong> y una visita guiada por las <strong>ruinas arqueológicas</strong>.</p>', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Ruta del pueblo inca;\r\nCaminata a Machu Picchu;\r\nRuinas en Cusco;\r\nCamino Inca 4 dias\r\n', '<p>En este viaje que consiste de 4 días vivirás la experiencia de recorrer la <strong>legendaria ruta</strong> del <strong>pueblo inca</strong> y que concluye con la llegada al recinto sagrado del <strong>Machu Picchu</strong>. Durante el trayecto, podrás disfrutar de una ruta única en el que descubriremos lugares mágicos como <strong>la ciudad inca de Llactapata</strong>, el valle de Pacaymayo o los <strong>complejos arqueológicos</strong> de <strong>Sayacmarca</strong> y <strong>Phuyupatamarca</strong>. El ultimo día viviremos un momento único, acceder a <strong>Machu Picchu</strong> por <strong>la Puerta del Sol</strong>, donde disfrutaremos del mágico amanecer sobre <strong>la ciudadela sagrada de los incas</strong>.</p>', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Salkantay Trek a Machu Picchu; Salkantay Cusco; Salkantay Trekking; Conoce Machu Picchu', '<p>El tour Salkantay trek 4 dias Es una alternativa desafiante al Camino Inca.Desde las montañas cubiertas de nieve hasta la selva amazónica, esta es una excelente manera de viajar a Machu Picchu.</p>', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Salkantay Trek; \r\nCaminata A Machu Picchu;\r\nCaminata Misteriosa', '<p> </p>\r\n<p>El famoso y <strong>Clasico Salkantay</strong> Trek 5 Dias (<strong>caminata Salkantay hacia machu picchu</strong>) nombrado como uno de los 25 mejores Treks del mundo por <strong>National Geographic</strong>. El Tour Salkantay a Machu Picchu es una alternativa al <strong>Camino Inca</strong> y, según la opinión de la mayoría, es la mejor ruta alternativa (¡y quizás incluso mejor!). Este magnífico sendero es perfecto para los aventureros que desean integrarse personalmente en los ecosistemas cambiantes de un día para otro. Caminarás a través de los <strong>paisajes mágicos</strong> de una montaña nevada hasta una jungla tropical donde encontrarás una gran variedad de animales, plantas medicinales y variedades de flores. Sea testigo de sus propios ojos de una cultura viva, pero antigua, en aldeas remotas que casi nunca ven los extranjeros.</p>', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Sacred Valley of the Incas; Experience the different climates of Moray; Salt Mines; The Valley of the Incas', '<p>The tour begins in Cusco at approximately 07:30 am. and ends at 19:00 pm, the transport will pass through their respective lodgings with your guide, then transfer to the Sacred Valley, to have a guided tour, our first sight will be the citadel of Pisac and a visit to the typical market of Pisac, where we will have the opportunity to buy handicrafts from the villagers.</p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p>Then we go to the city of Urubamba where we can enjoy a buffet lunch, after lunch we continue our journey, traveling to Ollantaytambo on the banks of the river Vilcanota.</p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p>After the tour guide in Ollantaytambo, those who have booked a sacred valley tour connection to Machu Picchu will have to go to the train station, the rest of the group will return to Cusco arriving between 18:30 to 18:40 hours.</p>', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'The best selection is about to happen;\r\nHow to get to Machu Picchu ;\r\nWe Can All Travel to Machu Picchu', '<p>It starts and ends in Cusco! With the Machupicchu Full Day private tour, you have a 1 day tour package that will take you to Cusco, Peru and 2 other destinations in Peru. Machupicchu Full Day is a small group tour that includes meals, transportation.</p>', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Machu Picchu economic;\r\nMachu Picchu Route;\r\nScale Machu Picchu;\r\nAscent to Machu Picchu', '<p><strong>machu picchu by car</strong> an alternative tour for all travelers who want to visit <strong>machu picchu</strong> and enjoy in just 2 days and at a low cost.</p>', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Machu Picchu For A Day; Guided For 2 Hours At Machu Picchu; Custom Train Schedules', '<p>Machu Picchu by train in 1 day : This option is recommended for those who do not have much time in Cusco-Peru. You will visit Machu Picchu on the best train in 1 day, with a two hour guide, then you will continue visiting Machu Picchu individually, and then return to the city of Cusco.</p>', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Historical Machu Picchu;\r\nMachu Picchu Height;\r\nMachu Picchu Landscapes;\r\nCitadel of Machu Picchu ', '<p>Travel through the beautiful landscapes of <strong>Andean peaks, rainforest</strong>, and the mighty <strong>Vilcanota River</strong> on a route to the famous lost city of <strong>the Incas, Machu Picchu</strong>. This excursion lasts two days and is the best travel option, as it allows you to devote more time to exploring the archaeological complex without the presence of crowds of tourists and thus enjoy an impressive view of the sunrise over <strong>Machu Picchu</strong>, a unique and unforgettable landscape that every traveler should know!</p>', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Sacred Valley of the Incas; Experience the different climates of Moray; Salt Mines; The Valley of the Incas', '<p>Sacred Valley And <strong>Machu Picchu</strong> is a traditional tour of Cusco, visiting valleys and picturesque places. The first day we will visit the most visited archaeological sites and highly recognized. And second day we will visit <strong>the Inca citadel</strong> of <strong>Machu Picchu</strong>. The tour starts and ends in the city of <strong>Cusco</strong>.</p>', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'ATVs in the Andes;\r\nAdventure Cusco;\r\nSalineras Cuatrimotos;\r\nMoray Cusco Cuatrimotos;\r\nHalf Day Tour', '<p>Visiting the main tourist destinations in <strong>Cusco</strong> is always a great adventure. Now imagine a tour of <strong>Cuatrimotos</strong> through its <strong>beautiful landscapes</strong> and the most symbolic areas that the capital of the <strong>Inca empire</strong> has to offer. You will be able to do this and much more during this tour as you travel. Admire the enormous rural prairies that characterize the great <strong>Sacred Valley</strong> and reach the <strong>Maras Salt Mines</strong>. Then you will visit the <strong>archaeological site of Moray</strong>, known as an Inca agricultural research laboratory, where you can still clearly see the particular design of its platforms. Let the mysterious city of <strong>Cusco</strong> surprise you while you travel by quad!</p>', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Cycling in maras;\r\nCycling cusco;\r\nMoray cycling;\r\nhiking cusco;\r\nHiking Maras - Moray ', '<p>In this day trip you will have the opportunity to explore the fascinating landscapes of the <strong>archaeological</strong> center of <strong>Moray</strong> and the amazing salt mines of the town of <strong>Maras (salt mines)</strong>.</p>', 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Inka jungle Walk;\r\nInca Trail;\r\nInca Trail in the jungle;\r\nJungle Walk;\r\nWalk 4 days', '<p><strong>Inca Jungle</strong> <strong>trek</strong> 4 days, <strong>Mountain</strong> bike tour to sleep with a cozy local family of the jungle, <strong>zipline</strong> (zip line), hiking (<strong>hiking</strong>) and we will discover recently discovered Inca roads. Finally, the tour ends in one of the seven wonders of the world, chosen the best travel destination by National Geographic: <strong>Machu Picchu.</strong></p>', 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Rafting Inca Jungle;\r\nZip Line Inca Jungle;\r\nBiking Inca Jungle;\r\nVisita Machu Picchu', '<p>The 3 Day <strong>Inca Jungle</strong> <strong>Tour</strong> is an alternative with adventure sports such as <strong>cycling</strong>, <strong>canoeing</strong>, <strong>zip line</strong> that will take us to <strong>Machu Picchu</strong>. </p>\r\n<p> </p>\r\n<p>Visiting coca and coffee plantations. Also we will be able to appreciate animals of diverse species. This route was thought for those who like <strong>walking</strong>, adventure and adrenaline.</p>', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Walk;\r\nUnique views;\r\nNature\r\nChallenge', '<p>The <strong>Humantay Lagoon</strong> is surrounded by imposing glaciers that form part of the <strong>Andes Mountain</strong> Range, among them the majestic <strong>Salkantay</strong> and <strong>Humantay Nevado</strong> that gives rise to the splendid <strong>Humantay Lagoon</strong>, just seeing it transforms your way of seeing nature, enjoying the flora and fauna that will be registered in your wonderful photographs in the place with the pure air that your body will thank you for it.</p>', 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Mountain of Colors altitude;\r\nMountain of Colors Peru;\r\nMountain of Colors Vinicunca;\r\nMountain of Colors altitude\r\n', '<p>The <strong>mountain</strong> of the <strong>seven colors</strong> (<strong>vinicunca</strong>) some of the best attractions of <strong>Peru</strong>. Its main tourist attraction are its slopes and peaks, which acquired different <strong>colors</strong> and shades due to the thawing and erosion of a diversity of minerals formed millions of years ago. We invite you to know the <strong>mountain</strong> with us.</p>', 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Inca Trail map;\r\nInca Trail height;\r\nInca Trail architecture;\r\nInca Trail bicycle', '<p>In these two days we will travel the <strong>hiking route</strong>: a trail more than 500 years old. In the walk we will appreciate a unique environment and our steps will take us to the <strong>Archaeological</strong> zone of <strong>Wiñaywayna</strong> and the<strong> Puerta del Sol</strong>, the entrance to the sacred enclosure of the <strong>Inca citadel</strong>. The next morning, we will finish our <strong>Inka Trail</strong>, with one of the most beautiful sunrises, the sunrise at <strong>Machu Picchu</strong> and a guided tour of the <strong>archaeological</strong> ruins.</p>', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Route of the Inca village;\r\nWalk to Machu Picchu;\r\nRuins in Cusco;\r\nInca Trail 4 days', '<p>In this trip that consists of 4 days you will live the experience of traveling<strong> the legendary route of the Inca</strong> people and that concludes with the arrival at <strong>the sacred site of Machu Picchu</strong>. During the journey, you can enjoy a unique route where we will discover magical places such as<strong> the Inca city of Llactapata</strong>, t<strong>he Pacaymayo valley</strong> or <strong>the archaeological complexes</strong> of <strong>Sayacmarca</strong> and <strong>Phuyupatamarca</strong>. The last day we will live a unique moment, access <strong>Machu Picchu</strong> through<strong> the Puerta del Sol</strong>, where we will enjoy <strong>the magical sunrise</strong> over<strong> the sacred citadel of the Incas</strong>.</p>', 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Salkantay Trek; \r\nWalk To Machu Picchu;\r\nMysterious Walk', '<p>The tour Salkantay trek 4 days Is a challenging alternative to Inca Trail , and the government has not imposed restrictions on the number of people who can complete it per day, so you do not need to book 4 months in advance. The name Salkantay is a Quechua word meaning \"Wild Mountain\". From the snow-capped mountains to the Amazon jungle, this is an excellent way to travel to Machu Picchu.</p>', 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Salkantay Trek; \r\nWalk To Machu Picchu;\r\nMysterious Walk', '<p>On this 5-day tour that is one of the most impressive treks in <strong>Cusco</strong>. This great walk will allow us to see incredible landscapes, imposing snowy peaks, waterfalls, rivers and small towns, it is nature in its purest form. This hiking route is ideal for those who want to reach <strong>Machu Picchu</strong> by a different and authentic path, approaching Andean communities and away from noise. All this makes this Salkantay Trek the perfect alternative to the <strong>Inca Trail</strong>.</p>', 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Minas de Sal de Maras; Moray imagenes; Mejor recorrido en Cusco; Visita Guiada en Medio Dia  ; Los incas ; andenes de cultivos', '<p><strong>Los incas</strong> construyeron miles de andenes para <strong>el cultivo de diversos productos como la papa, el camote, el tomate, la quinua</strong> y más. Quizá el más hermoso de todos fue Moray,también <strong>un sistema circular de andenes</strong> que sobrevive hasta hoy. Cerca de allí,también  existen unos <strong>pozos de sal natural</strong> formado hace miles de años. Este lugar se llama <strong>las salineras de Maras</strong> y junto a <strong>Moray</strong>, son dos de los atractivos turísticos más populares en Cusco.</p>', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Maras Salt Mines;\r\nMoray Antigua Culture;\r\nBest Excursion in Cusco;\r\nHalf Day Guide Visit\r\n', '<p>The <strong>Incas built</strong> miles of platforms for the cultivation of various products such as potatoes, sweet potatoes, tomatoes, quinoa and more. Perhaps the most beautiful of all was Moray, a circular system of platforms that survives until today. Nearby, there are some natural salt wells formed miles of years ago. This place is called the salt mines of Maras and, together with Moray, son of the most popular tourist attractions in Cusco.</p>', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itinerary`
--

CREATE TABLE `itinerary` (
  `IdItinerary` int(11) NOT NULL,
  `IdTour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `itinerary`
--

INSERT INTO `itinerary` (`IdItinerary`, `IdTour`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25),
(26, 26),
(27, 27),
(28, 28),
(29, 29),
(30, 30),
(31, 31),
(32, 32),
(33, 33),
(34, 34),
(43, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itineraryday`
--

CREATE TABLE `itineraryday` (
  `IdItineraryDay` int(11) NOT NULL,
  `ItineraryDay` int(11) NOT NULL,
  `RutaDay` text NOT NULL,
  `IdItinerary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `itineraryday`
--

INSERT INTO `itineraryday` (`IdItineraryDay`, `ItineraryDay`, `RutaDay`, `IdItinerary`) VALUES
(2, 1, 'Cusco – Pisac – Ollantaytambo – Chinchero – Cusco', 2),
(3, 1, 'Cuzco – Sacsayhuaman – Qenko – Puka Pukara – Tambomachay', 3),
(4, 1, 'Cuzco - Aguas Calientes', 4),
(5, 2, 'Aguas Calientes - Machu Picchu - Cuzco', 4),
(6, 1, 'Cuzco - Machu Picchu - Cuzco', 5),
(7, 1, 'Cuzco - Aguas Calientes', 6),
(8, 2, 'Aguas Calientes - Machu Picchu - Cuzco', 6),
(9, 1, 'Cusco - Pisac - Urubamba - Ollantaytambo - Aguas Calientes', 7),
(10, 2, 'Aguas Calientes - Machu Picchu - Cuzco', 7),
(11, 1, 'Cusco - Racchi - Salineras - Moray - Cusco', 8),
(12, 1, 'Cusco - Cruspata - Moray - Salineras - Cusco', 9),
(13, 1, 'Cusco - Bicicleta - Rafting - Santa Maria', 10),
(14, 2, 'Santa Maria - Quellomayo - Santa Teresa', 10),
(15, 3, 'Santa Tereza - Hidroelectrica - Aguas Calientes', 10),
(16, 4, 'Aguas Calientes - Machu Picchu - Ollantaytambo - Cusco', 10),
(19, 1, 'Cuzco – Mollepata – Soraypampa – Lago Humantay', 12),
(20, 1, 'Cuzco - Pitumarca - Chillihuani - Montaña Arcoíris', 13),
(21, 1, 'Cusco - Km. 104 - Aguas calientes', 14),
(22, 2, 'Aguas Calientes - Machu Picchu - Cusco', 14),
(23, 1, 'Cusco - Wayllabamba', 15),
(24, 2, 'Wayllabamba - Picaymayo', 15),
(25, 3, 'Picaymayo - Wiñaywayna', 15),
(26, 4, 'Wiñaywayna - Machu Picchu - Cuzco', 15),
(27, 1, 'Cuzco - Mollepata - Soraypampa', 16),
(28, 2, 'Soraypampa - Soyrococha – Huayracmachay – Colpapampa', 16),
(29, 3, 'Colpapampa – Playa Sahuayaco – Hidroeléctrica – Aguas Calientes', 16),
(30, 4, 'Aguas Calientes – Machu Picchu – Cusco', 16),
(31, 1, 'Cuzco - Chinchero - Maras - Moray', 18),
(32, 1, 'Cusco – Pisac – Ollantaytambo – Chinchero – Cusco', 19),
(33, 1, 'Cuzco – Sacsayhuaman – Qenko – Puka Pukara – Tambomachay', 20),
(34, 1, 'Cuzco - Aguas Calientes', 21),
(35, 2, 'Aguas Calientes - Machu Picchu - Cuzco', 21),
(36, 1, 'Cuzco - Machu Picchu - Cuzco', 22),
(37, 1, 'Cuzco - Aguas Calientes', 23),
(38, 2, 'Aguas Calientes - Machu Picchu - Cuzco', 23),
(39, 1, 'Cusco - Pisac - Urubamba - Ollantaytambo - Aguas Calientes', 24),
(40, 2, 'Aguas Calientes - Machu Picchu - Cuzco', 24),
(41, 1, 'Cusco - Racchi - Salineras - Moray - Cusco', 25),
(42, 1, 'Cusco - Cruspata - Moray - Salineras - Cusco', 26),
(43, 1, 'Cusco - Bicicleta - Rafting - Santa Maria', 27),
(44, 2, 'Santa Maria - Quellomayo - Santa Teresa', 27),
(45, 3, 'Santa Tereza - Hydroelectric - Aguas Calientes', 27),
(46, 4, 'Aguas Calientes - Machu Picchu - Ollantaytambo - Cusco', 27),
(47, 1, 'Cusco - Bicicleta - Rafting - Santa Maria', 28),
(48, 2, 'Santa Tereza - Hidroelectrica - Aguas Calientes', 28),
(49, 3, 'Aguas Calientes - Machu Picchu - Ollantaytambo - Cusco', 28),
(50, 1, 'Cuzco – Mollepata – Soraypampa – Humantay Lake', 29),
(51, 1, 'Cuzco - Pitumarca - Chillihuani - Raimbow Mountain', 30),
(52, 1, 'Cusco - Km. 104 - Aguas calientes', 31),
(53, 2, 'Aguas Calientes - Machu Picchu - Cusco', 31),
(54, 1, 'Cuzco - Wayllabamba', 32),
(55, 2, 'Wayllabamba - Picaymayo', 32),
(56, 3, 'Picaymayo - Wiñaywayna', 32),
(57, 4, 'Wiñaywayna - Machu Picchu - Cuzco', 32),
(58, 1, 'Cuzco - Mollepata - Soraypampa', 33),
(59, 2, 'Soraypampa - Soyrococha – Huayracmachay – Colpapampa', 33),
(60, 3, 'Colpapampa – Playa Sahuayaco – Hidroeléctrica – Aguas Calientes', 33),
(61, 4, 'Aguas Calientes – Machu Picchu – Cusco', 33),
(62, 1, 'Cuzco - Mollepata - Soraypampa', 34),
(63, 2, 'Soraypampa - Soyrococha – Huayracmachay – Colpapampa', 34),
(64, 3, 'Colpapampa – Playa Sahuayaco – Santa Teresa - Cocalmayo', 34),
(65, 4, 'Santa Teresa - Hidroeléctrica – Aguas Calientes', 34),
(66, 5, 'Aguas Calientes – Machu Picchu – Cusco', 34),
(75, 1, 'CUSCO  SANTA MARIA  SANTA TERESA. (Bicicleta - Canotaje)', 43),
(76, 2, ' SANTA TERESA  AGUAS CALIENTES  MACHU PICCHU PUEBLO ( Tiroleza )', 43),
(77, 3, 'VISITA DE LA CIUDADELA DEL INCA MACHU PICCHU - VUELTA A CUSCO', 43),
(90, 1, 'Cusco -  Mollepata -  Laguna Humantay', 17),
(91, 2, 'Soraypampa - Abra Salkantay - Chaullay (22 km a pie, 10 horas)..', 17),
(92, 3, 'Chaullay - Santa Teresa (11 km a pie, 5 horas).', 17),
(93, 4, 'Santa Teresa - Aguas Calientes (16 km a pie, 6 horas).', 17),
(94, 5, 'Aguas Calientes - Machu Picchu - Cusco.', 17),
(97, 1, 'Cuzco - Chinchero - Maras - Moray', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itinerarydescription`
--

CREATE TABLE `itinerarydescription` (
  `IdItinerarioDescription` int(11) NOT NULL,
  `DescriptionItem` text NOT NULL,
  `ImgItem` text DEFAULT NULL,
  `IdItineraryDay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `itinerarydescription`
--

INSERT INTO `itinerarydescription` (`IdItinerarioDescription`, `DescriptionItem`, `ImgItem`, `IdItineraryDay`) VALUES
(4, '<h4>Andenes de Pisac</h4>\r\n<p>Nuestro transporte vendrá a sus respectivos alojamientos a las 8.00 am, para abordar el trasporte después nos dirigiremos rumbo al <strong>valle sagrado de los incas</strong>, donde realizaremos nuestra visita a el <strong>sitio arqueológico de Pisac.</strong></p>\r\n<p>Visitaremos los monumentos históricos de la ciudad y su <strong>mercado artesanal de Pisac</strong>.</p>\r\n<p><sub><em><strong>Es recomedable llevar dinero extra para realizar algunas compras.</strong></em></sub></p>', 'https://i.postimg.cc/tTjSwcZN/image.jpg', 2),
(5, '<h4>Almuezo Buffet Urubamba</h4>\r\n<p>Después de visitar la <strong>zona arqueologica de pisac</strong> nos dirigiremos hasta <strong>Urubamba</strong> donde tendremos un tiempo para poder almorzar, los que no tengan incluido el almuerzo podran hacer la compra en los <strong>restaurantes de la zona</strong>.</p>', 'https://i.postimg.cc/PqG2v74n/CXZwnu-AUw-AIqjn-X.jpg', 2),
(6, '<h4>Ruinas Ollantantaytambo</h4>\r\n<p>Continuaremos nuestra travesía camino por <strong>el Valle Sagrado</strong> visitando <strong>la increíble fortaleza de los incas</strong>. Ollantaytambo es una de las fortificaciones más importantes de <strong>los incas</strong> junto a sus majestuosos andes que lo rodean.</p>\r\n<p><sub><strong><em>Después de la visita guiada Los que reservaron Valle Sagrado Conexión Machu Picchu tendrán que aproximarse a la estación de ollantaytambo.</em></strong></sub></p>', 'https://i.postimg.cc/tJg5HGXW/image.jpg', 2),
(7, '<h4>Mercado Artesanal de Chinchero</h4>\r\n<p>realizaremos la ultima visita antes de regresar a <strong>la ciudad del Cusco</strong>: <strong>Chinchero y su mercado artesanal</strong>, exploraremos las ruinas de la Hacienda Real de Túpac Inca Yupanqui y el templo de la época colonial construido sobre <strong>cimientos incas</strong>, visitaremos<strong> el centro textil de chichero</strong> para explorar los distintos tejidos hechos con lana de alpaca de la zona.</p>\r\n<p>Finalmente tomaremos el bus de regreso, donde llegaremos alrededor de las 19:00 pm.</p>', 'https://i.postimg.cc/SNJrT41p/image.jpg', 2),
(8, '<h4>Museo de Qoricancha</h4>\r\n<p><strong>City Tour</strong> inicia en <strong>centro arqueologico de Qoricancha</strong> donde estaremos aproximadamente 1 hora visitando las ruinas y <strong>el templo de Qoricancha</strong></p>\r\n<p> </p>\r\n<p><sub><em><strong>Para acceder al Qoricancha se pagar S/.20.00.</strong></em></sub></p>', 'https://i.postimg.cc/zDsNnvHR/image.jpg', 3),
(9, '<h4>Sacsayhuaman</h4>\r\n<p>El<strong> centro arqueologico de Sacsayhuaman</strong> se encuentra localizado a  3 km de Cusco y es <strong>la ciudadela inca</strong> más impresionante. Hasta el día de hoy, en este lugar se celebra cada 24 de junio la <strong>fiesta del so</strong>l, conocida como el “<strong>Inti Raymi</strong>” donde estaremos visitandola por un promedio de 1 hora. Luego desde aquí continuaremos hacia <strong>el centro arqueologico de Qenko</strong> un lugar religioso también ceremonial.</p>', 'https://i.postimg.cc/SKfRP885/cusco-sacsayhuaman.jpg', 3),
(10, '<h4>Complejo Arqueologico Qenqo - Kenko</h4>\r\n<p>Qenqo se encuentra aproximadamente a 1 kilómetro de Sacsayhuaman.<br />Qenqo fue un <strong>centro ceremonial inca,</strong> las construcciones estaban dedicadas a la madre tierra.<br />Allí destaca un espacio llamado <strong>anfiteatro donde los incas</strong> debieron colocar a sus muertos en nichos trapezoidales.</p>\r\n<p>Su mayor atracción son sus laberintos subterráneos.</p>\r\n<p>Es más, <strong>Qenqo</strong> en quechua (<strong>lenguaje de los incas</strong>) significa ‘Laberinto’.</p>', 'https://i.postimg.cc/bJQJKBMq/image.jpg', 3),
(11, '<h4>Puca Pucara</h4>\r\n<p><strong>Puca Pucara</strong> es uno de los antiguos puestos de <strong>la Guardia Inca</strong> y paradas de descanso a lo largo de sus carreteras principales. Debido a encontrarse en una plataforma elevada se le dio la denominación de fortaleza. Actualmente es un lugar obligado de visita durante la estadía en <strong>la Ciudad Imperial</strong>.</p>', 'https://i.postimg.cc/QCNnh7Wm/puka-pukara-03.jpg', 3),
(12, '<h4>Tambomachay</h4>\r\n<p>nuestro ultimo destino es el <strong>centro arqueologico de Tambomachay</strong>.</p>\r\n<p>En este lugar, l<strong>os antiguos Incas adoraban el agua</strong> y se conoce como el manantial de la eterna juventud. este centro arqueologico es considerado uno de los adoratorios del primer “ceque” (enormes líneas imaginarias, que servían para organizar santuarios y huacas en los alrededores del Antisuyo, partían de la ciudad del Cusco).</p>', 'https://i.postimg.cc/Kcs5RCXx/datos-tambomachay.jpg', 3),
(13, '<p> </p>\r\n<h4>Ollantaytambo templo del sol</h4>\r\n<p>Empezara nuestro recorrido desde su hotel o hostal a las 7:00 am para empezar el viaje a bordo del minibús que nos llevará a <strong>Hidroeléctrica</strong>. Durante el camino haremos una parada de 20 minutos en el <strong>pueblo de Ollantaytambo</strong> donde tendran un tiempo libre, después de la seguiremos con nuestra ruta rumbo a <strong>Santa Teresa</strong></p>', 'https://i.postimg.cc/rsmy7g9q/3322826063-587bac0b16-b.jpg', 4),
(14, '<h4>Estacion Hidroelectrica</h4>\r\n<p><strong>Hidroeléctrica</strong> durante 4 horas aproximadamente llegando sobre la 2:00 pm para tener nuestro almuerzo. Llegando a la <strong>Hidroelectrica </strong>este punto del recorrido la carretera se termina, por lo que tendremos dos opciones para la llegada a<strong> Aguas Calientes</strong>: caminar por la ruta que aprovecha el trazado de las <strong>vías del tren</strong> (una caminata de dos horas y media a través de la selva) o tomar el tren a <strong>Aguas Calientes</strong> (<strong>Inca Rail</strong> o <strong>Peru Rail</strong> en <strong>clase Expedition</strong>) que hace un recorrido de 30 minutos.</p>', 'https://i.postimg.cc/RFhdktzH/camino-hidroelectrica-machu-picchu.jpg', 4),
(15, '<p> </p>\r\n<h4>Pueblo Aguas Calientes Dia</h4>\r\n<p>llegaremos con un transfer a <strong>Aguas Calientes</strong> y Una vez allí tendremos que caminar por 15 minutos mas al hotel donde tendremos nuestra cena y la charla con el guia donde acordaran el punto de encuentro para el día siguiente. Durante la noche tendrás tiempo para conocer el pueblo y disfrutar de los <strong>Baños termales</strong> (costo S/. 10.00).</p>', 'https://i.postimg.cc/sxZcXRxf/aguas-calientes-machu-picchu-peru.jpg', 4),
(16, '<p> </p>\r\n<h4>Pueblo Aguas Calientes Noche</h4>\r\n<p>En este dia nos levantaremos muy temprano para poder tomar desayuno, luego tendremos que caminar por aproximadamente 1 hora y 30 minutos a la <strong>ciudadela Inca de Machupicchu</strong> por aproximandamente unos 30 minutos. Al llegar disfrutaremos de uno de los mejores momentos del tour: ver la <strong>salida del sol</strong>.</p>', 'https://i.postimg.cc/pTSCRd21/35174752963-8293dcdb0f-b.jpg', 5),
(17, '<p> </p>\r\n<h4>Machu Picchu con Montaña Waynapicchu</h4>\r\n<p>Realizaremos la excursión con el Guía por <strong>la ciudadela de Machu picchu</strong>, durante 2 horas. Luego se les dará tiempo libre donde podran dar un paseo a la <strong>ciudadela</strong>, Los pasajeros que tengan la visita a la <strong>Montaña Machu picchu</strong> | <strong>Huaynapicchu</strong> tendran que dirigirse al ingreso donde tendran que subir durante 1 hora 30 minutos y visitar por un tiempo prudente.</p>', 'https://i.postimg.cc/dt3DfQWG/798iujboiugoihg654654654.jpg', 5),
(18, '<p> </p>\r\n<h4>Regreso de Machu Picchu </h4>\r\n<p>Luego de retornar de <strong>Machu Picchu</strong> tendremos que retornar caminado por unas 3 horas o tambien podria tomar el tren por 30 minutos hacia <strong>Hidroelectrica</strong>, Allí nos estará esperando el minibús con nuestro nombre, luego nos llevará de regreso a <strong>Cuzco</strong> por aproximadamente unas 6 horas con el transporte, donde llegaremos aproximadamente a <strong>Cusco</strong> como alrededor de las 10:00 pm el cual concluirá en la plaza <strong>San francisco</strong>.</p>', 'https://i.postimg.cc/Dzpfv9RM/machu-picchu-fenomeno-el-nino-fen-defensa-civil-cusco.jpg', 5),
(19, '<h4>Excursion Machu Picchu</h4>\r\n<p>Nuestro transfer le recogerá en su alojamiento de Cuzco a las 4:00 am con un minibús y nos pondremos rumbo a la estación de tren de Ollantaytambo. Desde allí tomaremos el tren de las 6:10 | 6:40 am que llega a Aguas Calientes. El trayecto en tren dura 1 hora y media, llegando a aguas calientes los esperar nuestro transfer para luego llevarnos a abordar el bus a Machupicchu.</p>', 'https://i.postimg.cc/Wb4jXPYV/tren360-1.jpg', 6),
(20, '<p> </p>\r\n<p>Realizaremos la excursión con el Guía por la ciudadela de Machupicchu, durante 2 horas. Luego se les dará tiempo libre donde podran dar un paseo a la ciudadela, Los pasajeros que tengan la visita a la Montaña Machupicchu | Huaynapicchu tendran que dirigirse al ingreso donde tendran que subir durante 1hr 30m y visitar por un tiempo prudente.</p>', 'https://i.postimg.cc/vHFDNCsN/Machu-Picchu-Full-Day-1.jpg', 6),
(21, '<p> </p>\r\n<p>Despues de la visita guiada y el tiempo libre en machupicchu o de la visita que realizaran a a la Montaña Machupicchu | Huaynapicchu Montaña tendremos que abordar el bus compartido con destino a Aguas Calientes, en aguas calientes podran visitar citios opcionales como (Aguas Termales, Museo, Centro Artesanal) y por la tarde tendremos que tomar el tren de regreso a Ollantaytambo.</p>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 6),
(22, '<p> </p>\r\n<p>Luego de las 2hrs en tren hasta ollantaytambo, cuando lleguemos a ollantaytambo los estara esperando un personal con un letrero con el logo de la empresa para llevarlo a abordar el bus que nos transportara hacia la ciudad del Cusco por aproximadamente unas 2hrs de viaje y se le dejara serca del hotel donde se encuentra hospedado.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 6),
(23, '<p> </p>\r\n<h4>Tren Cusco Aguas Calientes</h4>\r\n<p>Nuestro transporte pasara para recojerlos(10:00 AM) de sus respectivos hoteles para dirigirnos hacia <strong>Ollantaytanbo</strong> en coche o minibús con una duración de 1.20 h donde tomaremos el tren de las 12:58 pm que nos llevará a Pueblo de <strong>Machupicchu</strong> el cual es popularmente conocido como “<strong>Aguas calientes</strong>”, el viaje que realizaremos podremos visualizar los paisajes espectaculares.</p>', 'https://i.postimg.cc/gcsmV0D4/Tren-Vistadome-1.jpg', 7),
(24, '<p> </p>\r\n<h4>Hermoso Pueblo Aguas Calientes </h4>\r\n<p>Al llegar a <strong>Aguas Calientes</strong> nos estarán esperando en la estación de tren para llevarnos a nuestro hotel (<strong>Waynama</strong> Hostel o similar) donde tendremos una charla con el guia de <strong>machu picchu</strong> para poder coordinar la hora nos recogerán mañana para subir a <strong>Machu Picchu</strong>. Durante el resto del día tendrás tiempo para visitar <strong>Aguas Calientes</strong>, las aguas termales y disfrutar de los alrededores.</p>', 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 7),
(25, '<h4>Caminata  a Machu Picchu</h4>\r\n<p>El amanecer en <strong>aguas calientes</strong> sera temprano para poder tomar desayuno, luego tendremos que diregirnos a la estacion de buses donde abordaremos en bus a la <strong>ciudadela Inca de Machupicchu</strong> por aproximandamente unos 30 minutos.</p>', 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 8),
(26, '<p> </p>\r\n<h4>Machu Picchu Amanecer</h4>\r\n<p>Realizaremos la excursión con el Guía por <strong>la ciudadela de Machupicchu</strong>, durante 2 horas. Luego se les dará tiempo libre donde podran dar un paseo a la ciudadela, Los pasajeros que tengan la visita a la <strong>Montaña Machupicchu</strong> | <strong>Huaynapicchu</strong> tendran que dirigirse al ingreso donde tendran que subir durante 1hr 30m y visitar por un tiempo prudente.</p>', 'https://i.postimg.cc/X7pBR9Gr/Amanecer-Machu-Picchu-1.jpg', 8),
(27, '<p> </p>\r\n<h4>Aguas Calientes Bajada</h4>\r\n<p>Despues de la visita guiada y el tiempo libre en machupicchu o de la visita que realizaran a a la <strong>Montaña Machupicchu</strong> | <strong>Huaynapicchu Montaña</strong> tendremos que abordar el bus compartido con destino a <strong>Aguas Calientes</strong>, en aguas calientes podran visitar citios opcionales como (<strong>Aguas Termales</strong>, <strong>Museo</strong>, <strong>Centro Artesanal</strong>) y por la tarde tendremos que tomar el tren de regreso a <strong>Ollantaytambo</strong>.</p>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 8),
(28, '<p> </p>\r\n<h4>Cusco Retorno</h4>\r\n<p>Luego de las 2hrs en tren hasta <strong>ollantaytambo</strong>, cuando lleguemos a <strong>ollantaytambo</strong> los estara esperando un personal con un letrero con el logo de la empresa para llevarlo a abordar el bus que nos transportara hacia la ciudad del <strong>Cusco</strong> por aproximadamente unas 2 horas de viaje y se le dejara serca del hotel donde se encuentra hospedado.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 8),
(29, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Excursion a Valle Sagrado</h4>\r\n<p>realizaremos el tour a <strong>Valle Sagrado de los Incas</strong>, iniciaremos con el recojo del hotel a las 8:30 am, para luego dirigirnos hacia <strong>Pisaq</strong>, Visitaremos el <strong>complejo arqueol&oacute;gico</strong> y Tambi&eacute;n su <strong>Feria artesanal</strong> donde se podr&aacute;n comprar <strong>hermosas artesan&iacute;as</strong>.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/rmzqDtKd/pisac-3.jpg', 9),
(30, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Zona Arqueologica de Pisac Buffet</h4>\r\n<p>visitaremos la <strong>zona arqueologica</strong> de <strong>pisac</strong> nos dirigiremos hasta <strong>Urubamba</strong> donde tenemos incluido un <strong>almuerzo buffet</strong> y estaremos ahi durante 40 minutos.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/G2dsVNGR/Almuerzo-Buffet-Urubamba.jpg', 9),
(31, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Urubamba Atractivos Turisticos</h4>\r\n<p>continuaremos nuestro camino hasta <strong>Urubamba</strong>, donde disfrutaremos de una <strong>comida tradicional</strong> tipo buffet. seguiremos la ruta por el <strong>Valle Sagrado</strong> visitando la incre&iacute;ble <strong>fortaleza de Ollantaytambo</strong>, donde el gu&iacute;a nos explicar&aacute; su historia y tras la visita nos llevar&aacute; a la estaci&oacute;n para que puedas tomar el tren categor&iacute;a <strong>Expedition</strong> / Voyager hasta <strong>Aguas Calientes</strong> (incluido en el precio del tour).</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/252zjXgr/mapi-15.jpg', 9),
(32, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Aguas Calientes Relajacion</h4>\r\n<p>Luego de la visita al <strong>complejo arqueol&oacute;gico</strong> nos dirigiremos a la estaci&oacute;n de tren para abordar el tren de las 16:36pm de <strong>Ollantaytambo</strong> rumbo a <strong>Aguas Calientes</strong> por aproximadamente unas 2h de viaje, donde tendremos un transfer que nos llevara al hostal para el pernocte.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/fybST98g/estacion-Ollanta.jpg', 9),
(33, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Amanecer Machu Picchu</h4>\r\n<p>Hoy toca ver el amanecer en<strong> Machu Picchu</strong>. Para ello tendremos que despertarnos pronto, por lo que sobre las cuatro de la ma&ntilde;ana nos servir&aacute;n el desayuno en nuestro alojamiento y nos dirigiremos a la parada de bus que nos llevar&aacute; a la <strong>Ciudadela Inca</strong>.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 10),
(34, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h3>Machu Picchu Altura</h3>\r\n<p>Una vez all&iacute;, nos encontraremos con nuestro gu&iacute;a y accederemos a<strong> Machu Picchu</strong> para ver la salida del sol. Posteriormente realizaremos un recorrido gu&iacute;ado de dos horas y tendremos tiempo libre hasta la hora de comer. Los pasajeros que tengan la visita a la <strong>Monta&ntilde;a Machupicchu</strong> | <strong>Huaynapicchu</strong> tendran que dirigirse al ingreso donde tendran que subir durante 1hr 30m y visitar por un tiempo prudente.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/j2JKXq2S/mapi-23.jpg', 10),
(35, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Recorrido Machu Picchu y Huaynapicchu</h4>\r\n<p>Despues de la visita guiada y el tiempo libre en <strong>machu picchu</strong> o de la visita que realizaran a a<strong> la Monta&ntilde;a Machupicchu</strong> | <strong>Huaynapicchu</strong> Monta&ntilde;a tendremos que abordar el bus compartido con destino a <strong>Aguas Calientes</strong>, en aguas calientes podran visitar citios opcionales como (<strong>Aguas Termales</strong>, <strong>Museo</strong>,<strong> Centro Artesanal</strong>) y por la tarde tendremos que tomar el tren de regreso a <strong>Ollantaytambo</strong>.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 10),
(36, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>ollantaytambo Ruinas</h4>\r\n<p>Luego de las 2 horas,&nbsp;cuando lleguemos a <strong>ollantaytambo</strong> los estara esperando un personal con un letrero con el logo de la empresa para llevarlo a abordar el bus que nos transportara hacia la ciudad del <strong>Cusco</strong> por aproximadamente unas 2hrs de viaje y se le dejara serca del hotel donde se encuentra hospedado.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 10),
(37, '<p>Iniciaremos la aventura recogiéndote de tu hotel en <strong>Cusco</strong> para emprender nuestro viaje de aproximadamente de 40 minutos en nuestro transporte turístico rumbo al Poblado de <strong>Racchi</strong>. Aquí el guia nos dara unas pequeñas instrucciones de manejo y luego tendremos la oportunidad de practicar en las <strong>Cuatrimotos</strong> por 15 o 20 minutos.</p>', 'https://i.postimg.cc/nrDLRGr7/atv-1.jpg', 11),
(38, '<p>Una vez que estemos listos para la singular travesía, iniciaremos el recorrido con las <strong>Cuatrimotos</strong> hacia la <strong>salineras</strong>. en este punto visitaremos las minas de sal de <strong>Maras</strong> (a 50 mnts de <strong>Cusco</strong>). tendremos tiempo 1hr en el cual el guía nos explicará el proceso de extracción de la sal y tendremos tiempo para hacer fotografías y poder hacer compras de chocolates puros procesados con la <strong>Salinas de Maras</strong>.</p>', 'https://i.postimg.cc/Qxg5k1zw/atv-49.jpg', 11),
(39, '<p>Despues de la visita a las <strong>Minas de Sal</strong>, continuaremos el trayecto con las cuatrimotos al centro arqueológico de <strong>Moray</strong> donde el guia nos dara una Breve explicacion del lugar sobre como ahi los pobladores de la localidad empleaban para experimentar con los cultivos agrícolas debido a las variaciones climáticas. Despues de la visita retornaremos con las motos a <strong>Racchi</strong> para luego abordar nuestro transporte hacia cusco a las 12:30 pm.</p>', 'https://i.postimg.cc/JtCz9HHF/Quad-Bike-Maras-Moray-1.jpg', 11),
(40, '<p> </p>\r\n<h4>Ciclismo en cruspata</h4>\r\n<p>Lo recogeremos de su hotel en <strong>Cusco</strong> para emprender nuestro viaje de aproximadamente de 40 minutos en nuestro transporte turístico rumbo al <strong>Poblado de cruspata</strong>. Aquí el guia nos dara unas pequeñas instrucciones de manejo y luego tendremos la oportunidad de practicar con la bicicleta por 15 o 20 minutos.</p>', 'https://i.postimg.cc/y6mWwBTS/biking-5.jpg', 12),
(41, '<p> </p>\r\n<p><strong>Agricultura Moray</strong></p>\r\n<p>Despues de la visita a las <strong>minas de sal</strong>, continuaremos el trayecto con las bicicletas al <strong>centro arqueológico de Moray</strong> donde el guia nos dara una Breve explicacion del lugar sobre como ahi los pobladores de la localidad empleaban para experimentar con los cultivos agrícolas debido a las variaciones climáticas.</p>', 'https://i.postimg.cc/JtCz9HHF/Quad-Bike-Maras-Moray-1.jpg', 12),
(42, '<p><strong>Minas de sal</strong></p>\r\n<p>en este punto visitaremos las minas de <strong>sal de Maras</strong> (a 50 mnts de <strong>Cusco</strong>). tendremos tiempo 1hora en el cual el guía nos explicará el proceso de extracción de la <strong>sal</strong> y tendremos tiempo para hacer fotografías y poder hacer compras de chocolates puros procesados con la <strong>sal de maras</strong>, despues de la visita abordaremos con las bicicletas nuestro transporte hacia <strong>cusco</strong> a las 12:30 pm..</p>', 'https://i.postimg.cc/Qxg5k1zw/atv-49.jpg', 12),
(43, '<p> </p>\r\n<h4>Santa Maria Bicicleta Recorrido</h4>\r\n<p>Lo recojeremos de su hotel a las 06:30am ,comenzar la aventura en bus. llegaremos al <strong>Abra Málaga</strong> a las 11 am, que es nuestro punto de partida. Ahi nos alistaremos con los equipos de proteccion de bicicleta para luego comenzar la ruta en bicicleta de 65 km (3 horas) hasta <strong>Huamanmarca</strong>, donde nos esperará el transporte que nos llevara al pueblo de <strong>Santa Maria</strong>.</p>', 'https://i.postimg.cc/NM65xxzn/jungle-4113-20181113.jpg', 13),
(44, '<p> </p>\r\n<h4>Canotaje - Rafting Santa Maria</h4>\r\n<p>Almorazemos en <strong>Santa María</strong> para luego descansar y continuar la aventura, te ofrecemos dos opciones: uno seria de quedarse en el hostal a descarsar o de realizar el rafting hasta un punto muy cercano a <strong>Santa Maria</strong> \"<strong>rafting</strong> esta disponible en los extras($30.00)\". Una vez en <strong>Santa Maria</strong> nos volveremos a reunir todo el grupo para tener la cena para luego descanzar para el segundo dia.</p>', 'https://i.postimg.cc/1twNjSfJ/hakuimg8.jpg', 13),
(45, '<p> </p>\r\n<h4>Camino inca travesia</h4>\r\n<p>saldremos a las 6:00 a.m. luego de disfrutar un desayuno, iniciaremos la caminata que recorre parte del <strong>Camino Inca</strong>. Durante el camino podremos presenciar la diversa <strong>flora y fauna</strong> del lugar, además de observar muchas especies de aves y de una amplia gama de orquídeas y árboles frutales. Recuerda que esta <strong>zona del Cusco</strong>, ceja de <strong>selva de la Amazonas</strong>, tiene una de las más ricas áreas con <strong>diversidad biológica del planeta</strong>, en la pequeña aldea de <strong>Kellomayo</strong> tendremos un reparador almuerzo.</p>', 'https://i.postimg.cc/2jdZdzp5/jungle-4316-20181114.jpg', 14),
(46, '<p> </p>\r\n<h4>Santa Teresa Medicinal</h4>\r\n<p>Los baños termales de <strong>Santa Teresa</strong> (<strong>Cocalmayo</strong>) el cual son aguas medicinales y terapéuticas. Luego del relajante continuaremos con nuestra caminata 40 minutos hasta el poblado de <strong>Santa Teresa</strong> donde pasaremos nuestra segunda noche, acomodándonos en un hospedaje familiar.</p>', 'https://i.postimg.cc/hG9DFt1z/jungle-4534-20181114.jpg', 14),
(47, '<h4>Pueblo Santa Teresa</h4>\r\n<p>Descubrimiremos en el camino nuevas cosas sorprendentes. Luego de un confortable desayuno en el pueblo de <strong>santa teresa</strong> tendrán la oportunidad de realizar el <strong>Zip line</strong> o <strong>canoping</strong> Separándose (opcional - $30.00) del grupo temporalmente para luego reunirse nuevamente y continuar el recorrido rumbo a la <strong>hidroelectrica</strong> en bus con el grupo.</p>', 'https://i.postimg.cc/t48JQf0f/jungle-4602-20181115.jpg', 15),
(48, '<h4>Recorriendo Junto al Tren</h4>\r\n<p>Llegaremos a <strong>Hidroeléctrica</strong> y comeremos en uno de los restaurantes antes de la última etapa de nuestro camino: una ruta de 9 km que discurre junto a las vías del tren, situada en el fondo de un valle increíble en el la vegetación ya se ha transformado por completo en <strong>selva alta</strong> (aquí tendremos la opción de coger el tren, $30.00) o caminar por unas 3 horas hasta aguas calientes.</p>', 'https://i.postimg.cc/CKGzYNs4/bycar-13.jpg', 15),
(49, '<p> </p>\r\n<h4>Aguas Caliente a Machu Picchu</h4>\r\n<p>Nuestra llegada a <strong>Aguas Calientes</strong> dejaremos nuestro equipaje en el hospedaje y tendremos tiempo libre para conocer el pueblo, su mercado o simplemente relajarnos con un baño en sus <strong>aguas termales</strong> ($5.00). Al final de la tarde nos reuniremos de nuevo todo el grupo. Cenaremos en un restaurante y planearemos con e guia para el siguiente día, en el que visitaremos<strong> Machu Picchu</strong>.</p>', 'https://i.postimg.cc/sxfD4CSB/jungle-4774-20181115.jpg', 14),
(50, '<h4>Maravilla del mundo</h4>\r\n<p>El cuarto día alrededor de las 4:00 a.m. nos alistaremos para dirigimos a la <strong>ciudadela inka</strong> de <strong>Machu picchu</strong> una vía de subida por medio de la selva, teniendo así la oportunidad de ver el amanecer, luego de registrarse en el control del parque, 6:20 a.m. apreciaran<strong> la salida del sol</strong> mientras la suerte nos acompañe, También se ofrece la opción de subir en bus ($24.00 ida y vuelta).</p>', 'https://i.postimg.cc/BnvhtzXP/machu-picchu-turista.jpg', 16),
(51, '<p> </p>\r\n<h4>La Montaña de Machu Picchu</h4>\r\n<p>Visitaremos diversos lugares junto a nuestro guía profesional quien compartirá sus conocimientos durante dos horas y luego tendremos tiempo libre para hacer las fotográficas del recuerdo. Los pasajeros que tengan la visita a la <strong>Montaña Machupicchu</strong> | <strong>Huaynapicchu</strong> tendran que dirigirse al ingreso donde tendran que subir durante 1hr 30m y visitar por un tiempo prudente, luego de la visista tendremos que retornar a <strong>aguas calientes</strong> en bus o caminando.</p>', 'https://i.postimg.cc/9FqsgNpz/jungle-4787-20181116.jpg', 16),
(52, '<p> </p>\r\n<h4>Aguas Calientes y sus Maravillas</h4>\r\n<p>Despues del tiempo libre en<strong> machu picchu</strong> o de la visita que realizaran a a la Montaña <strong>Machupicchu</strong> | <strong>Huaynapicchu</strong> Montaña tendremos que abordar el bus compartido con destino a <strong>Aguas Calientes</strong>,podran visitar sitios opcionales como (<strong>Aguas Termales</strong>, <strong>Museo</strong>, <strong>Centro Artesanal</strong>) y por la tarde tendremos que tomar el tren de regreso a <strong>Ollantaytambo</strong>.</p>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 16),
(53, '<p> </p>\r\n<h4>Finales de la travesia Cusco</h4>\r\n<p>Luego de las 2horas en tren hasta <strong>ollantaytambo</strong>, cuando lleguemos a <strong>ollantaytambo</strong> los estara esperando un personal con un letrero con el logo de la empresa para llevarlo a abordar el bus que nos transportara hacia la <strong>ciudad del Cusco</strong> por aproximadamente unas 2horas de viaje y se le dejara serca del hotel donde se encuentra hospedado.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 16),
(54, '<p> </p>\r\n<h4>Caminata por Soraypampa</h4>\r\n<p>El recojo del hotel comenzará a las 4:30 am. Una vez que hayamos conocido a nuestros compañeros de viaje, abordaremos el transporte y viajaremos por unas dos horas hasta llegar a <strong>Mollepata</strong>. Ahi tendremos el desayuno en la frontera del municipio de <strong>Cuzco</strong> antes de continuar hacia <strong>Soraypampa</strong>, que seria punto de partida de nuestra caminata.</p>', 'https://i.postimg.cc/bJXtGF52/cuesta-santa-ana-cusco.jpg', 19),
(55, '<p> </p>\r\n<h4>Altura Laguna Humantay</h4>\r\n<p>Cuando lleguemos a <strong>Soraypampa</strong>, comenzaremos a ascender los 300 m hasta la <strong>laguna de Humantay</strong>, que nos llevara aproximadamente 1hr 30 minutos a pie. Cuando lleguemos a la laguna, seremos recompensados con un bello panorama de este lugar. No solo podremos admirar el glaciar del <strong>Salkantay</strong> (que alcanza los 5450 m snm), sino también la cordillera de <strong>Villcamba</strong>. Después de un descanso y tiempo libre de 1hr para caminar por el lugar, nos prepararemos para el descenso a Soraypampa.</p>', 'https://i.postimg.cc/TwNNPrRK/humantay-12.jpg', 19),
(56, '<h4>Cruse de Salkantay</h4>\r\n<p>Cuando retornemos a <strong>Soraypampa</strong>, podremos explorar el lugar un poco y visitar el sendero y la vista a la montaña de <strong>Salkantay</strong>. Se trata de la segunda montaña más grande de toda la región de <strong>Cusco</strong>. Después de nuestra corta visita, nos dirigiremos a mollepata para compartir un almuerzo tipo buffet antes de tomar nuevamente el bus, que nos llevara de regreso a <strong>Cusco</strong>. Llegaremos a la <strong>plaza san francisco</strong> sobre las 7:30 pm de la noche.</p>', 'https://i.postimg.cc/3NxJWX3v/tour-laguna-humantay-caballo.jpg', 19),
(57, '<p> </p>\r\n<h4>Comunidad Pitumarca</h4>\r\n<p>El recorrido empieza a las 4:30am de vuestro hotel en <strong>Cusco</strong> y, tras dos horas de transporte, llegaremos a <strong>Pitumarca</strong> para desayunar. Después continuaremos nuestro viaje hasta <strong>Chillihuani</strong> para comprar el boleto de acceso a la <strong>Montaña de colores</strong> (10 soles), ubicada en el corazón de <strong>la cordillera más larga del mundo</strong>.</p>', 'https://i.postimg.cc/PqMCxr10/camino-montana-siete-colores.jpg', 20),
(58, '<p> </p>\r\n<h4>Recorrido Montaña de Colores</h4>\r\n<p>Caminaremos durante 2 horas hasta llegar a la Montaña <strong>Vinicunca</strong>, es conocida como <strong>el cerro de los siete colores</strong>. Una vez que lleguemos alli, tendremos tiempo para disfrutar de los <strong>fascinantes colores de la montaña</strong> y echar un vistazo a la gran vista panorámica que tenemos desde aquí. También tendremos tiempo suficiente para tomarnos fotos. Despues de apreciar completamente la <strong>hermosa vista</strong>, comenzaremos nuestro descenso de regreso a <strong>Pitumarca</strong>.</p>', 'https://i.postimg.cc/wx5wt80X/colores-7.jpg', 20),
(59, '<p> </p>\r\n<h4>Pitumarca a Cusco</h4>\r\n<p>Estaremos de vuelta en <strong>Pitumarca</strong>, haremos una parada para tener nuestro almuerzo y tras un pequeño descanso nos prepararemos para nuestro <strong>retorno a Cusco</strong> donde estaremos llegando a las 6:30 pm.</p>', 'https://i.postimg.cc/pXxzP4N9/pitumarca.jpg', 20),
(60, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Ingreso a Camino Inca</h4>\r\n<p>Temprano por la ma&ntilde;ana y nos trasladar&aacute;n hasta la estaci&oacute;n, donde tomaremos el tren que nos llevar&aacute; hasta el km 104, donde empieza el <strong>Camino Inca.</strong> Ah&iacute; comienza una caminata de tres horas hasta la <strong>zona Arqueol&oacute;gica</strong> de <strong>Wi&ntilde;aywayna</strong>, est&aacute; a 2600 metros de altitud y se cree que fue el hogar de los antiguos sacerdotes de <strong>Machu Picchu</strong>.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/W1ZBffsX/incatrail-2.jpg', 21),
(61, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Puesta del Sol Camino Inca</h4>\r\n<p>Aprovecharemos para comer y descansar un poco y seguiremos andando por el <strong>Camino Inca</strong> hasta llegar a <strong>la Puerta del Sol</strong>, el acceso al recinto sagrado de <strong>Machu Picchu</strong>. Despu&eacute;s descenderemos en bus hasta llegar a <strong>Aguas Calientes</strong>, donde nos alojaremos en el hotel Vista <strong>Machu Picchu</strong>. Cenaremos y tendremos tiempo para descansar u opcionalmente visitar los ba&ntilde;os termales.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/43brX77c/incatrail-1.png', 21),
(62, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4>Zonas arqueologica Ollantaytambo</h4>\r\n<p>Nos levantaremos temprano para tener el desayuno en el hotel, luego nos dirigiremos a la estacion de buses para abordar y llegar a <strong>machupicchu</strong> para poder apreciar la salida del sol. tendremos 2 horas de visita guiada, teniendo luego tiempo suficiente para disfrutarlo por tu cuenta hasta la hora de descender caminando o en autob&uacute;s a <strong>Aguas Calientes</strong>. All&iacute; tomaremos el tren hasta <strong>Ollantaytambo</strong>, donde nos estar&aacute; esperando el transporte para trasladarnos a <strong>Cusco</strong>.</p>\r\n</body>\r\n</html>', 'https://i.postimg.cc/fykkhhbG/mapi-7.jpg', 22),
(63, '<h4>Rumbo a Piskacuchu</h4>\r\n<p>Te recojeremos en bus en tu alojamiento a las 4:00 am y comenzareos el tour que nos llevará a <strong>Piskacuchu</strong>, también conocido como “km 82”, a 2.700 metros de altura. Desde ese punto iniciaremos nuestra ruta por el <strong>Camino Inca</strong>.</p>', 'https://i.postimg.cc/4Nj7GYpw/camino-inca-4-dias.jpg', 23),
(64, '<p> </p>\r\n<h4> El Alto Urubamba</h4>\r\n<p>Continuaremos caminando a lo largo del <strong>río de Urubamba</strong> por el terreno llano hasta llegar a <strong>Miskay</strong>. Allí ascenderemos hasta un mirador en la zona más alta de la montaña para ver la impresionante <strong>ciudad Inca</strong> de <strong>Llactapata</strong>.</p>', 'https://i.postimg.cc/dt6c4thb/lakehgu.jpg', 23),
(65, '<p> </p>\r\n<h4>Caminando por Rios</h4>\r\n<p>Continuaremos la caminata por el valle creado por el <strong>río Kusichaca</strong>, un tramo de cinco horas donde disfrutaremos de las vistas, la flora y la fauna autóctona, hasta llegar finalmente al campamento en <strong>Wayllabamba</strong> (3.000m de altura).</p>', 'https://i.postimg.cc/sXTqZkJD/incatrail-6.jpg', 23),
(66, '<h4>Caminando entre Montañas</h4>\r\n<p>Desayunaremos y continuaremos para enfrentarnos al tramo más duro de la ruta, una abrupta subida donde el paisaje cambia de <strong>sierra</strong> a <strong>puno</strong> (paisaje muy seco con poca vegetación). Pasaremos por Abra <strong>Warmihuañusca</strong> (Paso de la Mujer Muerta - 4.200m de altura), en esta zona se pueden ver llamas y <strong>alpacas domesticadas</strong>.</p>', 'https://i.postimg.cc/rpYZ86NS/WAYLLABAMBA-TO-PACAYMAYO.jpg', 24),
(67, '<h4>La fauna de Pacaymayo</h4>\r\n<p>Cruzaremos un bosque nuboso donde habitan muchos tipos de <strong>aves típicas andinas</strong>. Finalmente bajaremos al valle <strong>Pacaymayo</strong> (3.600m de altura) donde acamparemos.</p>', 'https://i.postimg.cc/bwS4swdm/aves-camino-inca.jpg', 24),
(68, '<h4>Ruinas en la Montaña</h4>\r\n<p>Desde <strong>Pacaymayo</strong> subiremos al segundo paso de <strong>Abra Runkurakay</strong>, que además, también es un complejo <strong>arqueológico</strong> a 3.970 metros sobre el nivel de mar.</p>', 'https://i.postimg.cc/gk5mS05W/Inca-Trail.jpg', 25),
(69, '<h4>Ciudad de las Nubes</h4>\r\n<p>Bajaremos hacia <strong>Yanacocha</strong> y entraremos en el <strong>bosque nublado</strong> para llegar al hermoso complejo arqueológico de <strong>Sayacmarca</strong> (3.624m). Continuaremos la ruta pasando por un túnel Inca hasta llegar al <strong>complejo arqueológico Phuyupatamarca</strong>, que significa “<strong>ciudad sobre las nubes</strong>”, ya que está en el punto más alto de la montaña.</p>', 'https://i.postimg.cc/43brX77c/incatrail-1.png', 25),
(70, '<h4>Agricultura inca</h4>\r\n<p>Continuaremos la caminata por los largos escalones de piedra descendientes hasta llegar a <strong>Wiñaywayna</strong>, donde se encuentra un impresionante<strong> complejo inca</strong> en el que se dedicaban a la <strong>agricultura</strong>, donde acamparemos para pasar la noche.</p>', 'https://i.postimg.cc/W1ZBffsX/incatrail-2.jpg', 25),
(71, '<h4>La Ciudad Perdida</h4>\r\n<p>temprano empezaremos a caminar a lo largo de una ruta empedrada construida por los Incas que conecta directamente con <strong>Machu Picchu</strong>. Accederemos al recinto a través de <strong>la puerta del Sol</strong>, conocida como<strong> Inti Punku</strong>, y viviremos uno de los momento más mágicos del tour: observar el amanecer sobre<strong> la ciudadela sagrada de los incas</strong>.</p>', 'https://i.postimg.cc/NMZLYzrL/mapi-39.jpg', 26),
(72, '<h4>Santuario Inca</h4>\r\n<p>Despues haremos un recorrido de 2horas con un guía experto que nos explicará todos los detalles de este increíble lugar. Para finalizar tendremos tiempo libre para disfrutar por nuestra cuenta y podremos visitar lugares como <strong>el Templo de la Luna</strong> o el impresionante Puente Inca.</p>', 'https://i.postimg.cc/7Y0JPw94/mapi-18.jpg', 26),
(73, '<h4>Paisajes por tren</h4>\r\n<p>Alrededor de la 13:00 pm tomaremos el bus a <strong>Aguas Calientes</strong>, donde tendremos tiempo libre para poder almorzar, despues del almuerzo tendremos tiempo para visitar el pueblo de aguas calientes para después tomar el tren de vuelta a<strong> la ciudad de Cuzco</strong>.</p>', 'https://i.postimg.cc/PqPkHnwj/trenacusco.jpg', 26),
(74, '<h4>Caminata por Pueblos</h4>\r\n<p>Alrededor de las 4:30 am nos recogerán en nuestro alojamiento en <strong>Cusco</strong> para poner rumbo a <strong>Mollepata</strong>, que se encuentra a 2900 metros de altitud. Allí desayunaremos y al finalizar nos dirigiremos a <strong>Marcocasa</strong>, población desde la que comenzaremos nuestra caminata hasta llegar a <strong>Soraypampa</strong>, donde tendremos nuestro almuerzo.</p>', 'https://i.postimg.cc/5tDx4g0n/soraypampa.jpg', 27),
(75, '<h4>Recorrido ala Laguna Humantay</h4>\r\n<p>Despues del almuerzo continuaremos hasta la preciosa <strong>Laguna Humantay</strong>, allí disfrutaremos de unas vistas increibles de la laguna con el nevado al fondo, creando un paisaje que está considerado uno de los más bellos de los andes. Volveremos al campamento de <strong>Soraypampa</strong> donde cenaremos y descansaremos hasta la mañana siguiente.</p>', 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 27),
(76, '<h4>Recorriendo Cordilleras </h4>\r\n<p>Hoy nos levantaremos temprano y tras el desayuno comenzaremos el ascenso al paso <strong>Salkantay</strong>, que se encuentra a 4750 msnm, el punto más elevado de toda la ruta, desde donde podremos observar los preciosos valles y los imponentes picos nevados. Luego comenzaremos nuestro descenso hasta el poblado <strong>Huayracmachay</strong> donde tendremos tiempo para comer y recuperar fuerzas para seguir hasta nuestro campamento en <strong>Colpapampa</strong>, donde cenaremos y pasaremos la noche.</p>', 'https://i.postimg.cc/50N3c6Wn/salkantay-5.jpg', 28),
(77, '<h4>Un valle con rieles</h4>\r\n<p>Nos levantaremos temprano y después del desayuno comenzaremos nuestra caminata hasta el pequeño p<strong>ueblo de La Playa Sahuayaco</strong> atravesando <strong>el Valle de Santa Teresa</strong>. Durante la caminata estaremos en pleno contacto con la naturaleza, veremos ríos, cascadas, preciosas orquídeas silvestres y plantaciones de café y plátano entre otros. Después de comer, un bus privado nos llevará hasta Santa Teresa donde tomaremos un transporte hasta <strong>Hidroeléctrica</strong> desde donde continuaremos a pie otras 3 horas siguiendo la línea del tren hasta llegar a <strong>Aguas Calientes</strong>.</p>', 'https://i.postimg.cc/76qbL2Br/bycar-14.jpg', 29),
(78, '<h4>Pueblo Aguas Calientes</h4>\r\n<p>Una vez en <strong>Aguas Calientes</strong> dispondrás de tiempo libre antes de cenar para descansar o caminar por el pueblo. Esta noche nos alojaremos en nuestro hostal con habitación y baño privados. Nos acostaremos temprano ya que al día siguiente nos espera una de los momentos más especiales de nuestro tour, la imponente fortaleza de <strong>Machu Picchu</strong>.</p>', 'https://i.postimg.cc/cCHHRNyD/bycar-21.jpg', 29),
(79, '<h4>Caminata a lo Maravilloso</h4>\r\n<p>Hoy nos levantaremos muy temprano (5 am) para comenzar el quinto y último día del <strong>trek a Salkantay</strong> con una caminata de aproximadamente 1 hora y media hasta <strong>Machu Picchu</strong>. Disfrutaremos de una visita guiada de 2 horas por la ciudadela, tras la cual tendremos tiempo libre para explorarla por nuestra cuenta y poder ver con calma<strong> la histórica ciudadela</strong>.</p>', 'https://i.postimg.cc/BZKtgJ77/reglamento-machu-picchu.jpg', 30),
(80, '<p><strong>Fin del Recorrido Misterioso</strong></p>\r\n<p>Al finalizar la visita regresaremos a <strong>Aguas Calientes</strong> (si te apetece podrás regresar en bus para evitar la caminata, comprando allí el ticket por 12 usd por trayecto), tendremos tiempo para comer y tomaremos el tren que nos llevará hasta <strong>Ollantaytambo</strong>, donde nos estará esperando nuestro transporte que nos llevará a <strong>Cusco</strong>.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 30),
(81, '<h4>Chinchero Textile Center</h4>\r\n<p>The transport will pass by your lodgings and we will begin our course to Chinchero (textile center), which is located to approximately 40 minutes of the city of Cusco, lodges a textile center where we will be able to see how they process the wool of flame and we will be able to see the different weaves.</p>\r\n<p> </p>\r\n<p><sub><em><strong>In the place you can buy the crafts that offer </strong></em></sub></p>', 'https://i.postimg.cc/XqcPVs8f/image.jpg', 31),
(82, '<h4>Maras Saltpans</h4>\r\n<p>We will make a guided tour of <strong>the salt mines of Maras</strong> (is located 50 minutes from Cusco). We will have approximately 30 minutes, in which our guide will explain the process of extracting salt and we will have time to take photographs and make purchases of pure chocolates processed with salt maras.</p>\r\n<p> </p>\r\n<h4><sub><em><strong>During our guided visit we will be able to see the extraction of salt.</strong></em></sub></h4>', 'https://i.postimg.cc/sxvTFh7L/tour-salineras-cusco-800x600.jpg', 31),
(83, '<h4>Andenes de Moray</h4>\r\n<p>After the visit to the salt mines we will visit the archaeological center of Moray, which are like terraces or agricultural terraces that are superimposed concentrically taking the form of a gigantic amphitheater. After the visit we will return to Cusco at 3:00 pm.</p>\r\n<p> </p>\r\n<p><sub><em><strong>The tour ends in San Francisco 2 blocks from the Plaza de Armas of Cusco.</strong></em></sub></p>', 'https://i.postimg.cc/QdmJXHTW/image.jpg', 31),
(84, '<h4>Andenes de Pisac</h4>\r\n<p>Our transport will come to their respective lodgings at 8.00 am, to board the transport then we will head towards the <strong>Sacred Valley of the Incas</strong>, where we will make our first stop at <strong>the archaeological site of Pisac.</strong></p>\r\n<p> </p>\r\n<p>Pisac is located in the interior of the Sacred Valley, we will visit the historical monuments of the city and its handicraft market of Pisac.</p>\r\n<p> </p>\r\n<p><sub><em><strong>It is recommended to bring extra money to make some purchases in Pisac.</strong></em></sub></p>', 'https://i.postimg.cc/MZCj8zF0/pisac-1.jpg', 32),
(85, '<h4>Lunch Buffet Urubamba</h4>\r\n<p>After visiting <strong>the archaeological zone of pisac</strong> we will go to Urubamba where we will have 40 minutes for lunch, those who do not have lunch included can do the shopping in <strong>restaurants in the area</strong>.</p>', 'https://i.postimg.cc/PqG2v74n/CXZwnu-AUw-AIqjn-X.jpg', 32),
(86, '<h4>Ollantantaytambo Ruins</h4>\r\n<p>We will continue our journey through the Sacred Valley visiting the incredible fortress of the Incas. Ollantaytambo is one of the most important fortifications of the ancient Incas along with its majestic andes that surround it.</p>\r\n<p><sub><em><strong>After the guided tour Those who booked Sacred Valley Connection Machu Picchu will have to approach the station of ollantaytambo </strong></em></sub></p>', 'https://i.postimg.cc/tJg5HGXW/image.jpg', 32),
(87, '<h4>Chinchero Handicrafts Market</h4>\r\n<p>We will make the last visit before returning to Cusco: Chinchero and its handicraft market, we will explore the ruins of the Royal Hacienda of Túpac Inca Yupanqui and the temple of the colonial era built on Inca foundations, we will visit the chichero textile center to explore the different fabrics made with alpaca wool in the area. Finally, we will take the bus back, where we will arrive around 19:00 pm.</p>', 'https://i.postimg.cc/SNJrT41p/image.jpg', 32),
(88, '<h4>Qoricancha Museum</h4>\r\n<p>The City Tour starts at the archaeological center of Qoricancha where we will be approximately 1 hour visiting the ruins and the temple of Qoricancha.</p>\r\n<p><em><strong><sub>To access the Qoricancha you must pay S/.20.00.</sub></strong></em></p>', 'https://i.postimg.cc/zDsNnvHR/image.jpg', 33),
(89, '<h4>Sacsayhuaman</h4>\r\n<p>The archaeological center of Sacsayhuaman is located only 3 km from Cusco and is the most impressive Inca citadel. To this day, this place is celebrated every June 24th the feast of the sun, known as the \"Inti Raymi\" where we will be visiting for an average of 1 hour. Then from here we will continue to the archaeological center of Qenko a religious and ceremonial place.</p>', 'https://i.postimg.cc/SKfRP885/cusco-sacsayhuaman.jpg', 33),
(90, '<h4>Qenqo Archaeological Complex - Kenko</h4>\r\n<p>Qenqo is located approximately 1 kilometer from Sacsayhuaman.</p>\r\n<p>Qenqo was an Inca ceremonial center whose constructions were dedicated to mother earth.</p>\r\n<p>There is a space called an amphitheater where the Incas had to place their dead in trapezoidal niches.</p>\r\n<p> </p>\r\n<p>Its main attraction is its underground labyrinths. Moreover, Qenqo in Quechua (language of the Incas) means \'Labyrinth\'.</p>', 'https://i.postimg.cc/bJQJKBMq/image.jpg', 33),
(91, '<h4>Puka Pukara</h4>\r\n<p>Puka Pukara is one of the former Inca Guard posts and rest stops along its main roads. Due to being in an elevated platform it was given the denomination of fortress. It is now a must-see during your stay in the Imperial City.</p>', 'https://i.postimg.cc/QCNnh7Wm/puka-pukara-03.jpg', 33),
(92, '<h4>Tambomachay</h4>\r\n<p>The last destination of our tour is the archaeological center of Tambomachay. In this place, the ancient Incas adored water and it is known as the spring of eternal youth. This archaeological center is considered one of the adoratories of the first \"ceque\" (enormous imaginary lines, that served to organize sanctuaries and huacas in the surroundings of the Antisuyo, departed from the city of Cusco).</p>', 'https://i.postimg.cc/6qRP18XT/tambomachay-1.jpg', 33),
(93, '<p> </p>\r\n<h4>Ollantaytambo temple of the sun</h4>\r\n<p>We will start our tour from your hotel or hostel at 7:00 am to start the journey aboard the minibus that will take us to <strong>Hidroeléctrica</strong>. During the way we will make a stop of 20 minutes in the <strong>town of Ollantaytambo</strong> where you will have some free time, after that we will continue with our route towards <strong>Santa Teresa</strong>.</p>', 'https://i.postimg.cc/rsmy7g9q/3322826063-587bac0b16-b.jpg', 34),
(94, '<h4>Hydroelectric Station</h4>\r\n<p><strong>Hydroelectric</strong> for approximately 4 hours arriving around 2:00 pm to have our lunch. Arriving at the <strong>Hydroelectric</strong> this point of the journey the road ends, so we will have two options for the arrival at <strong>Aguas Calientes</strong>: walking the route that takes advantage of the layout of the train tracks (a walk of two and a half hours through the jungle) or taking the train to Aguas Calientes (<strong>Inca Rail</strong> or <strong>Peru Rail</strong> in <strong>Expedition class</strong>) which makes a 30 minute journey.</p>', 'https://i.postimg.cc/RFhdktzH/camino-hidroelectrica-machu-picchu.jpg', 34),
(95, '<h4>Pueblo Aguas Calientes Day</h4>\r\n<p>We will arrive with a transfer to <strong>Aguas Calientes</strong> and once there we will have to walk for 15 minutes more to the hotel where we will have our dinner and talk with the guide where they will agree on the meeting point for the next day. During the night you will have time to get to know the town and enjoy the <strong>thermal baths</strong> (cost S/. 10.00).</p>', 'https://i.postimg.cc/sxZcXRxf/aguas-calientes-machu-picchu-peru.jpg', 34),
(96, '<p> </p>\r\n<h4>Pueblo Aguas Calientes Night</h4>\r\n<p>On this day we will get up very early to have breakfast, then we will have to walk for about 1 hour and 30 minutes to the <strong>Inca citadel of Machupicchu</strong> for about 30 minutes. Upon arrival we will enjoy one of the best moments of the tour: <strong>watching the sunrise.</strong></p>', 'https://i.postimg.cc/pTSCRd21/35174752963-8293dcdb0f-b.jpg', 35),
(97, '<h4>Machu Picchu with Waynapicchu Mountain</h4>\r\n<p>We will make the excursion with the guide through the citadel of Machu Picchu, during 2 hours. Then they will be given free time where they will be able to give a stroll to the <strong>citadel</strong>, The passengers that have the visit to the <strong>Mountain Machu picchu | Huaynapicchu</strong> will have to go to the entrance where they will have to raise during 1 hour 30 minutes and to visit for a prudent time.</p>', 'https://i.postimg.cc/dt3DfQWG/798iujboiugoihg654654654.jpg', 35),
(98, '<p><strong>Return from Machu Picchu </strong></p>\r\n<p>After returning from <strong>Machu Picchu</strong> we will have to walk back for about 3 hours or you can also take the train for 30 minutes to <strong>Hidroelectrica</strong>, there we will be waiting for the minibus with our name, then will take us back to <strong>Cuzco</strong> for about 6 hours with the transport, where we will arrive approximately to <strong>Cusco</strong> around 10:00 pm which will end in the <strong>Plaza San Francisco.</strong></p>', 'https://i.postimg.cc/Dzpfv9RM/machu-picchu-fenomeno-el-nino-fen-defensa-civil-cusco.jpg', 35),
(99, '<h4>Excursion Machu Picchu</h4>\r\n<p>Our transfer will pick you up at your accommodation in Cuzco at 4:00 am with a minibus and we will head to Ollantaytambo train station. From there we will take the 6:10 | 6:40 am train to Aguas Calientes. The journey by train takes 1 hour and a half, arriving at Aguas Calientes, waiting for our transfer to then take us to board the bus to Machupicchu.</p>', 'https://i.postimg.cc/Wb4jXPYV/tren360-1.jpg', 36);
INSERT INTO `itinerarydescription` (`IdItinerarioDescription`, `DescriptionItem`, `ImgItem`, `IdItineraryDay`) VALUES
(100, '<p>We will make the excursion with the Guide through the citadel of Machupicchu, during 2 hours. Then they will be given free time where they will be able to give a stroll to the citadel, The passengers who have the visit to the Mountain Machupicchu | Huaynapicchu will have to go to the entrance where they will have to raise during 1hr 30m and to visit for a prudent time.</p>', 'https://i.postimg.cc/vHFDNCsN/Machu-Picchu-Full-Day-1.jpg', 36),
(101, '<p>After the guided tour and free time in Machu Picchu or the visit to Machupicchu Mountain | Huaynapicchu Montaña we will have to board the shared bus to Aguas Calientes, in hot waters you can visit optional sites such as (Hot Springs, Museum, Artisan Center) and in the afternoon We will have to take the train back to Ollantaytambo.</p>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 36),
(102, '<p>After 2 hours by train to Ollantaytambo, when we arrive in Ollantaytambo, a staff with a company logo sign will be waiting to take you to board the bus that will transport us to the city of Cusco for approximately 2 hours of travel and you will be will leave the hotel where you are staying.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 36),
(103, '<h4>Train Cusco Aguas Calientes</h4>\r\n<p>Our transportation will pick you up (10:00 AM) from your respective hotels to go to <strong>Ollantaytanbo</strong> by car or minibus with a duration of 1.20 h where we will take the train at 12:58 pm that will take us to Pueblo de <strong>Machupicchu</strong> which is popularly known as \"<strong>Aguas Calientes</strong>\", the trip we will make we will be able to visualize the spectacular landscapes.</p>', 'https://i.postimg.cc/gcsmV0D4/Tren-Vistadome-1.jpg', 37),
(104, '<h4>Beautiful Pueblo Aguas Calientes </h4>\r\n<p>When we arrive to <strong>Aguas Calientes</strong> they will be waiting for us at the train station to take us to our hotel (<strong>Waynama</strong> Hostel or similar) where we will have a talk with the <strong>machu picchu</strong> guide to be able to coordinate the time they will pick us up tomorrow to go up to <strong>Machu Picchu</strong>. During the rest of the day you will have time to visit <strong>Aguas Calientes</strong>, the hot springs and enjoy the surroundings.</p>', 'https://i.postimg.cc/4dfmBcRN/Aguas-Calientes.jpg', 37),
(105, '<h4>Walk to Machu Picchu</h4>\r\n<p>The sunrise in <strong>hot water</strong> will be early to have breakfast, then we will have to go to the bus station where we will board a bus to the Inca citadel of Machupicchu for about 30 minutes.</p>', 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 38),
(106, '<h4>Machu Picchu Sunrise</h4>\r\n<p>We will make the excursion with the Guide through the <strong>citadel of Machupicchu,</strong> during 2 hours. Then they will be given free time where they will be able to give a stroll to the citadel, The passengers who have the visit to the Mountain <strong>Machupicchu | Huaynapicchu</strong> will have to go to the entrance where they will have to raise during 1hr 30m and to visit for a prudent time.</p>', 'https://i.postimg.cc/X7pBR9Gr/Amanecer-Machu-Picchu-1.jpg', 38),
(107, '<p> </p>\r\n<h4>Aguas Calientes Bajada</h4>\r\n<p>After the guided tour and free time in machupicchu or the visit to the<strong> Mountain Machupicchu Huaynapicchu Mountain</strong> we will have to board the shared bus to <strong>Aguas Calientes</strong>, in hot waters you can visit optional sites such as (<strong>Hot Springs, Museum, Craft Center</strong>) and in the afternoon we will have to take the train back to <strong>Ollantaytambo</strong>.</p>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 38),
(108, '<h4>Cusco Return</h4>\r\n<p>After 2 hours by train to <strong>ollantaytambo</strong>, when we arrive at <strong>ollantaytambo</strong> will be waiting for a staff with a sign with the logo of the company to take you to board the bus that will transport us to the city of <strong>Cusco</strong> for about 2 hours of travel and you will be left serca of the hotel where you are staying.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 38),
(109, 'This day we will make the tour to the Sacred Valley of the Incas, We started picking up the hotel at 8:30 am, and then head towards Pisaq, we will visit the archaeological complex and also its Craft Fair where you can buy beautiful crafts.', 'https://i.postimg.cc/rmzqDtKd/pisac-3.jpg', 39),
(110, 'After visiting the archaeological zone of Pisac we We will go to Urubamba where we have included a Buffet lunch and we will be there for 40 minutes.', 'https://i.postimg.cc/G2dsVNGR/Almuerzo-Buffet-Urubamba.jpg', 39),
(111, 'Then we will continue our way to Urubamba, where we will enjoy a traditional buffet meal. We will finish the route through the Sacred Valley visiting the incredible fortress of Ollantaytambo, where the guide will explain its history and after the visit will take us to the station so you can take the Expedition / Voyager category train to Aguas Calientes (included in the tour price ).', 'https://i.postimg.cc/252zjXgr/mapi-15.jpg', 39),
(112, 'After the visit to the archaeological complex we will go to the train station to board the 16:36 pm train from Ollantaytambo towards Aguas Calientes for approximately 2h of travel, where we will have a transfer that will take us to the hostel for the night.', 'https://i.postimg.cc/fybST98g/estacion-Ollanta.jpg', 39),
(113, 'Today we will have the opportunity to live a magical moment: see the sunrise in Machu Picchu. For this we will have to wake up soon, so about four in the morning they will serve us breakfast in our accommodation and we will go to the bus stop that will take us to the Inca Citadel (keep in mind that it is the only bus line that operate to make this journey and there are usually people waiting).\r\n\r\n', 'https://i.postimg.cc/N01HNYyb/Estacion-Buses-Aguas-Calientes.jpg', 40),
(114, 'Once there, we will meet our guide and access Machu Picchu to see the sunrise. Later we will take a guided tour of two hours and have free time until lunchtime. Passengers who visit Machupicchu Mountain | Huaynapicchu will have to go to the entrance where they will have to climb for 1hr 30m and visit for a prudent time.', 'https://i.postimg.cc/j2JKXq2S/mapi-23.jpg', 40),
(115, 'After the guided tour and free time in Machu Picchu or the visit to Machupicchu Mountain | Huaynapicchu Montaña we will have to board the shared bus to Aguas Calientes, in hot waters you can visit optional sites such as (Hot Springs, Museum, Artisan Center) and in the afternoon We will have to take the train back to Ollantaytambo.', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 40),
(116, 'After 2 hours by train to Ollantaytambo, when we arrive in Ollantaytambo, a staff with a company logo sign will be waiting to take you to board the bus that will transport us to the city of Cusco for approximately 2 hours of travel and you will be will leave the hotel where you are staying.', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 40),
(117, '<p>We will begin the adventure by picking you up from your hotel in <strong>Cusco</strong> to begin our journey of approximately 40 minutes in our tourist transport to the town of <strong>Racchi</strong>. Here the guide will give us some small driving instructions and then we will have the opportunity to practice on the <strong>ATVs</strong> for 15 or 20 minutes.</p>', 'https://i.postimg.cc/nrDLRGr7/atv-1.jpg', 41),
(118, '<p>At this point we will visit the salt mines of <strong>Maras</strong> (50 mnts from <strong>Cusco</strong>). we will have 1hr time in which the guide will explain us the process of extraction of the salt and we will have time to make photographs and to be able to make purchases of pure chocolates processed with the salt of <strong>Maras</strong>.</p>', 'https://i.postimg.cc/Qxg5k1zw/atv-49.jpg', 41),
(119, '<p>After the visit to the salt mines, we will continue the journey with the <strong>ATVs</strong> to the <strong>archaeological</strong> center of <strong>Moray</strong> where the guide will give us a brief explanation of the place on how the locals used to experiment with agricultural crops due to climatic variations. after the visit we will return with the <strong>motorcycles</strong> to <strong>Racchi</strong> and then board our transport to <strong>Cusco</strong> at 12:30 pm.</p>', 'https://i.postimg.cc/JtCz9HHF/Quad-Bike-Maras-Moray-1.jpg', 41),
(120, '<h4>Cruspathic Cycling</h4>\r\n<p>We will pick you up from your hotel in <strong>Cusco</strong> to begin our journey of approximately 40 minutes in our tourist transport to the <strong>Cruspata Village</strong>. Here the guide will give us some small driving instructions and then we will have the opportunity to practice with the bicycle for 15 or 20 minutes.</p>', 'https://i.postimg.cc/y6mWwBTS/biking-5.jpg', 42),
(121, '<p> </p>\r\n<h4>Agriculture Moray</h4>\r\n<p>After the visit to the<strong> salt mines</strong>, we will continue the journey with the bicycles to the <strong>archaeological center of Moray</strong> where the guide will give us a brief explanation of the place on how the locals used to experiment with agricultural crops due to climatic variations.</p>', 'https://i.postimg.cc/JtCz9HHF/Quad-Bike-Maras-Moray-1.jpg', 42),
(122, '<h4>Salt mines</h4>\r\n<p>At this point we will visit the <strong>salt mines of Maras</strong> (50 mnts from <strong>Cusco</strong>). we will have 1 hour in which the guide will explain us the process of extraction of the salt and we will have time to make photographs and to be able to make purchases of pure chocolates processed with the salt of <strong>maras</strong>, after the visit we will board with the bicycles our transport towards <strong>Cusco</strong> at 12:30 pm.</p>', 'https://i.postimg.cc/Qxg5k1zw/atv-49.jpg', 42),
(123, '<h4>Santa Maria Bike Tour</h4>\r\n<p>We will pick you up from your <strong>hotel</strong> at 06:30am, start the adventure bus. We will arrive at <strong>Abra Malaga</strong> at 11 am, which is our starting point. There we will get ready with the bicycle protection equipment and then start the 65 km (3 hours) <strong>bicycle</strong> route to <strong>Huamanmarca</strong>, where the transport that will take us to the village of <strong>Santa Maria</strong> will be waiting for us.</p>', 'https://i.postimg.cc/NM65xxzn/jungle-4113-20181113.jpg', 43),
(124, '<h4>Rafting - Rafting Santa Maria</h4>\r\n<p>Let\'s have lunch in <strong>Santa Maria</strong> and then rest and continue the adventure, we offer two options: one would be to stay in the hostel to unload or <strong>rafting</strong> to a point very close to <strong>Santa Maria</strong> \"rafting is available in the extras ($ 30.00)\". Once in <strong>Santa Maria</strong> we will meet again the whole group to have dinner and then rest for the second day.</p>', 'https://i.postimg.cc/1twNjSfJ/hakuimg8.jpg', 43),
(125, '<h4>Inca Traverse Trail</h4>\r\n<p>we will leave at 6:00 a.m. after enjoying a breakfast, we will begin the walk that crosses part of the <strong>Inca Trail</strong>. On the way we will be able to witness the diverse flora and fauna of the place, in addition to observing many species of birds and a wide range of orchids and fruit trees. Remember that this area of <strong>Cusco</strong>, the edge of the <strong>Amazon jungle</strong>, has one of the richest areas with biological diversity on the planet, in the small village of <strong>Kellomayo</strong> we will have a refreshing lunch.</p>', 'https://i.postimg.cc/2jdZdzp5/jungle-4316-20181114.jpg', 44),
(126, '<h4>Saint Teresa Medicinal</h4>\r\n<p>The thermal baths of <strong>Santa Teresa</strong> (<strong>Cocalmayo</strong>) which are medicinal and therapeutic waters. After the relaxing we will continue with our 40 minute walk to the town of <strong>Santa Teresa</strong> where we will spend our second night in a family lodge. </p>', 'https://i.postimg.cc/hG9DFt1z/jungle-4534-20181114.jpg', 44),
(127, '<h4>Aguas Caliente to Machu Picchu</h4>\r\n<p>Our arrival to <strong>Aguas Calientes</strong> will leave our luggage at the lodge and we will have free time to know the town, its market or simply relax with a bath in its hot springs ($5.00). At the end of the afternoon we will meet again the whole group. We will have dinner in a restaurant and plan with the guide for the next day, when we will visit <strong>Machu Picchu</strong>.</p>', 'https://i.postimg.cc/sxfD4CSB/jungle-4774-20181115.jpg', 45),
(128, '<p> </p>\r\n<h4>Santa Teresa Village</h4>\r\n<p>We will discover surprising new things along the way. After a comfortable breakfast in the town of <strong>Santa Teresa</strong> you will have the opportunity to make the <strong>Zip line</strong> or <strong>canoping</strong> separating (optional - $30.00) from the group temporarily and then meet again and continue the journey to the <strong>hydroelectric</strong> bus with the group.</p>', 'https://i.postimg.cc/t48JQf0f/jungle-4602-20181115.jpg', 45),
(129, '<h4>Travelling by the Train</h4>\r\n<p>We will arrive at <strong>Hidroeléctrica</strong> and eat in one of the restaurants before the last stage of our way: a 9 km route that runs next to the train tracks, located at the bottom of an incredible valley in which the vegetation has already completely transformed into <strong>high jungle</strong> (here we will have the option to take the train, $30.00) or <strong>walk</strong> for about 3 hours to hot water.</p>', 'https://i.postimg.cc/CKGzYNs4/bycar-13.jpg', 45),
(134, '<h4>Wonder of the world</h4>\r\n<p>On the fourth day around 4:00 a.m. we will get ready to head to the <strong>Inca citadel</strong> of <strong>Machu Picchu</strong> a way up through the jungle, thus having the opportunity to see the sunrise, after registering at the park control, 6:20 a.m. will appreciate <strong>the sunrise</strong> while luck accompanies us, We also offer the option of boarding the bus ($24.00 roundtrip).</p>', 'https://i.postimg.cc/BnvhtzXP/machu-picchu-turista.jpg', 46),
(135, '<h4>Machu Picchu Mountain</h4>\r\n<p>We will visit several places with our professional guide who will share his knowledge for two hours and then we will have free time to make the photographs of the memory. The passengers who have the visit to the <strong>Mountain Machupicchu | Huaynapicchu</strong> will have to go to the entrance where they will have to climb for 1hr 30m and to visit for a prudent time, after the visit we will have to return to hot waters in bus or walking.</p>', 'https://i.postimg.cc/9FqsgNpz/jungle-4787-20181116.jpg', 46),
(136, '<h4>Aguas Calientes and its Wonders</h4>\r\n<p>After the free time in <strong>Machu Picchu</strong> or the visit to the <strong>Mountain Machupicchu</strong> <strong>Huaynapicchu Mountain</strong> we will have to board the shared bus to <strong>Aguas Calientes</strong>, you can visit optional sites such as (<strong>Hot Springs, Museum, Arts and Crafts Center</strong>) and in the afternoon we will take the train back to <strong>Ollantaytambo</strong>.</p>', 'https://i.postimg.cc/LXDZpT31/Estacion-Aguas-Calientes.jpg', 46),
(137, '<h4>End of the journey Cusco</h4>\r\n<p>After 2 hours by train to <strong>ollantaytambo</strong>, when we arrive at <strong>ollantaytambo</strong> will be waiting for a staff with a sign with the logo of the company to take you to board the bus that will transport us to the city of <strong>Cusco</strong> for about 2 hours and you will be left serca of the hotel where you are staying.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 46),
(138, '<h4>Inca Trail Bicycle</h4>\r\n<p>We will start the <strong>Inca Jungle</strong> tour around 5:30 am our transport will come to pick you up along with our guide to their respective hostels and / or meeting place. Then we will continue to <strong>Ollantaytambo</strong>, where we will have breakfast for approximately 30 minutes. At the end of the breakfast we will continue towards <strong>Abra Málaga</strong> at 4.200 m.a.s.l. From there we will begin our journey by <strong>bicycle</strong> of approximately 4 hours crossing streams and rivers to reach the city of <strong>Huamanmarca</strong>.</p>\r\n<p><sub><em><strong>If you do not opt for this activity you will be able to continue in the transport that will go along with the group.</strong></em></sub></p>', 'https://i.postimg.cc/pL22pNTc/INCA-JUNGLE-TRAIL.jpg', 47),
(139, '<h4>Rafting in Santa Teresa</h4>\r\n<p>Then we will travel by bus to <strong>Santa Maria</strong> where we will enjoy lunch before starting our part of the <strong>kayak trip</strong> (For those who chose as an additional option at the time of booking). The sport of kayaking lasts approximately two hours. At the end of the activity, the group will take the bus to <strong>Santa Teresa</strong> (1 hour drive). Upon arrival in <strong>Santa Teresa</strong> to the hostel where you will stay for your stay and spend the night. Dinner will be served at a restaurant in <strong>Santa Teresa</strong>.</p>', 'https://i.postimg.cc/cJn5WCSp/rafting-inca-jungle-trek-1.jpg', 47),
(140, '<h4>Zip Line Santa Teresa</h4>\r\n<p>After a sunrise in the <strong>Inca jungle</strong>, we will taste an exquisite breakfast, we will depart from <strong>Santa Teresa</strong> (those who have booked <strong>Tirolesa</strong> will be taken to the starting point of this activity) and after this they will be transported by bus to <strong>Hidroeléctrica</strong>. Those who do not participate in the activity will continue the walk for approximately three hours to the <strong>hydroelectric</strong> plant, where we will meet the rest of the group. Then everyone in the group will enjoy lunch.</p>', 'https://i.postimg.cc/L8bsqHW4/zipline-4.jpg', 47),
(141, '<h4>Inca Jungle Trek</h4>\r\n<p>At the end of the day we will continue with the walk through the trails of the Inca Jungle for 3 hours to the town of <strong>Machu Picchu</strong> <strong>(Aguas Calientes)</strong>, where the guide will accommodate us in their respective accommodations. The group will be taken to a local restaurant for dinner while they receive information about the next day\'s <strong>Machu Picchu</strong> excursion.</p>', 'https://i.postimg.cc/zBq48hky/hidroelectrica.jpg', 48),
(142, '<h4>Guided View in Machu Picchu</h4>\r\n<p>At dawn we get up around 4:00 a.m. where we will receive a small Box Lunch, to go to the<strong> Inca Citadel</strong> of <strong>Machu Picchu</strong>, the walk will last an hour and a half (if you feel tired you can skip the walk and take the bus). We will enter the Inca <strong>citadel of Machu Picchu</strong> and enjoy a guided tour of the main points of <strong>Machu Picchu</strong> for approximately two hours. At the end you will have time to visit <strong>Machu Picchu</strong> individually or climb the mountains (<strong>Machu Picchu or Huayna Picchu</strong>) if you have your reservation made.</p>\r\n<p> </p>\r\n<p><sub><em><strong>Bus tickets to Machu Picchu can be purchased in the town of Aguas Calientes or can be purchased in advance with us.</strong></em></sub></p>', 'https://i.postimg.cc/fLkpVN6s/machu-picchu-honeymoon.jpg', 48),
(143, '<h4>Machu Picchu Cusco</h4>\r\n<p>After the visit you will return to <strong>Aguas Calientes</strong>. </p>\r\n<p>In the town of <strong>Aguas Calientes</strong> you will have time to enjoy a delicious meal or drink. Once on the train, you will arrive at <strong>Ollantaytambo</strong> station, where our staff will be waiting for you to return to <strong>Cusco</strong>, leaving travelers near your hotel.</p>\r\n<p><sub><em><strong>A very important note to keep in mind is that you must be at the station half an hour before the departure of your train.</strong></em></sub></p>', 'https://i.postimg.cc/pXkf6Fm0/peru-rail-hidroelectrica-02.jpg', 48),
(148, '<h4>Soraypampa Walk</h4>\r\n<p>Hotel pickup will begin at 4:30 am. Once we have met our fellow travelers, we will board the transport and travel for about two hours to <strong>Mollepata</strong>. There we will have breakfast at the border of the municipality of <strong>Cuzco</strong> before continuing to <strong>Soraypampa</strong>, which would be the starting point of our walk.</p>', 'https://i.postimg.cc/bJXtGF52/cuesta-santa-ana-cusco.jpg', 50),
(149, '<h4>Humantay Lagoon Height</h4>\r\n<p>When we arrive at <strong>Soraypampa</strong>, we will begin to ascend the 300 m to the lagoon of <strong>Humantay</strong>, which will take us approximately 1hr 30 minutes on foot. When we reach the <strong>lagoon</strong>, we will be rewarded with a beautiful view of this place. Not only will we be able to admire the <strong>Salkantay</strong> glacier (which reaches 5450 m asl), but also the <strong>Villcamba</strong> mountain range. After a rest and free time of 1hr to walk around the place, we will prepare for the descent to <strong>Soraypampa</strong>.</p>', 'https://i.postimg.cc/TwNNPrRK/humantay-12.jpg', 50),
(150, '<h4>Salkantay Cruse</h4>\r\n<p>When we return to <strong>Soraypampa</strong>, we can explore the place a bit and visit the trail and the view of the <strong>Salkantay</strong> mountain. This is the second largest mountain in the whole region of <strong>Cusco</strong>. After our short visit, we will head to <strong>Mollepata</strong> to share a buffet lunch before taking the bus back to <strong>Cusco</strong>. We will arrive at P<strong>laza San Francisco</strong> around 7:30 pm at night.</p>', 'https://i.postimg.cc/3NxJWX3v/tour-laguna-humantay-caballo.jpg', 50),
(151, '<h4>Pitumarca Community</h4>\r\n<p>The tour begins at 4:30am from your hotel in <strong>Cusco</strong> and, after two hours of transportation, we will arrive at <strong>Pitumarca</strong> for breakfast. Then we will continue our trip to <strong>Chillihuani</strong> to buy the access ticket to the Colorful <strong>Mountain</strong> (10 suns), located in the heart of the longest mountain range in the world.</p>', 'https://i.postimg.cc/PqMCxr10/camino-montana-siete-colores.jpg', 51),
(152, '<h4>Mountain of Colors Tour</h4>\r\n<p>We will walk for 2 hours until we reach <strong>Vinicunca Mountain</strong>, known as the hill of seven colors. Once we get there, we will have time to enjoy the fascinating <strong>colors</strong> of the <strong>mountain</strong> and take a look at the great panoramic view we have from here. We will also have enough time to take pictures. After fully appreciating the beautiful view, we will begin our descent back to <strong>Pitumarca</strong>.</p>', 'https://i.postimg.cc/wx5wt80X/colores-7.jpg', 51),
(153, '<h4>Pitumarca to Cusco</h4>\r\n<p>We will be back in <strong>Pitumarca</strong>, we will make a stop to have our lunch and after a short rest we will prepare for our return to <strong>Cusco</strong> where we will be arriving at 6:30 pm.</p>', 'https://i.postimg.cc/pXxzP4N9/pitumarca.jpg', 51),
(154, '<h4>Entrance to Inca Trail</h4>\r\n<p>Early in the morning and we will be transferred to the station, where we will take the train that will take us to km 104, where the <strong>Inca Trail</strong> begins. There begins a three-hour walk to the <strong>Archaeological</strong> <strong>Zone of Wiñaywayna</strong>, is at 2600 meters altitude and is believed to have been home to the ancient priests of <strong>Machu Picchu</strong>.</p>', 'https://i.postimg.cc/W1ZBffsX/incatrail-2.jpg', 52),
(155, '<h4>Sunset Inca Trail</h4>\r\n<p>We will eat and rest a bit and continue walking along the<strong> Inca Trail</strong> until we reach the <strong>Puerta del Sol</strong>, the access to the sacred enclosure of <strong>Machu Picchu</strong>. Then we will descend by bus to <strong>Aguas Calientes</strong>, where we will stay at the Vista <strong>Machu Picchu</strong> Hotel. We will have dinner and time to rest or optionally visit the hot springs.</p>', 'https://i.postimg.cc/43brX77c/incatrail-1.png', 52),
(157, '<h4>Archaeological zones Ollantaytambo</h4>\r\n<p>We will get up early to have breakfast at the hotel, then we will go to the bus station to board and arrive at <strong>machupicchu</strong> to appreciate <strong>the sunrise</strong>. We will have 2 hours of guided tour, having enough time to enjoy it on your own until the time to descend on foot or by bus to <strong>Aguas Calientes</strong>. There we will take the train to <strong>Ollantaytambo</strong>, where the transport will be waiting for us to move to <strong>Cusco</strong>.</p>', 'https://i.postimg.cc/fykkhhbG/mapi-7.jpg', 53),
(158, '<h4>On the way to Piskacuchu</h4>\r\n<p>We will pick you up by bus at your accommodation at 4:00 am and begin the tour that will take us to <strong>Piskacuchu</strong>, also known as \"km 82\", at an altitude of 2,700 meters. From that point we will start our route along <strong>the Inca Trail.</strong></p>', 'https://i.postimg.cc/sxNrzC16/tourcaminoinca0820181108-134710.jpg', 54),
(159, '<h4> The Upper Urubamba</h4>\r\n<p>We will continue walking along the <strong>Urubamba</strong> River along the flat terrain until we reach <strong>Miskay</strong>. There we will ascend to a viewpoint in the highest part of the mountain to see the impressive <strong>Inca city</strong> of <strong>Llactapata</strong>.</p>', 'https://i.postimg.cc/rp0t2qmb/incatrail1.jpg', 54),
(160, '<h4>Walking along Rios</h4>\r\n<p>We will continue the walk through the valley created by <strong>the Kusichaca</strong> River, a five-hour stretch where we will enjoy the views,<strong> flora and native fauna</strong>, until we finally reach the camp at <strong>Wayllabamba</strong> (3,000m high).</p>', 'https://i.postimg.cc/sXTqZkJD/incatrail-6.jpg', 54),
(161, '<h4>Walking in the Mountains</h4>\r\n<p>We have to get up early, have <strong>breakfast</strong> and continue to face the toughest section of the route, a steep climb where the landscape changes from <strong>sierra to puno</strong> (very dry landscape with little vegetation). We will pass through Abra <strong>Warmihuañusca</strong> (Passage of the Dead Woman - 4,200m high), in this area you can see <strong>llamas</strong> and <strong>domesticated alpacas</strong>.</p>', 'https://i.postimg.cc/rpYZ86NS/WAYLLABAMBA-TO-PACAYMAYO.jpg', 55),
(162, '<h4>The fauna of Pacaymayo</h4>\r\n<p>We will cross a cloud forest where many types of <strong>typical Andean</strong> birds live. Finally we will go down to <strong>the Pacaymayo valley</strong> (3,600m high) where we will camp.</p>', 'https://i.postimg.cc/bwS4swdm/aves-camino-inca.jpg', 55),
(163, '<h4>Mountain Ruins</h4>\r\n<p>From <strong>Pacaymayo</strong> we will go up to the second step of <strong>Abra Runkurakay</strong>, which is also an <strong>archaeological</strong> complex at 3,970 meters above sea level.</p>', 'https://i.postimg.cc/gk5mS05W/Inca-Trail.jpg', 56),
(164, '<h4>City of Clouds</h4>\r\n<p>We will go down to Yanacocha and enter the cloud forest to reach<strong> the beautiful archaeological</strong> <strong>complex of Sayacmarca</strong> (3,624m). We will continue the route through an Inca tunnel until we reach<strong> the archaeological complex Phuyupatamarca</strong>, which means “<strong>city above the clouds</strong>”, since it is at the highest point of the mountain.</p>', 'https://i.postimg.cc/43brX77c/incatrail-1.png', 56),
(165, '<h4>Inca Agriculture</h4>\r\n<p>We will continue the walk along the long descending stone steps until we reach <strong>Wiñaywayna</strong>, where there is an impressive Inca complex where they were engaged in <strong>agriculture</strong>, where we will camp for the night.</p>', 'https://i.postimg.cc/W1ZBffsX/incatrail-2.jpg', 56),
(166, '<h4>The Lost City</h4>\r\n<p>We will have to get up early to walk 1hr along a cobbled route built by <strong>the Incas that connects</strong> directly with <strong>Machu Picchu</strong>. We will access the site through the <strong>Puerta del Sol</strong>, known as <strong>Inti Punku</strong>, and we will live one of the most magical moments of the tour: observe the sunrise over <strong>The sacred citadel of the Incas</strong>.</p>', 'https://i.postimg.cc/NMZLYzrL/mapi-39.jpg', 57),
(167, '<h4>Inca Sanctuary</h4>\r\n<p>Then we will do a 2hrs tour with an expert guide who will explain all the details of <strong>this amazing place</strong>. Finally we will have free time to enjoy on our own and we can visit places like <strong>the Temple of the Moon</strong> or <strong>the impressive Inca Bridge</strong>.</p>', 'https://i.postimg.cc/7Y0JPw94/mapi-18.jpg', 57),
(168, '<h4>Landscapes by train</h4>\r\n<p>Around 13:00 pm we will take the bus to <strong>Aguas Calientes</strong>, where we will have free time to have lunch, after lunch we will have time to visit the town of hot waters and then take the train back to<strong> the city of Cuzco</strong>.</p>', 'https://i.postimg.cc/PqPkHnwj/trenacusco.jpg', 57),
(169, '<h4>Village Walk</h4>\r\n<p>Around 4:30 a.m. we will be picked up at our accommodation in <strong>Cuzco</strong> to head for Mollepata, which is 2900 meters high. There we will have breakfast and at the end we will go to <strong>Marcocasa</strong>, population from which we will begin our walk until we reach <strong>Soraypampa</strong>, where we will have our lunch.</p>\r\n<p><em><strong>(16 km a pie, 6 horas).</strong></em></p>', 'https://i.postimg.cc/5tDx4g0n/soraypampa.jpg', 58),
(170, '<h4>Humantay Lagoon Tour</h4>\r\n<p>After lunch we will continue to <strong>the beautiful Humantay Lagoon</strong>, there we will enjoy incredible views of the lagoon with the snow in the background, creating a landscape that is considered one of the most <strong>beautiful of the Andes</strong>. We will return to the Soraypampa camp where we will have dinner and rest until the next morning.</p>', 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 58),
(171, '<h4>Touring Cordilleras </h4>\r\n<p>Today we will get up early and after breakfast we will begin the ascent to the <strong>Salkantay</strong> pass, which is located at 4750 meters above sea level, the highest point of the entire route, from where we can observe the beautiful valleys and the imposing snowy peaks. Then we will begin our descent to the town of <strong>Huayracmachay</strong> where we will have time to eat and regain strength to continue to our camp in <strong>Colpapampa</strong>, where we will have dinner and spend the night.</p>', 'https://i.postimg.cc/50N3c6Wn/salkantay-5.jpg', 59),
(172, '<h4>A valley with rieles</h4>\r\n<p>We will get up early and after breakfast we will begin our walk to the small town of <strong>La Playa Sahuayaco</strong> through <strong>the Santa Teresa Valley.</strong> During the walk we will be in full contact with nature, we will see rivers, waterfalls, beautiful wild orchids and coffee and banana plantations among others. After lunch, a private bus will take us to <strong>Santa Teresa</strong> where we will take a transport to <strong>Hydroelectric</strong> where we will continue on foot for another 3 hours following the train line until reaching <strong>Aguas Calientes</strong>.</p>', 'https://i.postimg.cc/76qbL2Br/bycar-14.jpg', 60),
(173, '<h4>Aguas Calientes Village</h4>\r\n<p>Once in <strong>Aguas Calientes</strong> you will have free time before dinner to rest or walk through the town. Tonight we will stay in our hostel with private room and bathroom. We will go to bed early because the next day one of the most special moments of our tour, the imposing fortress of <strong>Machu Picchu</strong>, awaits us.</p>', 'https://i.postimg.cc/cCHHRNyD/bycar-21.jpg', 60),
(174, '<h4>Wonderful Walk</h4>\r\n<p>Today we will get up very early (5 am) to start the fifth and final day of the trek to Salkantay with a walk of approximately 1 hour and a half to<strong> Machu Picchu</strong>. We will enjoy a 2-hour guided tour of the citadel, after which we will have free time to explore it on our own and be able to calmly see <strong>the historic citadel</strong>.</p>', 'https://i.postimg.cc/T1z6K8R5/bycar-45.jpg', 61),
(175, '<h4>End of the Mysterious Tour</h4>\r\n<p>At the end of the visit we will return to Aguas Calientes (if you want you can return by bus to avoid the walk, buying the ticket there for 12 usd per way), we will have time to eat and take the train that will take us to <strong>Ollantaytambo</strong>, where we will be waiting our transport that will take us to <strong>Cusco</strong>.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 61),
(176, '<h4>Mollepata to Salkantay </h4>\r\n<p>The Salkantay Trek begins in <strong>Cusco</strong>. Transportation will come to your respective lodges. After the pick-up we will go to <strong>Mollepata</strong>, in a journey of 3 hours, where we can have breakfast (not included).</p>\r\n<p> </p>\r\n<p>We will also have time to buy the last items and then the transport will take us to <strong>Challacancha</strong>, from where we will start the trekking. We will walk along the trail, observing the first landscapes with <strong>mountains and valleys</strong>. After 3 hours of trekking, we will arrive at the first camp, <strong>Soraypampa</strong>, at 3900 mm. where we will have our lunch and time to recover energies.</p>\r\n<p> </p>\r\n<p>(16 km on foot, 6 hours).</p>', 'https://i.postimg.cc/5tDx4g0n/soraypampa.jpg', 62),
(177, '<h4>Visit to Laguna Humantay</h4>\r\n<p>In the afternoon we will ascend to the <strong>lagoon of Humantay</strong>, natural spectacle, to 1 hour and a half of the camp. In the background you can see the glacier of the same name, then return to our camp where we dislike dinner, where we share and receive instructions from the guide for the next day, before sleeping in our tents.</p>\r\n<p><strong><em>Altitude of Laguna Humantay : 4.100 m.a.s.l.</em></strong></p>', 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 62),
(178, '<h4>Abra Salkantay</h4>\r\n<p>We will start the day with a coca tea in our camps, then we will start the ascent through the mountains, seeing the difference in the landscape with the previous day.</p>\r\n<p>Our destination will be <strong>Abra Salkantay</strong>, at 4650 mnm, for lunch. </p>\r\n<p>After enjoying lunch and recovering our energy, we will start descending, passing from a cold and mountainous <strong>landscape to a warm and jungle one</strong>,<strong> until the Chaullay camp</strong> (2900 m of altitude).</p>\r\n<p><em><strong>Altitude of Laguna Humantay : 4.100 m.a.s.l.</strong></em></p>', 'https://i.postimg.cc/50N3c6Wn/salkantay-5.jpg', 63),
(179, '<h4>Salkantay - &gt; Playa Sahuayaco</h4>\r\n<p>The next day after breakfast we\'ll start walking. In the route we will see different plantations, among which are the quinoa and the pomegranate. The guide will give us explanations about some flowers and their importance in the Andean culture. After 5 hours of walking, we will arrive at <strong>Sahuayaco</strong> beach, where we will have our lunch.</p>', 'https://i.postimg.cc/fb4ypyD3/Colpapampa.jpg', 64),
(180, '<h4>Thermal Baths of Cocalmayo</h4>\r\n<p>After lunch, we follow the bus route to <strong>Santa Teresa</strong>, where we spend our third night. In the afternoon you can go to the thermal baths of <strong>Cocalmayo</strong>,<strong> complexes with 3 pools of different temperatures</strong>, and recover the muscles of the walk of the previous days. During the night we will have dinner.</p>', 'https://i.postimg.cc/3JJt0JMD/Sahuayaco.jpg', 64),
(181, '<h4>Zip Line Santa Teresa</h4>\r\n<p>After breakfast we will start the 3-hour walk that will take us to the <strong>Hydroelectric</strong>. There we will eat and after a short rest we will continue on foot for another 3 hours following the train line until we reach<strong> Aguas Calientes</strong>, where we will have dinner. We will go to bed early because <strong>the next day</strong> one of the most special moments of our tour, the imposing fortress of <strong>Machu Picchu</strong>, awaits us.</p>', 'https://i.postimg.cc/76qbL2Br/bycar-14.jpg', 65),
(182, '<h4>sunrise to Machupicchu</h4>\r\n<p>At dawn we get up around 4:00 a.m. where we will receive a small Box Lunch, to go to <strong>the Inca Citadel</strong> of <strong>Machu Picchu</strong>, the walk will last an hour and a half (if you feel tired you can skip the walk and take the bus). We will enter <strong>the Inca citadel of Machu Picchu</strong> and enjoy a guided tour of the main points of<strong> Machu Picchu</strong> for approximately two hours. At the end you will have time to visit <strong>Machu Picchu</strong> individually or climb the mountains (<strong>Machu Picchu or Huayna Picchu</strong>) if you have your reservation made.</p>\r\n<p><em>Bus tickets to Machu Picchu can be purchased in the town of Aguas Calientes or can be purchased in advance with us.</em></p>', 'https://i.postimg.cc/T1z6K8R5/bycar-45.jpg', 66),
(183, '<h4>Machu Picchu Cusco</h4>\r\n<p>At the end of the visit we will return to <strong>Aguas Calientes</strong> (if you want you can return by bus to avoid the walk, buying the ticket there for 12 usd per way), we will have time to eat and take the train that will take us to <strong>Ollantaytambo</strong>, where we will be waiting our transport that will take us to <strong>Cusco</strong>.</p>', 'https://i.postimg.cc/02vLr4XT/cusco.jpg', 66),
(190, '<h4>Camino del Inca Bicicleta</h4>\r\n<p>Iniciaremos el tour de <strong>Inca Jungle</strong> sobre las 5:30 am nuestro transporte vendr&aacute; a recogerlo junto con nuestro gu&iacute;a a sus respectivos hostales y/o lugar de encuentro. Despu&eacute;s continuaremos rumbo a <strong>Ollantaytambo</strong>, donde desayunaremos durante aproximadamente 30 minutos. Al finalizar el desayuno seguiremos en direcci&oacute;n a <strong>Abra M&aacute;laga</strong> a 4.200 m.s.n.m. Desde all&iacute; comenzaremos nuestra traves&iacute;a en bicicleta de aproximadamente 4 horas atravesando arroyos y r&iacute;os hasta llegar a la ciudad de <strong>Huamanmarca</strong>.</p>\r\n<p>&nbsp;</p>\r\n<p><sub><em><strong>si no optas por esta actividad podr&aacute;s seguir en el trayecto en el transporte que ir&aacute; junto con el grupo.</strong></em></sub></p>', 'https://i.postimg.cc/pL22pNTc/INCA-JUNGLE-TRAIL.jpg', 75),
(191, '<h4>Rafting en Santa Teresa</h4>\r\n<p>Entonces nos trasladaremos en autob&uacute;s a <strong>Santa Mar&iacute;a</strong> en donde disfrutaremos de un almuerzo antes de comenzar nuestra parte del viaje en <strong>kayak</strong> (Para los que eligieron como opci&oacute;n adicional a la hora de hacer la reserva). El deporte de kayak tiene una duraci&oacute;n de dos horas aproximadamente. Al finalizar la actividad, el grupo tomar&aacute; el autob&uacute;s a <strong>Santa Teresa</strong> (1 hora de viaje en auto). Al llegar a <strong>Santa Teresa</strong> al hostal donde se alojar&aacute; para su estad&iacute;a y pernoctar&aacute;n. La cena ser&aacute; servida en un restaurante de la localidad de <strong>Santa Teresa</strong>.</p>', 'https://i.postimg.cc/cJn5WCSp/rafting-inca-jungle-trek-1.jpg', 75),
(192, '<p>&nbsp;</p>\r\n<h4>Zip Line Santa Teresa</h4>\r\n<p>Tras un amanecer en la<strong> selva inca</strong>, degustaremos de un exquisito desayuno, partiremos desde <strong>Santa Teresa</strong> (los que hayan reservado <strong>Tirolesa</strong> ser&aacute;n llevados al punto de partida de esta actividad) y despu&eacute;s de esto ser&aacute;n transportados en autob&uacute;s a <strong>Hidroel&eacute;ctrica</strong>. Los que no participen en la actividad continuar&aacute;n la caminata por tres horas aproximadamente hasta la central <strong>hidroel&eacute;ctrica</strong>, donde nos reuniremos con el resto del grupo. Entonces todos el grupo disfrutar&aacute;n del almuerzo.</p>', 'https://i.postimg.cc/L8bsqHW4/zipline-4.jpg', 76),
(193, '<p>&nbsp;</p>\r\n<h4>Inca Jungle Trek</h4>\r\n<p>Al finalizar la jornada seguiremos con la caminata a trav&eacute;s de los senderos de la <strong>Selva Inca</strong> durante 3 horas hasta el pueblo de <strong>Machu Picchu</strong> (<strong>Aguas Calientes</strong>), donde el gu&iacute;a nos acomodar&aacute; en sus respectivos alojamientos. El grupo ser&aacute; llevado a cenar a un restaurante local mientras reciben la informaci&oacute;n sobre la excursi&oacute;n de al d&iacute;a siguiente de <strong>Machu Picchu</strong>.</p>', 'https://i.postimg.cc/zBq48hky/hidroelectrica.jpg', 76),
(220, '<h4>Mollepata a Salkantay </h4>\r\n<p>El <strong>Salkantay Trek</strong> comienza en <strong>Cusco</strong>. el transporte vendrá a sus respectivos albergues. Luego de la recogida nos dirigiremos a <strong>Mollepata</strong>, en un trayecto de 3 horas, donde podremos desayunar (no incluido).</p>\r\n<p>También Tendremos tiempo de comprar los últimos ítems y después el transporte nos llevará a <strong>Challacancha</strong>, desde donde iniciaremos el <strong>trekking</strong>. Recorreremos el sendero, observando los primeros paisajes con montañas y valles. Después de 3 horas de <strong>trekking</strong>, llegaremos al primer campamento, <strong>Soraypampa</strong>, a 3900 Mm. donde tendremos nuestro almuerzo y tiempo para recuperar energías.</p>\r\n<p><em><strong>(16 km a pie, 6 horas).</strong></em></p>', 'https://i.postimg.cc/GpBCQn4h/salkantay-trek-machupicchu-0620170524-001943.jpg', 90),
(221, '<p>&nbsp;</p>\r\n<h4>Vista Guiada en Machu Picchu</h4>\r\n<p>Por la madrugada nos levantamos sobre las 4:00 a.m. en donde recibiremos un peque&ntilde;o Box Lunch, para dirigirnos a la <strong>Ciudadela Inca</strong> de <strong>Machu Picchu</strong>, la caminata durar&aacute; una hora y media (si se siente cansado puede saltear la caminata y tomar el &oacute;mnibus). Ingresaremos a la ciudadela inca de <strong>Machu Picchu</strong> y disfrutaremos de una visita guiada de los principales puntos de <strong>Machu Picchu</strong> durante dos horas aproximadamente. Al finalizar tendr&aacute; tiempo para visitar <strong>Machu Picchu</strong> individualmente o escalar las monta&ntilde;as (<strong>Machu Picchu</strong> o <strong>Huayna Picchu</strong>) si tiene la reserva hecha.</p>\r\n<p>&nbsp;</p>\r\n<p><sub><strong><em>Los boletos de Bus a Machu Picchu pueden ser comprados en el poblado de Aguas Calientes o bien, puede comprarse de antemano con nosotros</em></strong></sub></p>', 'https://i.postimg.cc/fLkpVN6s/machu-picchu-honeymoon.jpg', 77),
(222, '<p>&nbsp;</p>\r\n<h4>Machu Picchu Cusco</h4>\r\n<p>Despu&eacute;s de la visita se retornar&aacute; a <strong>Aguas Calientes</strong>.&nbsp;<br />En el poblado de <strong>Aguas Calientes</strong> dispondr&aacute; de tiempo para disfrutar de una deliciosa comida o bebida . Un vez que est&eacute; en el tren, llegar&aacute; a la estaci&oacute;n de <strong>Ollantaytambo</strong>, donde nuestro personal lo estar&aacute; esperando para regresar a <strong>Cusco</strong>, dejando a los viajeros cerca de su hotel.</p>\r\n<p>&nbsp;</p>\r\n<p><br /><sub><em><strong>Una nota muy importante a tener en cuenta es que debe estar en la estaci&oacute;n media hora antes de la salida de su tren</strong></em></sub></p>', 'https://i.postimg.cc/pXkf6Fm0/peru-rail-hidroelectrica-02.jpg', 77),
(223, '<h4>Visita de Laguna Humantay</h4>\r\n<p>A la tardecer ascenderemos a la<strong> laguna de Humantay</strong>, espectáculo natural, a 1 hora y media del campamento. En el fondo se puede ver el glaciar del mismo nombre, después retornaremos a nuestro campamento donde disgustaremos de la cena, en la que compartiremos y recibiremos instrucciones del guía para el día siguiente, antes de dormir en nuestras carpas.</p>\r\n<p> </p>\r\n<p><em>Altitud de Laguna Humantay</em> :  <strong>4.100 m.s.n.m</strong></p>', 'https://i.postimg.cc/VvwPmSDd/IMG-3365-e1513791579166.jpg', 90),
(224, '<h4>Abra Salkantay</h4>\r\n<p>Iniciaremos el día con un te de coca en nuestros campamentos, luego iniciaremos el ascenso <strong>a través de las montañas</strong>, viendo la diferencia en el paisaje con el día anterior.</p>\r\n<p> </p>\r\n<p>Nuestro destino sera <strong>Abra Salkantay</strong>, a 4650 mnm, para el almuerzo. <br />Después disfrutar del almuerzo y de recuperar energías, empezaremos a descender, pasando de un paisaje frío y montañoso a otro cálido y selvático, hasta el campamento de Chaullay (2900 m de altitud).</p>', 'https://i.postimg.cc/C1j9PhSY/salkantay-930x555.jpg', 91),
(231, '<h4>Salkantay - > Playa Sahuayaco</h4>\r\n<p>Al otro día después del desayuno empezaremos a caminar. En la ruta veremos diferentes plantaciones, entre las que se encuentran la quinua y la granada. El guía nos dará explicaciones sobre algunas flores y su importancia en la cultura andina. Después de 5 horas de caminata, llegaremos a la playa de Sahuayaco, donde tendremos nuestro almuerzo.</p>', 'https://i.postimg.cc/sxM7fXxC/salkantay-trek-cusco-5.jpg', 92),
(232, '<h4>Baños Termales de Cocalmayo</h4>\r\n<p>Luego del almuerzo, seguimos la ruta en bus hacia Santa Teresa, donde pernoctamos nuestra tercera noche. Por la tarde se podrá ir a los <strong>baños termales</strong> de <strong>Cocalmayo</strong>, complejos con <strong>3 piscinas</strong> de <strong>diferentes temperaturas</strong>, y recuperar los músculos de la caminata de los días anteriores. Durante la noche cenaremos.</p>', 'https://i.postimg.cc/Wpy0WMYN/Hot-Springs-Inka-Jungle-Machu-Picchu-Inka-Jungle-Cusco-Inka-Jung.jpg', 92),
(233, '<h4>Zip Line Santa Teresa</h4>\r\n<p>After a sunrise in the <strong>Inca jungle</strong>, we will taste an exquisite breakfast, we will depart from <strong>Santa Teresa</strong> (those who have booked <strong>Tirolesa</strong> will be taken to the starting point of this activity) and after this they will be transported by bus to Hidroeléctrica. Those who do not participate in the activity will continue the walk for approximately three hours to the <strong>hydroelectric</strong> plant, where we will meet the rest of the group. Then everyone in the group will enjoy lunch.</p>', 'https://i.postimg.cc/L8bsqHW4/zipline-4.jpg', 93),
(234, '<h4>Selva Inca a Aguas Calientes</h4>\r\n<p>Al finalizar la jornada seguiremos con la caminata a través de los senderos de la Selva Inca durante 3 horas hasta el pueblo de <strong>Machu Picchu (Aguas Calientes)</strong>, donde el guía nos acomodará en sus respectivos alojamientos. El grupo será llevado a cenar a un restaurante local mientras reciben la información sobre la excursión de al día siguiente de Machu Picchu.</p>', 'https://i.postimg.cc/zBq48hky/hidroelectrica.jpg', 93),
(235, '<h4>Visita Machu Picchu</h4>\r\n<p style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: #2c3e50; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\">Por la madrugada nos levantamos sobre las 4:00 a.m. en donde recibiremos un pequeño Box Lunch, para dirigirnos a la <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Ciudadela Inca</strong> de <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Machu Picchu</strong>, la caminata durará una hora y media (si se siente cansado puede saltear la caminata y tomar el ómnibus). Ingresaremos a la ciudadela inca de <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Machu Picchu</strong> y disfrutaremos de una visita guiada de los principales puntos de <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Machu Picchu</strong> durante dos horas aproximadamente. Al finalizar tendrá tiempo para visitar <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Machu Picchu</strong> individualmente o escalar las montañas (<strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Machu Picchu</strong> o <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Huayna Picchu</strong>) si tiene la reserva hecha.</p>\r\n<p style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: #2c3e50; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\"> </p>\r\n<p style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: #2c3e50; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\"><sub style=\"margin: 0px; padding: 0px; box-sizing: border-box;\"><strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\"><em style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Los boletos de Bus a Machu Picchu pueden ser comprados en el poblado de Aguas Calientes o bien, puede comprarse de antemano con nosotros</em></strong></sub></p>', 'https://i.postimg.cc/fLkpVN6s/machu-picchu-honeymoon.jpg', 94);
INSERT INTO `itinerarydescription` (`IdItinerarioDescription`, `DescriptionItem`, `ImgItem`, `IdItineraryDay`) VALUES
(236, '<h4>Machu Picchu Cusco</h4>\r\n<p style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: #2c3e50; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\">Después de la visita se retornará a <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Aguas Calientes</strong>. <br style=\"margin: 0px; padding: 0px; box-sizing: border-box;\" />En el poblado de <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Aguas Calientes</strong> dispondrá de tiempo para disfrutar de una deliciosa comida o bebida . Un vez que esté en el tren, llegará a la estación de <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Ollantaytambo</strong>, donde nuestro personal lo estará esperando para regresar a <strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Cusco</strong>, dejando a los viajeros cerca de su hotel.</p>\r\n<p style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: #2c3e50; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\"> </p>\r\n<p style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: #2c3e50; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\"><br style=\"margin: 0px; padding: 0px; box-sizing: border-box;\" /><sub style=\"margin: 0px; padding: 0px; box-sizing: border-box;\"><em style=\"margin: 0px; padding: 0px; box-sizing: border-box;\"><strong style=\"margin: 0px; padding: 0px; box-sizing: border-box;\">Una nota muy importante a tener en cuenta es que debe estar en la estación media hora antes de la salida de su tren</strong></em></sub></p>', 'https://i.postimg.cc/pXkf6Fm0/peru-rail-hidroelectrica-02.jpg', 94),
(239, '<h4>Chinchero Centro Textil</h4>\r\n<p>El transporte pasara por sus alojamientos y comenzaremos nuestro rumbo a <strong>Chinchero</strong> (<strong>centro textil y artesanal</strong>), el cual se encuentra situado a unos 40 minutos de <strong>la ciudad de Cusco</strong>, podremos ver cómo procesan <strong>lana de llama</strong> y podremos ver los diferentes <strong>tejidos artesanales</strong>.</p>\r\n<p> </p>\r\n<p><strong><sup><em>En el lugar puedes comprar las artesanias y recordatorios. </em></sup></strong></p>', 'https://i.postimg.cc/XqcPVs8f/image.jpg', 97),
(240, '<h4><span style=\"color: #3598db;\">Salineras de Maras</span></h4>\r\n<p>Realizaremos una visita guiada por las <strong>minas de sal de Maras</strong> (esta ubicado a 50 minutos de Cusco).</p>\r\n<p>Tendremos 30 minutos aproximadamente en el cual nuestro guía nos explicará el proceso de <strong>extracción de la sal</strong> , también tendremos tiempo para hacer fotografías y poder hacer compras de chocolates puros procesados con la sal de maras.</p>\r\n<p> </p>\r\n<p><strong><em><sup>Durante nuestra visita guiada podremos ver la extraccion de sal y recoleccion.</sup></em></strong></p>', 'https://i.postimg.cc/sxvTFh7L/tour-salineras-cusco-800x600.jpg', 97),
(241, '<h4>Andenes de Moray</h4>\r\n<p>Despues de la visita a las minas de sal visitaremos el <strong>centro arqueológico de Moray</strong>, que son <strong>terrazas y andenes agrícolas</strong> que están superpuestas concéntricamente tomando la forma de un gigantesco anfiteatro. retornaremos a  <strong>la ciudad del cusco</strong>  las 3:00 pm.</p>\r\n<p> </p>\r\n<p><em><sup>El tour finazliza en <a title=\"Plaza san Francisco\" href=\"https://goo.gl/maps/hCSUTfqnbvnv9UPu7\" target=\"_blank\" rel=\"noopener\">San Francisco</a></sup><sup> a 2 cuadras del</sup><sup><strong> Plaza de Armas del cusco.</strong></sup></em></p>', 'https://i.postimg.cc/QdmJXHTW/image.jpg', 97);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lang`
--

CREATE TABLE `lang` (
  `IdLang` int(11) NOT NULL,
  `CodLang` varchar(2) NOT NULL,
  `Lang` varchar(30) NOT NULL,
  `LangSlogan` text NOT NULL,
  `bannerImg` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TitleLang` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `qualitiesLang` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MetaTitle` text NOT NULL,
  `MetaDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lang`
--

INSERT INTO `lang` (`IdLang`, `CodLang`, `Lang`, `LangSlogan`, `bannerImg`, `TitleLang`, `qualitiesLang`, `MetaTitle`, `MetaDescription`) VALUES
(1, 'es', 'Español', 'Reserva Los Mejores TOurs con nosotros', 'https://i.postimg.cc/pLHxDS2m/imagen-banner-home-typicaltrips.jpg', 'Excursiones a Machu Picchu por carro ', 'Asistencia turística gratuita las 24 horas del día, para resolver todas tus dudas.;Cambio de fecha de actividad sin costo adicional, en algunos casos hasta 24 horas antes, Flexible y fácil.;Tours en servicios confortables y de acuerdo a su disponibilidad de tiempo.;', 'Los mejores toures los realizas en cusco', 'Recorre los mejores tours con nosotros, excursiones y paquetes del Perú a precios muy cómodos. Asesoramiento y asistencia en viaje gratis. Reserva ahora!!'),
(2, 'en', 'Ingles', 'Book The Best Tours with us', 'https://i.postimg.cc/pLHxDS2m/imagen-banner-home-typicaltrips.jpg', 'Typical Trips to Machu Picchu ', 'Free tourist assistance 24 hours a day, to solve all your doubts.;Change of activity date at no additional cost, in some cases up to 24 hours before, Flexible and easy.;Tours in comfortable services and according to your availability of time.;', 'You make the best trips in Cusco', 'Visit the best tours with us, excursions and packages of Peru at very comfortable prices. Free travel advice and assistance. Book now!');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pg_sliderhome`
--

CREATE TABLE `pg_sliderhome` (
  `IdSliderHome` int(11) NOT NULL,
  `url` text NOT NULL,
  `IdLang` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `link_buttom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pg_sliderhome`
--

INSERT INTO `pg_sliderhome` (`IdSliderHome`, `url`, `IdLang`, `title`, `description`, `link_buttom`) VALUES
(1, 'https://i.postimg.cc/W16rtNVg/photo-1526052056866-810289073817-ixlib-rb-1-2.jpg', 2, 'Travel To Machu Picchu By Car', 'machu picchu by car , It is an alternative tour for all travelers who want to visit machu picchu and enjoy in just 2 days also for people with little time in peru', '/en/traditional-tours/machu-picchu-by-car-2-days'),
(2, 'https://i.postimg.cc/W16rtNVg/photo-1526052056866-810289073817-ixlib-rb-1-2.jpg', 1, 'Visite Machu Picchu 2 Dias  en Carro', 'Una excursión ideal para viajeros que no cuentan con mucho tiempo para conocer Machu Picchu', '/es/tours-tradicionales/machu-picchu-por-carro-2-dias'),
(3, 'https://i.postimg.cc/RhCdMgN5/image.jpg', 1, 'Excursión de Montaña de Colores 1 Días', 'Visita el lugar donde el arcoíris cayó del cielo para fundirse con el paisaje en esta ruta de trekking por Vinicunca, la Montaña de los Siete Colores.', '/es/tours-de-caminatas/ausangate-montana-de-colores-1-dia'),
(4, 'https://i.postimg.cc/25T8PQ9J/image.jpg', 1, 'Excursión de Laguna Humantay Todo el Día', 'La laguna Humantay es un espejo de agua de color turquesa que yace a las faldas del imponente nevado Apu Humantay', '/es/tours-de-caminatas/laguna-de-humantay');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pricecategory`
--

CREATE TABLE `pricecategory` (
  `IdPriceCategory` int(11) NOT NULL,
  `PriceName` varchar(255) NOT NULL,
  `PriceData` varchar(255) NOT NULL,
  `PriceRange` varchar(255) NOT NULL,
  `IdLang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pricecategory`
--

INSERT INTO `pricecategory` (`IdPriceCategory`, `PriceName`, `PriceData`, `PriceRange`, `IdLang`) VALUES
(1, 'Adulto', 'pad', '( 18 + )', 1),
(2, 'Joven', 'pjv', '(Edad 11 - 17)', 1),
(3, 'Niño', 'pnn', '( Edad +3 - 11 )', 1),
(4, 'Child', 'pnn', '(+3 - 11 years) ', 2),
(5, 'Young', 'pjv', '(+11 - 17 years)', 2),
(6, 'Adult', 'pad', '( +18  )', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recommendations`
--

CREATE TABLE `recommendations` (
  `IdRecommendation` int(11) NOT NULL,
  `RecommendationDescription` text NOT NULL,
  `IdTour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recommendations`
--

INSERT INTO `recommendations` (`IdRecommendation`, `RecommendationDescription`, `IdTour`) VALUES
(3, 'Este tour no requiere esfuerzo físico, por lo que sólo te recomendamos traer:', 3),
(4, 'Hay que tener en cuenta que Aguas Calientes está a 1000 metros menos de altitud respecto a Cuzco y que el clima varía en función de la época del año, especialmente en época de lluvias. Como es un clima variable nosotros recomendamos llevar:', 4),
(5, 'Hay que tener en cuenta que Aguas Calientes está a 1000 metros menos de altitud respecto a Cuzco y que el clima varía en función de la época del año, especialmente en época de lluvias. Como es un clima variable nosotros recomendamos llevar:', 5),
(6, 'Hay que tener en cuenta que el tour comienza en Cuzco, ubicado a 3.400 metros de altitud, y Machu Picchu pueblo (Aguas Calientes) está a 1.000 metros menos de altitud respecto a Cuzco, por lo que tienes que ir equipado con ropa de abrigo pero también con manga corta, ya que las noches son frías y durante el día hace calor. También hay que tener en cuenta que el clima varía según la época del año, especialmente en época de lluvias (diciembre - marzo). Recomendamos dejar las mayor parte del equipaje en tu alojamiento en Cuzco y viajar solo con lo necesario para el tour:', 6),
(7, 'Hay que tener en cuenta que el tour comienza en Cuzco, ubicado a 3.400 metros de altitud, y Machu Picchu pueblo (Aguas Calientes) está a 1.000 metros menos de altitud respecto a Cuzco, por lo que tienes que ir equipado con ropa de abrigo pero también con manga corta, ya que las noches son frías y durante el día hace calor. También hay que tener en cuenta que el clima varía según la época del año, especialmente en época de lluvias (diciembre - marzo). Recomendamos dejar las mayor parte del equipaje en tu alojamiento en Cuzco y viajar solo con lo necesario para el tour:', 7),
(8, 'Te recomendamos llevar:', 8),
(9, 'Te recomendamos llevar:', 9),
(10, 'Te recomendamos llevar:', 12),
(11, 'Te recomendamos llevar:', 13),
(12, 'Hay que tener en cuenta que Aguas Calientes está a 1000 metros menos de altitud respecto a Cuzco y que el clima varía en función de la época del año, especialmente en época de lluvias. Como es un clima variable nosotros recomendamos llevar:', 14),
(13, 'Hay que tener en cuenta que el clima es diferente según la época del año y que cambia a lo largo de la ruta, ya que pasamos por diferentes ecosistemas, por lo que recomendamos llevar:', 15),
(14, 'Hay que tener en cuenta que Aguas Calientes está a 1000 metros menos de altitud respecto a Cuzco y que el clima varía en función de la época del año, especialmente en época de lluvias. Como es un clima variable nosotros recomendamos llevar:', 16),
(15, 'Hay que tener en cuenta que Aguas Calientes está a 1000 metros menos de altitud respecto a Cuzco y que el clima varía en función de la época del año, especialmente en época de lluvias. Como es un clima variable nosotros recomendamos llevar:', 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recommendationslists`
--

CREATE TABLE `recommendationslists` (
  `IdRecommendationList` int(11) NOT NULL,
  `Recommendation` text NOT NULL,
  `IdLang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recommendationslists`
--

INSERT INTO `recommendationslists` (`IdRecommendationList`, `Recommendation`, `IdLang`) VALUES
(1, 'Cámara de fotos', 1),
(2, 'Protector solar', 1),
(3, 'Ropa de abrigo (impermeable en temporada de lluvias)', 1),
(4, 'Agua', 1),
(5, 'Snacks', 1),
(6, 'Gafas de sol', 1),
(7, 'Repelente de insectos', 1),
(8, 'Calzado cómodo (a ser posible zapatillas de montaña impermeables)', 1),
(9, 'Botella de agua rellenable', 1),
(10, 'Chubasquero/poncho impermeable', 1),
(11, 'Pasaporte', 1),
(12, 'Ropa de baño y toalla (para las aguas termales de Aguas Calientes)', 1),
(13, 'Medicación para evitar mal de altura (en caso de que lo consideres necesario).', 1),
(14, 'Mochila para dos días', 1),
(41, 'Photo camera', 2),
(42, 'Sunscreen', 2),
(43, 'Warm clothes (rainy season raincoat)', 2),
(44, 'Water', 2),
(45, 'Sunglasses', 2),
(46, 'Insect repellent', 2),
(47, 'Comfortable footwear (if possible waterproof mountain shoes)', 2),
(48, 'Refillable water bottle', 2),
(49, 'Raincoat / waterproof poncho', 2),
(50, 'Passport', 2),
(51, 'Bath linen and towel (for the hot springs of Aguas Calientes)', 2),
(52, 'Medication to avoid altitude sickness (if you consider it necessary).', 2),
(53, 'Backpack for two days', 2),
(54, 'Gorra o sombrero', 1),
(55, 'Camiseta y pantalón corto para las caminatas', 1),
(56, 'Sandalias o chanclas', 1),
(57, 'Productos de higiene personal', 1),
(58, 'Linterna', 1),
(59, 'Cap or hat', 2),
(60, 'T-shirt and shorts for walks', 2),
(61, 'Sandals or flip flops', 2),
(62, 'Personal hygiene products', 2),
(63, 'Lantern', 2),
(78, 'Mochila ligera', 1),
(79, 'Mochila ligera (máximo 7 kg)', 1),
(80, 'Saco de dormir (-15º)', 1),
(81, 'Bastones para caminar', 1),
(82, 'Mochila de trekking mediana (20 litros aprox.)', 1),
(83, 'Ropa térmica', 1),
(84, 'Dinero de extra(soles o dólares)', 1),
(85, 'Lightweight backpack', 2),
(86, 'Light backpack (maximum 7 kg)', 2),
(87, 'Sleeping bag (-15º)', 2),
(88, 'Walking sticks', 2),
(89, 'Medium trekking backpack (20 liters approx.)', 2),
(90, 'Thermal clothing', 2),
(91, 'Extra money (soles or dollars)', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recommendationslist_has_tour`
--

CREATE TABLE `recommendationslist_has_tour` (
  `IdRecommendationList` int(11) NOT NULL,
  `IdTour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recommendationslist_has_tour`
--

INSERT INTO `recommendationslist_has_tour` (`IdRecommendationList`, `IdTour`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 8),
(1, 9),
(1, 13),
(1, 16),
(1, 17),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 10),
(3, 11),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(4, 1),
(4, 2),
(4, 3),
(4, 9),
(4, 12),
(5, 1),
(5, 2),
(5, 3),
(5, 9),
(5, 12),
(5, 13),
(5, 16),
(5, 17),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 10),
(6, 11),
(6, 12),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(7, 4),
(7, 5),
(7, 6),
(7, 7),
(7, 10),
(7, 11),
(7, 14),
(7, 15),
(7, 16),
(7, 17),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(8, 8),
(8, 11),
(8, 12),
(8, 13),
(8, 14),
(8, 15),
(8, 16),
(8, 17),
(9, 4),
(9, 5),
(9, 6),
(9, 7),
(9, 10),
(9, 11),
(9, 14),
(9, 15),
(9, 16),
(9, 17),
(10, 4),
(10, 8),
(10, 10),
(10, 11),
(10, 12),
(10, 14),
(10, 15),
(10, 16),
(10, 17),
(11, 4),
(11, 5),
(11, 6),
(11, 7),
(11, 10),
(11, 11),
(11, 14),
(11, 15),
(11, 16),
(11, 17),
(12, 4),
(12, 5),
(12, 6),
(12, 7),
(12, 10),
(12, 11),
(12, 14),
(12, 15),
(12, 16),
(12, 17),
(13, 4),
(13, 5),
(13, 6),
(13, 7),
(13, 10),
(13, 11),
(13, 12),
(13, 14),
(13, 15),
(13, 16),
(13, 17),
(14, 6),
(14, 7),
(54, 10),
(54, 11),
(54, 16),
(54, 17),
(55, 10),
(55, 11),
(56, 10),
(56, 11),
(56, 16),
(56, 17),
(57, 10),
(57, 11),
(58, 10),
(58, 11),
(58, 16),
(58, 17),
(78, 8),
(78, 9),
(78, 14),
(79, 15),
(80, 16),
(80, 17),
(81, 16),
(81, 17),
(82, 16),
(82, 17),
(83, 16),
(83, 17),
(84, 8),
(84, 9),
(84, 16),
(84, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tour`
--

CREATE TABLE `tour` (
  `IdTour` int(11) NOT NULL,
  `UriTour` text NOT NULL,
  `TourBannerImg` text NOT NULL,
  `TourTitle` text NOT NULL,
  `TourDeparture` varchar(100) NOT NULL,
  `TourEnd` varchar(255) NOT NULL,
  `TourMap` varchar(255) DEFAULT NULL,
  `TourTransport` varchar(255) NOT NULL,
  `TourDestinations` varchar(255) NOT NULL,
  `TourStyle` varchar(255) NOT NULL,
  `AgeRange` varchar(255) NOT NULL,
  `TourMaxGroup` int(11) NOT NULL,
  `TourDurationInt` int(11) NOT NULL,
  `TourDurationText` varchar(50) NOT NULL,
  `TourLangGuided` varchar(50) NOT NULL,
  `DetailsAboutTour` text NOT NULL,
  `MetaTitle` text NOT NULL,
  `MetaDescription` text NOT NULL,
  `IdCategory` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `AgeFrom` int(11) NOT NULL,
  `AgeTo` int(11) NOT NULL,
  `Bannertourprincipal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tour`
--

INSERT INTO `tour` (`IdTour`, `UriTour`, `TourBannerImg`, `TourTitle`, `TourDeparture`, `TourEnd`, `TourMap`, `TourTransport`, `TourDestinations`, `TourStyle`, `AgeRange`, `TourMaxGroup`, `TourDurationInt`, `TourDurationText`, `TourLangGuided`, `DetailsAboutTour`, `MetaTitle`, `MetaDescription`, `IdCategory`, `status`, `AgeFrom`, `AgeTo`, `Bannertourprincipal`) VALUES
(1, 'tour-en-salineras-y-andes-de-moray', 'https://www.mundomapi.com/images/tours/maras-y-moray-1552149919.jpg', 'Minas de sal y Andenes de Moray', 'Hostel, Cusco', 'San Francisco, Cusco', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22002.69576010217!2d-72.16576153022461!3d-13.298641100000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x916dc2c50bc8d515%3A0x2232a3f3560d29c9!2sSalineras%20de%20Maras!5e1!3m2!1ses', 'car, van', 'Chincheros - Maras - Moray', '3', '15 a 50 años', 15, 1, '1 dias', 'en_es', '<p> </p>\r\n<p>Tener en cuenta <strong>el clima de cusco</strong>, ya que puede ser muy lluvioso o muy soleado tambien el tour se ofrece con salidas diarias en grupos . <br /><br />La confirmación será recibida en el momento de la reserva. <br /><br />Ya que esta actividad es un tour grupal, recogeremos a la primera persona a las 8:00 y la última a las 8:30, por favor esté listo en el lobby de su hotel a las 8:00 am y nuestro guía tiene tiempo de pasar. hasta las 8:30 am</p>', 'Tour en las Salineras Maras y andenes de Moray ', 'Salineras de Maras y Andenes de Moray Tour de medio día, salidas todo los días', 1, 1, 15, 50, 'https://i.postimg.cc/8khKfVkT/maras-moray-typicialtrips.jpg'),
(2, 'valle-sagrado-de-los-incas-tour-1-dia', 'https://i.postimg.cc/T325RQGT/image.jpg', 'Valle sagrado de los Incas Completo', 'Hotel, Peru', 'Plazoleta santa Ana, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van', 'Cusco, Pisaq, Urubamba,Ollantaytambo,Cusco', '1', '5 a 50 años', 15, 1, '1 dias', 'en_es', '<p> </p>\r\n<p>Tener en cuenta <strong>el clima de cusco</strong>, ya que puede ser muy lluvioso o muy soleado también el tour se ofrece con salidas diarias en grupos de 15 a 25 personas.</p>\r\n<p> </p>\r\n<p><span style=\"color: #1a2b49; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; background-color: #ffffff;\">La confirmación será recibida en el momento de la reserva.</span></p>\r\n<p><span style=\"color: #1a2b49; font-family: \'GT Eesti\', Arial, sans-serif; font-size: 14px; background-color: #ffffff;\">Ya que esta actividad es un tour grupal, recogeremos a la primera persona a las 07:30 y la última a las 08:10, por favor esté listo en el lobby de su hotel a las 07:30 y nuestro guía tiene tiempo de pasar. hasta las 08:10</span></p>', 'Valle Sagrado de los Incas Tour de 1 Dia', 'Una de las mejores experiencias que tendrás al recorrer por el valle sagrado de los Incas \"Todo Incluido\".', 1, 1, 5, 50, 'https://i.postimg.cc/9F7J5mx6/maras-moray-completo-typicaltrips.jpg'),
(3, 'city-tour-cusco-medio-dia-catedral-cusco', 'https://i.postimg.cc/WpnJKHsc/32ny.jpg', 'City Tour Cusco Medio Dia', 'Qoricancha, Cusco', 'Calle Tullumayo, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van', 'Templo del Sol Qoricancha, Sacsayhuaman, Qenqo, Pucapucara, Tambomachay.', '1', '10 a 50 años', 30, 1, '1 dias', 'en_es', '<p><strong>City Tour Cusco</strong> es un recorrido por los principales atractivos turísticos de<strong> la ciudad del Cusco</strong> ,<strong> la antigua capital de los incas</strong>.</p>\r\n<p> </p>\r\n<p>El trayecto incluye lugares de gran importancia histórica como el templo del <strong>Coricancha</strong>, la fortaleza de <strong>Sacsayhuaman</strong> y otros recintos. Este tour es la forma más extendida de comenzar la visita al <strong>Cusco</strong> y <strong>Machu Picchu.</strong></p>', 'City Tour Cusco: Sacsayhuaman, Qoricancha, Catedral', 'City Tour Cusco incluye el ingreso al Qurikancha, Sacsayhuaman, Qenqo, Puca Pucara y Tambomachay. El tour incluye trasporte y servicios', 1, 1, 10, 50, 'https://i.postimg.cc/FRfNhkkb/sacsayhuaman-typicaltrips.jpg'),
(4, 'machu-picchu-por-carro-2-dias', 'https://i.postimg.cc/1tRyNM2D/39896323802-a7a5d787f0-k.jpg', 'Machu Picchu By Car', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Santa teresa, Hidroelectrica, Aguas Calientes, Machu Picchu', '1', '5 a 50 años', 20, 2, '2 dias', 'en_es', '<p>Tener en cuenta que los ingresos a la ciudadela de machu picchu estan posibles en que varian en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>', 'Machu Picchu por carro 2 dias', 'tour Machu Picchu by Car, la forma mas economica de visitar Machu Picchu maravilla del mundo . Incluye alojamiento y tour guiado.', 1, 1, 5, 50, 'https://i.postimg.cc/GhRd1yY6/MACHUPICHU-BY-CAR-2-D-typicaltrips.jpg'),
(5, 'machu-picchu-1-dia-por-tren', 'https://i.postimg.cc/SNnNrTsr/Tren.jpg', 'Machu Picchu 1 dia', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Ollantaytambo, Aguas Calientes, Machu Picchu, Cusco', '1', '5 a 50 años', 15, 1, '1 dias', 'en_es', '<p>Tener en cuenta que los ingresos a la ciudadela de machupicchu estan posibles en que varian en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>', 'Machu Picchu por un día, Full Day', 'Tour para quienes tienen poco tiempo y solo quieren conocer Machu Picchu, disfruta Machu picchu en un dia', 1, 1, 5, 50, 'https://i.postimg.cc/d3G7VZqD/machu-picchu-1-dia-typicaltrips.jpg'),
(6, 'amanecer-en-machu-picchu-2-dias', 'https://i.postimg.cc/dt3DfQWG/798iujboiugoihg654654654.jpg', 'Amanecer en Machu Picchu', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Ollantaytambo, Aguas Calientes, Machu Picchu', '1', '5 a 60 años', 15, 2, '2 dias', 'en_es', '<p>Tener en cuenta que los ingresos a la ciudadela de machupicchu estan posibles en que varían en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>', 'Amanecer en Machu Picchu', 'La mejor opcion para conocer Machu Picchu sin prisa y disfrutar de las maravillas que nos ofrece la naturaleza, También  Disfruta de una visita inolvidable al santuario histórico de Machu Picchu.', 1, 1, 5, 60, 'https://i.postimg.cc/W3ZNjRn7/Machupicchu-amacener-typicaltrips.jpg'),
(7, 'valle-sagrado-y-machupicchu-conexion-2-dias', 'https://i.postimg.cc/8zvJ5DbD/Valle-Sagrado-y-Machu-Picchu-3.jpg', 'Valle Sagrado y Machu Picchu', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Pisaq, Urubamba, ollantaytambo, Aguas Calientes, Machu Picchu', '1', '10 a 50 años', 15, 2, '2 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tener en cuenta que los ingresos a la ciudadela de machupicchu estan posibles en que varian en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>\r\n</body>\r\n</html>', 'Valle Sagrado conexión a Machu Picchu', 'Aprecia la cultura Inca en nuestro Tour al Valle Sagrado y Machu Picchu de dos dias de duracion.', 1, 1, 10, 50, 'https://i.postimg.cc/sgGRzxDr/Tour-Valle-sagrado-incas-typical.jpg'),
(8, 'maras-y-moray-con-cuatrimotos', 'https://i.postimg.cc/hvQH2GJS/75426209-787734838334064-5060491484028320274-n.jpg', 'Cuatrimotos Maras Moray', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van', ' Cruzpata, Maras, Moray, Cruzpata', '1', '15 a 40 años', 20, 1, '1 dias', 'en_es', '<p>Tener en cuenta el <strong>clima de cusco</strong>, ya que puede ser muy lluvioso o muy soleado y el tour se ofrece con salidas diarias en 02 turnos (07:20am - 12:30pm o 13:30pm - 18:30pm) de 5 a 10 personas.</p>', 'Cuatrimotos en Maras (salineras) y moray', 'Podras apreciar unos paisajes del centro arqueológico cuatrimotos  de Moray y las asombrosas minas de sal del pueblo de Maras salineras.', 2, 1, 15, 40, 'https://i.postimg.cc/zfgSgjFG/tour-maras-moray-y-salineras-typicaltrips.jpg'),
(9, 'ciclismo-en-maras-y-moray', 'https://i.postimg.cc/fbDN6Bw1/Ciclismo.jpg', 'Ciclismo Maras Moray', '  Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van', 'Chequerec, Maras, Moray, Salineras, Cusco', '1', '18 a 40 años', 20, 1, '1 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tener en cuenta el<strong> clima de cusco</strong>, ya que puede ser muy lluvioso o muy soleado y el tour se ofrece con salidas diarias en el unico turno (08:40am - 18:30pm) de 2 a 6 personas.</p>\r\n</body>\r\n</html>', 'Ciclismo en maras y moray', 'explora la naturaleza y paisajes del centro arqueológico de Moray y las asombrosas minas de sal del pueblo de Maras salineras! todo en una cómoda bicicleta.', 2, 1, 18, 40, 'https://i.postimg.cc/Jzwhh50q/maras-bicicletas-typicaltrips.jpg'),
(10, 'inca-jungle-y-machu-picchu-4-dias-3-noches', 'https://i.postimg.cc/9M1JRvJc/image.jpg', 'Inca Jungle Classic a Machu Picchu 4 dias', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', ' Cusco, ollantaytambo, Montaña Abra Malaga, Ciclismo,  Santa Maria, Rafting, Santa Teresa, Idrolectica, Aguas Calientes, Machu Picchu, Cusco', '1', '18 a 40 años', 18, 4, '4 dias', 'en_es', '<p>Tener en cuenta que los ingresos a la ciudadela de <strong>machu picchu</strong> estan posibles en que varian en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>', 'Inca Jungle Tour al Machu Picchu ( 4D / 3N )', 'una experiencia maravillosa a lo largo de la selva inca mientras recorres el Camino a Machu Picchu, el Inca Jungle es una excursión de 4 días.', 2, 1, 18, 40, 'https://i.postimg.cc/5NyPt8T6/camino-inca-Jungle-typicaltrips.jpg'),
(11, 'inca-jungle-classic-a-machu-picchu-3-dias-2-noches', 'https://i.postimg.cc/wBg9yCjb/biking-inka-jungle-trail.jpg', 'Inca Jungle Trail Classic a Machu Picchu 3dias ', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Cusco, Hollantaytambo, Montaña Abra Malaga, Ciclismo, Santa Teresa, Rafting, Santa Maria, Aguas Calientes, Machu Picchu', '1', '14 a 40 años', 15, 3, '3 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tenga en consideración que los ingresos a la ciudadela de <strong>Machu Picchu</strong>, es probable que varíen en su programa de ingresos ya que tiene 6 programas diferentes.</p>\r\n</body>\r\n</html>', 'Caminata Selva Inka Classic a Machu Picchu 3D / 2N ', 'Inca Jungle una de las mejores rutas para llegar a Machu Picchu, haciendo actividades como mountain bike, canotaje y canopy, algo inolvidable!', 2, 1, 14, 40, 'https://i.postimg.cc/yN3Wj6xV/inca-jungle-typicaltrips.jpg'),
(12, 'laguna-de-humantay', 'https://i.postimg.cc/Vvxy4FGG/q3rrq3r13412.jpg', 'Laguna Humantay', ' Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van', 'Cusco, Mollepata , Soraypampa, Caminata, Laguna Humantay', '1', '10 a 60 años', 19, 1, '1 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tener en cuenta el<strong> clima de cusco</strong>, ya que puede ser muy lluvioso o muy soleado y el tour se ofrece con salidas diarias en grupos de 15 a 25 personas.</p>\r\n</body>\r\n</html>', 'tour Laguna Humantay - Humantay Trek Full Day', 'Tour de Laguna Humantay en una caminata inolvidable a la imponente y hermosa laguna de color turquesa, una excursión ideal para los amantes de la naturaleza.', 3, 1, 10, 60, 'https://i.postimg.cc/fy10D3Jg/laguna-humantay-typicaltrips.jpg'),
(13, 'ausangate-montana-de-colores-1-dia', 'https://i.postimg.cc/bJFfSPQj/ojbiiibibn.jpg', 'Montaña de Colores 1 dia', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van', 'Cusco, Pitumarca, Hanchipata – Quesuno,  Vinicunca', '1', '10 a 55 años', 20, 1, '1 dias', 'en_es', '<p> </p>\r\n<p>Tener en cuenta el clima de <strong>cusco</strong>, ya que puede ser muy lluvioso o muy soleado y el tour se ofrece con salidas diarias en grupos de 15 a 25 personas.</p>', 'Montaña de Colores 1 dia', 'un guia experto en espanol e ingles En este tour vamos a descubrir la increible montana de colores Vinicunca o tambien denominada Montana Arcoiris.', 3, 1, 10, 55, 'https://i.postimg.cc/7LDFLmzS/monta-a-colores-typicaltrips.jpg'),
(14, 'camino-inca-a-machu-picchu-2-dias', 'https://i.postimg.cc/pdHWPPBG/ytru.jpg', 'Camino Inca 2 dias', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Chachabamba, Wiñay Wayna, Inti Punku, Aguas Calientes,Machu Picchu', '1', '15 a 60 años', 25, 2, '2 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tener en cuenta que los ingresos a la ciudadela de <strong>machupicchu</strong> estan posibles en que varian en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>\r\n</body>\r\n</html>', 'Camino Inca 2 Dias', 'Recorreremos la ruta de un sendero que tiene mas de 500 años de antigüedad (disfrutando la belleza de la naturaleza).', 3, 1, 15, 60, 'https://i.postimg.cc/RFNxrd29/camino-inca-typicaltrips.jpg'),
(15, 'camino-inca-clasico-a-machu-picchu-4-dias', 'https://i.postimg.cc/sXTqZkJD/incatrail-6.jpg', 'Camino Inca 4 dias', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Urubamba, Llactapata, Wayllabamba, Warmiwañusca, Pacaymayo,  Wiñaywayna, Inti Punku,  Wiñaywayna, Machu Picchu , Cusco', '1', '10 a 65 años', 25, 4, '4 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Tener en cuenta que los ingresos a la <strong>ciudadela de machupicchu</strong> estan posibles en que varian en su horario de ingreso ya que cuenta con 6 horarios distintos.</p>\r\n</body>\r\n</html>', 'Camino Inca 4 Dias', 'En este viaje vivirás las experiencias de recorrer la legendaria ruta del pueblo Inca y que concluye con la llegada al sitio sagrado de Machu Picchu.', 3, 1, 10, 65, 'https://i.postimg.cc/Z5pMd2zC/camino-inca-machu-picchu-typicaltrips.jpg'),
(16, 'clasico-salkantay-trek-4-dias-3-noches', 'https://i.postimg.cc/kD3xtF6v/ae.jpg', 'Salkantay Trek 4 Dias', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Cusco, Mollepata. Soraypampa, Chaullay,  Hidroélectrica, Aguas Calientes, Machu Picchu, Cusco', '1', '10 a 55 años', 25, 4, '4 dias', 'en_es', '<p> </p>\r\n<p>Tener en cuenta que los ingresos a la ciudadela de <strong>machupicchu</strong> estan posibles en que varian en su horario de ingreso ya que cuenta con 6 programas distintos.</p>', 'Salkantay Trek  a Machu Picchu - Salkantay Cusco ', 'Salkanatay Trek 4 días a machu picchu es una de las formas de llegar a machu picchu visitando diversos lugares impresionantes y también la laguna Humantay', 3, 1, 10, 55, 'https://i.postimg.cc/Hx9J8pBB/Salkantay-trek-typicaltrips.jpg'),
(17, 'salkantay-trek-y-machu-picchu-5-dias-4-noches', 'https://i.postimg.cc/htFbjDc9/salkantay-a-machu-picchu.jpg', 'Trekking Salkantay a Machu Picchu Classic', 'Hotel Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Cusco , Mollepata, Soraypampa, Humantay lake, Wuayracpampa, Chaullay,La Playa, Santa Teresa, Hidroelectrica, Aguas Calientes, Machu Picchu, Cusco', '1', '10 a 55 años', 25, 5, '5 dias', 'en_es', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Hay que tener en cuenta que los ingresos a la <strong>ciudadela de machu picchu</strong>, es posible que var&iacute;en en su itinerario de ingresos ya que cuenta con 6 itinerarios diferentes.</p>\r\n</body>\r\n</html>', 'Trekking Salkantay a Machu Picchu ( 5D / 4N )', 'Salkantay trek 5 Dias : es una de las expediciones más impresionantes, es sin igual la mejor aventura. que te esta esperando!', 3, 1, 10, 55, 'https://i.postimg.cc/SxtQ5ntB/Salkantay-Trek-5d-typicaltrips.jpg'),
(18, 'maras-and-moray', 'https://www.mundomapi.com/images/tours/maras-y-moray-1552149919.jpg', 'Maras and Moray', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22002.69576010217!2d-72.16576153022461!3d-13.298641100000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x916dc2c50bc8d515%3A0x2232a3f3560d29c9!2sSalineras%20de%20Maras!5e1!3m2!1ses-419!2spe!4v1', 'van', 'Cusco, Maras, Moray, Salineras, Cusco', '1', '15 to 50 years old', 15, 1, '1 days', 'en_es', '<p>Take into account the climate of Cusco, as it can be very rainy or very sunny and the tour is offered with daily departures in groups of 15 to 20 people.</p>', 'Maras and Moray half day tour', 'You will be able to appreciate some landscapes of the archeological center of Moray and the amazing salt mines of the town of Maras salineras.', 4, 1, 15, 50, 'https://i.postimg.cc/zfgSgjFG/tour-maras-moray-y-salineras-typicaltrips.jpg'),
(19, 'the-incas-sacred-valley', 'https://i.postimg.cc/T325RQGT/image.jpg', 'the Incas\' Sacred Valley', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van', 'Cusco, Pisaq, Urubamba,Ollantaytambo,Cusco', '1', '8 to 65 years old', 20, 1, '1 days', 'en_es', '<p>Take into account the <strong>climate</strong> of Cusco, as it can be very rainy or very sunny and the tour is offered with daily departures in groups of 15 to 25 people.</p>\r\n<p>Confirmation will be received at the time of booking</p>\r\n<p>Since this activity is a group tour, we will pick up the first person at 07:30 and the last person at 08:10, please be ready in the lobby of your hotel at 07:30 and our guide has time to spend. until 08:10</p>', 'Sacred Valley from Cusco  - Pisac - Ollantaytambo', 'One of the best experiences you will have when touring the sacred valley of the Incas \"All Inclusive\".', 4, 1, 8, 65, 'https://i.postimg.cc/9F7J5mx6/maras-moray-completo-typicaltrips.jpg'),
(20, 'city-tour-cusco', 'https://i.postimg.cc/WpnJKHsc/32ny.jpg', 'Half Day Cusco City Tour', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van', 'Temple of the Sun Qoricancha, Sacsayhuaman, Qenqo, Pucapucara, Tambomachay.', '1', '5 to 60 years old', 25, 1, '1 days', 'en_es', '<p>The <strong>City Tour Cusco</strong> is a tour of the main tourist attractions of the city of <strong>Cusco, the ancient capital of the Incas.</strong></p>', 'City Tour Cusco: Catedral, Qoricancha, Sacsayhuamán, Qenqo', 'City Tour Cusco Enjoy the imperial city, with us and appreciate the wonders of the Inca empire.', 4, 1, 5, 60, 'https://i.postimg.cc/50t8qYT6/qoricancha-city-tour-cusco-typicaltrips.jpg'),
(21, 'machu-picchu-by-car-2-days', 'https://i.postimg.cc/1tRyNM2D/39896323802-a7a5d787f0-k.jpg', 'Machu Picchu By Car', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Santa teresa, Hidroelectrica, Aguas Calientes, Machu Picchu', '1', '5 to 60 years old', 30, 2, '2 days', 'en_es', '<p>Keep in mind that the <strong>income to the citadel of Machu Picchu</strong> are possible in that they vary in their schedule of income since it has 6 different schedules.</p>', 'Machu Picchu by car 2 days', 'Machu Picchu by Car tour, the most economical way to visit Machu Picchu wonder of the world. Includes lodging and guided tour.', 4, 1, 5, 60, 'https://i.postimg.cc/GhRd1yY6/MACHUPICHU-BY-CAR-2-D-typicaltrips.jpg'),
(22, 'machu-picchu-1-day-by-train', 'https://i.postimg.cc/SNnNrTsr/Tren.jpg', 'Machu Picchu 1 day', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Ollantaytambo, Aguas Calientes, Machu Picchu, Cusco', '1', '5 to 60 years old', 15, 1, '1 days', 'en_es', '<p>Bear in mind that the income to the citadel of machupicchu are possible in that they vary in their schedule of income since it has 6 different schedules.</p>', 'Machu Picchu one day by train', 'Tour for those who have little time and only want to know Machu Picchu, enjoy Machu Picchu in one day!', 4, 1, 5, 60, 'https://i.postimg.cc/d3G7VZqD/machu-picchu-1-dia-typicaltrips.jpg'),
(23, 'sunrise-in-machu-picchu-2-days', 'https://i.postimg.cc/dt3DfQWG/798iujboiugoihg654654654.jpg', 'Sunrise in Machu Picchu', 'Hotel, Cusco', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Ollantaytambo, Aguas Calientes, Machu Picchu', '1', '5 to 60 years old', 30, 2, '2 days', 'en_es', '<p>Bear in mind that the income to the citadel of machupicchu are possible in that they vary in their schedule of income since it has 6 different schedules.</p>', 'Sunrise in Machu Picchu', 'The best option to know Machu Picchu without hurry and enjoy the wonders that nature offers us. Also enjoy an unforgettable visit to the historical sanctuary of Machu Picchu.', 4, 1, 5, 60, 'https://i.postimg.cc/W3ZNjRn7/Machupicchu-amacener-typicaltrips.jpg'),
(24, 'sacred-valley-of-the-incas-connection-machu-picchu', 'https://i.postimg.cc/8zvJ5DbD/Valle-Sagrado-y-Machu-Picchu-3.jpg', 'Sacred Valley and Machu Picchu', 'Hotel Cusco', 'San Francisco cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Pisaq, Urubamba, Hollantaytambo, Aguas Calientes, Machu Picchu', '1', '8 to 65 years old', 20, 2, '2 days', 'en_es', 'Keep in mind that the income to the Machupicchu Citadel is possible in that they vary in your admission schedule since it has 6 different schedules.', 'Sacred Valley and Machu Picchu', 'Appreciate the Inca culture in our two day tour to the Sacred Valley and Machu Picchu.', 4, 1, 8, 65, 'https://i.postimg.cc/sgGRzxDr/Tour-Valle-sagrado-incas-typical.jpg'),
(25, 'maras-and-moray-in-atv', 'https://i.postimg.cc/hvQH2GJS/75426209-787734838334064-5060491484028320274-n.jpg', 'ATVs Maras Moray', 'Cusco, Peru', 'Cusco, Peru', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', '', 'Maras and Moray', '1', '10 to 40 years old', 0, 1, '1 days', 'en_es', '<p>Take into account the weather of Cusco, as it can be very rainy or very sunny and the tour is offered with daily departures in 02 shifts (07:20 am - 12:30 pm or 13:30 pm - 18:30 pm) from 5 to 10 people .</p>', 'ATVs in Maras and moray', 'You will be able to appreciate some landscapes of the archeological center of Moray and the amazing salt mines of the town of Maras salineras.', 5, 1, 10, 40, 'https://i.postimg.cc/zfgSgjFG/tour-maras-moray-y-salineras-typicaltrips.jpg'),
(26, 'biking-in-maras-and-moray', 'https://i.postimg.cc/y6mWwBTS/biking-5.jpg', 'Biking Maras Moray', 'Cusco, Peru', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van', 'Chequerec, Maras, Moray, Salineras, Cusco', '3', '0 to 0 years old', 20, 1, '1 days', 'en_es', '<p>Take into account the <strong>climate of Cusco</strong>, as it can be very rainy or very sunny and the tour is offered with daily departures in the only shift (08:40am - 18:30pm) from 2 to 6 people.</p>', 'Biking in Maras and Moray', 'Explore the nature and landscapes of the archaeological centre of Moray and the amazing salt mines of the village of Maras salineras! all on a comfortable bike.', 5, 1, 0, 0, 'https://i.postimg.cc/Jzwhh50q/maras-bicicletas-typicaltrips.jpg'),
(27, 'Inca-jungle-and-machu-picchu-4-days-3-nights', 'https://i.postimg.cc/qqrCfFK0/hakuimg12.jpg', 'Inka Jungle Trail 4 Days', 'Cusco, Peru', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', ' Cusco, ollantaytambo, Abra Malaga Mountain, Cycling, Santa Maria, Rafting, Santa Teresa, Idrolectica, Aguas Calientes, Machu Picchu, Cusco', '1', '5 to 60 years old', 15, 4, '4 days', 'en_es', '<p>Keep in mind that the income to the Machupicchu Citadel is possible in that they vary in your admission schedule since it has 6 different schedules.</p>', 'Inka Jungle Trail 4 Days', 'a wonderful experience along the Inca jungle while walking the Machu Picchu Trail, the Inca Jungle is a 4 day excursion.', 5, 1, 5, 60, 'https://i.postimg.cc/5NyPt8T6/camino-inca-Jungle-typicaltrips.jpg'),
(28, 'inca-jungle-to-machu-picchu-3-days-2-nights', 'https://i.postimg.cc/25Bq1YFr/jungle-4094-20181113.jpg', 'Inka Jungle Classic 3 Days', 'Cusco, Peru', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Cusco, Hollantaytambo, Montaña Abra Malaga, Ciclismo, Santa Teresa, Rafting, Santa Maria, Aguas Calientes, Machu Picchu', '1', '15 to 60 years old', 15, 3, '3 days', 'en_es', '<p>Keep in mind that income to the citadel of <strong>Machu Picchu</strong> is likely to vary in your income program as it has 6 different programs.</p>', 'Inka Jungle Classic 3 Days', 'Inca Jungle one of the best routes to reach Machu Picchu, doing activities such as mountain biking, canoeing and canopy, something unforgettable!', 5, 1, 15, 60, 'https://i.postimg.cc/yN3Wj6xV/inca-jungle-typicaltrips.jpg'),
(29, 'humantay-lake', 'https://i.postimg.cc/rmwYchL6/humantay-7.jpg', 'Humantay Lake', 'Cusco, Peru', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van', 'Cusco, Mollepata , Soraypampa, Caminata, Laguna Humantay', '1', '15 to 60 years old', 15, 1, '1 days', 'en_es', '<p>Take into account the <strong>climate of Cusco</strong>, as it can be very rainy or very sunny and the tour is offered with daily departures in groups of 15 to 25 people.</p>', 'Laguna Humantay tour - Humantay Trek Full Day', 'Tour of Laguna Humantay in an unforgettable walk to the imposing and beautiful turquoise lagoon, an ideal excursion for nature lovers.', 6, 1, 15, 60, 'https://i.postimg.cc/fy10D3Jg/laguna-humantay-typicaltrips.jpg'),
(30, 'ausangate-raimbow-mountain-1-day', 'https://i.postimg.cc/bJFfSPQj/ojbiiibibn.jpg', 'Rainbow mountain full day', 'Cusco, Peru', 'san francisco , cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van', 'Cusco, Pitumarca, Hanchipata – Quesuno,  Vinicunca', '1', '5 to 56 years old', 20, 1, '1 days', 'en_es', '<p>Take into account the climate of Cusco, as it can be very rainy or very sunny and the tour is offered with daily departures in groups of 15 to 25 people.</p>', 'Rainbow mountain full day', 'an expert guide in Spanish and English In this tour we will discover the incredible colorful mountain Vinicunca or also called Rainbow Mountain.', 6, 1, 5, 56, 'https://i.postimg.cc/7LDFLmzS/monta-a-colores-typicaltrips.jpg'),
(31, 'Inca-Trail-2-Days', 'https://i.postimg.cc/pdHWPPBG/ytru.jpg', 'Inca Trail 2 days', 'Cusco, Peru', 'San Francisco, Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Chachabamba, Wiñay Wayna, Inti Punku, Aguas Calientes,Machu Picchu', '3', '5 to 60 years old', 15, 2, '2 days', 'en_es', '<p>Keep in mind that the income to the <strong>machupicchu citadel</strong> is possible in that they vary in their admission schedule since it has 6 different schedules.</p>', 'Inca Trail 2 Days', 'We will walk the route of a path that is more than 500 years old (enjoying the beauty of nature).', 6, 1, 5, 60, 'https://i.postimg.cc/RFNxrd29/camino-inca-typicaltrips.jpg'),
(32, 'classic-inca-trail-trek-4-days', 'https://i.postimg.cc/sXTqZkJD/incatrail-6.jpg', 'Inca Trail Classic 4 days', 'Hotel Cusco', 'San Francisco ,Cusco', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'car, van, train', 'Cusco, Mollepata', '1', '10 to 55 years old', 25, 4, '4 days', 'en_es', 'Keep in mind that the income to the machupicchu citadel is possible in that they vary in their admission schedule since it has 6 different schedules.', 'Inca Trail Classic 4 Days', 'In this trip you will live the experiences of traveling the legendary route of the Inca people and that concludes with the arrival to the sacred site of Machu Picchu.', 6, 1, 10, 55, 'https://i.postimg.cc/Z5pMd2zC/camino-inca-machu-picchu-typicaltrips.jpg'),
(33, 'salkantay-trek-classic-4-days-3-nights', 'https://i.postimg.cc/Fzvhs5GW/salkantay-1.jpg', 'Salkantay Trek Classic 4 days', 'Cusco, Peru', 'Cusco, Peru', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', 'van, train', 'Machu Picchu', '1', '15 to 50 years old', 16, 4, '4 days', 'en_es', '<p>Keep in mind that the income to <strong>the machupicchu citadel</strong> is possible in that they vary in their admission schedule since it has 6 different schedules.</p>', 'Classic Salkantay trek 4 Days', 'Salkantay trek 4 Days : it is one of the most impressive expeditions, it is without equal the best adventure.', 6, 1, 15, 50, 'https://i.postimg.cc/SxtQ5ntB/Salkantay-Trek-5d-typicaltrips.jpg'),
(34, 'salkantay-trek-to-machu-picchu-5-days-4-nights', 'https://i.postimg.cc/5NXwjv7v/salkantay-1862-20190720.jpg', 'Salkantay Trek 5 days', 'Cusco, Peru', '', 'https://cdn.tourradar.com/s3/natural_destinations/263/1200/lluiU9.png', '', '', '', '', 0, 5, '5 days', 'Spanish and English', 'Keep in mind that the income to the machupicchu citadel is possible in that they vary in their admission schedule since it has 6 different schedules.', 'Salkantay trek 5 Days', 'In this 5-day tour that is one of the most impressive treks in Cusco This great hike will allow us to see incredible landscapes, imposing snowy peaks, waterfalls, rivers and small towns, it is nature in its purest form', 6, 1, 0, 0, 'https://i.postimg.cc/SxtQ5ntB/Salkantay-Trek-5d-typicaltrips.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tourbookings`
--

CREATE TABLE `tourbookings` (
  `IdTourBooking` varchar(255) NOT NULL,
  `IdBooking` varchar(255) NOT NULL,
  `IdTour` int(11) NOT NULL,
  `CantPaxAd` int(11) NOT NULL,
  `CantPaxJv` int(11) NOT NULL,
  `CantPaxNn` int(11) NOT NULL,
  `TourStarDate` date NOT NULL,
  `TourPrice` float(16,2) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tourbookings`
--

INSERT INTO `tourbookings` (`IdTourBooking`, `IdBooking`, `IdTour`, `CantPaxAd`, `CantPaxJv`, `CantPaxNn`, `TourStarDate`, `TourPrice`, `updated_at`, `created_at`) VALUES
('TB-1YH7YNZM', 'TP-VUAVB9UO', 2, 3, 0, 0, '2019-11-20', 360.00, '2019-11-03 18:42:17', '2019-11-03 18:42:17'),
('TB-27J1G69B', 'TP-G5IV8FF4', 26, 5, 5, 2, '2020-02-14', 540.00, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TB-4YO5N3KU', 'TP-4ERSZ2T2', 3, 0, 3, 0, '2019-10-27', 360.00, '2019-11-03 20:10:08', '2019-11-03 20:10:08'),
('TB-7RPYFS47', 'TP-1C6S8R6M', 2, 3, 0, 0, '2019-11-20', 360.00, '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TB-BPMQOBD4', 'TP-1C6S8R6M', 3, 10, 10, 0, '2019-11-05', 2702.00, '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TB-HYQGUFAK', 'TP-4RRYDQH3', 2, 1, 0, 0, '2019-11-05', 120.00, '2019-11-03 19:57:36', '2019-11-03 19:57:36'),
('TB-KO258GNC', 'TP-4ERSZ2T2', 2, 2, 0, 0, '2019-11-03', 240.00, '2019-11-03 20:10:08', '2019-11-03 20:10:08'),
('TB-YDYACL2U', 'TP-DZDXCYKT', 2, 1, 0, 0, '2019-11-05', 120.00, '2019-11-03 19:56:31', '2019-11-03 19:56:31'),
('TB-Z912KG93', 'TP-3PL1YW6W', 18, 0, 1, 1, '2019-12-19', 50.00, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TB-ZAGLIMHK', 'TP-F8G4KJCS', 2, 1, 0, 0, '2019-11-05', 120.00, '2019-11-03 19:46:19', '2019-11-03 19:46:19'),
('TB-ZKL51JH4', 'TP-G5IV8FF4', 8, 1, 3, 0, '2019-12-19', 345.00, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tourincluded`
--

CREATE TABLE `tourincluded` (
  `IdTourIncluded` int(11) NOT NULL,
  `Title` text NOT NULL,
  `Details` text NOT NULL,
  `IdTour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tourincluded`
--

INSERT INTO `tourincluded` (`IdTourIncluded`, `Title`, `Details`, `IdTour`) VALUES
(22, 'Transportes', '<p>Recojo del sus respectivos alojamientos; Transporte en todo el tour;&nbsp;</p>', 1),
(23, 'Ticket', '<p><a title=\"ticket parcial\" href=\"../../blog/ticket-parcial\" target=\"_blank\" rel=\"noopener\">Ticket Parcial</a> ( Chinchero, Moray); Ticket de Ingreso a las Minas de Sal</p>', 1),
(24, 'Guia ', '<p><strong>Guia Bilingue</strong>; El guia esta con el grupo en todo el viaje&nbsp;</p>', 1),
(26, 'Transportes', '<p>Recojo desde su Alojamiento; Todo El Recorio hasta llegar a cusco</p>', 2),
(27, 'Boletos', '<p> </p>\r\n<p>Boleto para ingresara Pisac;Boleto Para Ingresar a Ollantaytabo</p>', 2),
(28, 'Guia', '<p> </p>\r\n<p>Guia bilingue con experiencia en la ruta</p>', 2),
(29, 'Almuerzo ', '<p> </p>\r\n<p>x1 buefett en urubamba</p>', 2),
(30, 'Bus de turismo', '', 3),
(31, 'Recojo del hotel', '', 3),
(32, 'Guía profesional', '', 3),
(33, 'Boleto Turístico', '', 3),
(34, 'Conductor experto en la zona', '', 4),
(35, 'Recojo del Hotel', '', 4),
(36, 'Alojamiento', '', 4),
(37, 'Trasporte Privado', '(Cusco-Hidroelectrica)\r\n(hidroeléctrica-Cusco)', 4),
(38, 'Comida', 'x1 Desayuno; x1 Almuerzo; x1 Cena', 4),
(39, 'Boleto de ingreso al Santuario de Machu Picchu', '', 4),
(40, 'Guiado en el Santuario Machu Picchu', '', 4),
(41, 'Conductor experto en la zona', '', 5),
(42, 'Recojo del Hotel', '', 5),
(43, 'Traslado Cusco - Ollantaytambo (trasporte Privado)', '', 5),
(44, 'Pasajes de Tren de ida y vuelta', '(Ollantaytambo - Machu picchu) : Expedition', 5),
(45, 'Boleto de ingreso al Santuario de Machu Picchu', '', 5),
(46, 'Ticket de Bus en Machu Picchu', ' (Subida y Bajada)', 5),
(47, 'Recojos del estacion de Tren', '', 5),
(48, 'Servicio de trasporte de ollantaytambo Hasta cerca del hotel', '', 5),
(49, 'Conductor experto en la zona', '', 6),
(50, 'Recojo del Hotel', '', 6),
(51, 'Traslado Cusco - Ollantaytambo (trasporte Privado)', '', 6),
(52, 'Pasajes de Tren de ida y vuelta', ' (Ollantaytambo - Machu picchu) : Expedition', 6),
(53, 'Boleto de ingreso al Santuario de Machu Picchu', '', 6),
(54, 'Ticket de Bus en Machu Picchu ', '(Subida y Bajada)', 6),
(55, 'Recojos del estacion de Tren', '', 6),
(56, 'Servicio de trasporte de ollantaytambo Hasta cerca del hotel', '', 6),
(57, 'Transporte privado', '', 7),
(58, 'Boleto Parcia', ' ( Pisaq, Ollantaytambo )', 7),
(59, 'Guia Local', '', 7),
(60, 'Habitacion privada en Aguas Calientes', '', 7),
(61, 'Comida', 'x1 desayuno\r\nx1 Almuerzo buefett\r\n', 7),
(62, 'Ticket de Ingreso al santuario de Machu Pìcchu', '', 7),
(63, 'Ticket de Tren de Ollantaytambo - Aguas Calientes', '', 7),
(64, 'Ticket de Tren de Aguas Calientes - Ollantatambo', '', 7),
(65, 'Trasporte privado Ollantaytambo - cusco', '', 7),
(66, 'Transporte privado de ida y retorno', '', 8),
(67, 'Visita de Moray y Maras (entrada no incluida)', '', 8),
(68, 'Cuatrimotos personales ', '<p>250cc Honda / Yamaha</p>', 8),
(69, 'Equipo de seguridad', '<p>casco; lentes</p>', 8),
(70, 'Guía profesional mecánico en ingles/español', '', 8),
(71, 'Botiquín de primeros auxilios y una botella de agua mineral', '', 8),
(72, 'Recogida en el hotel / hostal.', '', 10),
(73, 'Transporte en autobús.', '', 10),
(74, 'Bicicletas con equipo de seguridad.', '', 10),
(75, 'Guía profesional bilingüe.', '', 10),
(76, 'Todas las comidas', '', 10),
(77, 'Entrada gratuita a Machu Picchu.', '', 10),
(78, 'Boleto de tren de Aguas Calientes a Ollantaytambo.', '', 10),
(79, 'Primeros auxilios', '', 10),
(80, 'Transporte', '<p>Recojo del Hotel el primer dia; Traslado cusco  - Abra Malaga; Traslado Santa Maria - Santa Teresa; Traslado el tercer día del estación de ollantaytambo a Cusco </p>', 11),
(82, 'Actividades', '<p><strong>Bicicleta de Montaña</strong></p>\r\n<ul>\r\n<li>Casco</li>\r\n<li>Guante</li>\r\n<li>Body( chaleco de segurida)</li>\r\n<li>Rodilleras</li>\r\n<li>Bicicleta</li>\r\n</ul>\r\n<p><strong>Rafting</strong></p>\r\n<ul>\r\n<li>Casco</li>\r\n<li>Remo</li>\r\n<li>Chaleco de Salva Vidas</li>\r\n</ul>\r\n<p><strong>Zip Line </strong></p>\r\n<ul>\r\n<li>Casco</li>\r\n<li>Polea de Segurida</li>\r\n<li>Guantes</li>\r\n</ul>', 11),
(83, 'Guía', '<p>Guia berlingue y cuenta con mas de 5 años de experiencia en la ruta </p>', 11),
(84, 'Comidas', '<p>x2 Desayuno; x2 Almuezo; x2 Cena; <strong>Hay la opción vegetariana</strong></p>', 11),
(85, 'Ticket a Machu Picchu', '', 11),
(86, 'Tren de Retorno a Ollantaytambo', '', 11),
(87, 'Primeros auxilios', '', 11),
(88, 'Conductor experto en la zona', '', 12),
(89, 'Comida', 'x1 Desayuno;x1 Almuerzo con opción vegetariana\r\n', 12),
(90, 'Entradas al pueblo de Mollepata.', '', 12),
(91, 'Kit de primeros auxilios.', '', 12),
(92, 'Guia profesional', '', 12),
(93, 'Trasponte Cusco - Mollepata - Cusco', '', 12),
(94, 'Conductor experto en la zona', '', 13),
(95, 'Comida', 'x1 Desayuno;\r\nx1 Almuerzo con opción vegetariana\r\n', 13),
(96, 'Entradas al pueblo de Mollepata.', '', 13),
(97, 'Kit de primeros auxilios.', '', 13),
(98, 'Guia profesional', '', 13),
(99, 'Ticket de ingreso a la Montaña de colores', '', 13),
(100, 'Trasponte Cusco -Cusipata - Cusco', '', 13),
(101, 'Entrada al Camino Inca y Machu Picchu.', '', 14),
(102, 'Charla informativa del guía la noche anterior al viaje.', '', 14),
(103, 'Traslados Hotel / Estación de tren (Cusco - Ollantaytambo)', '', 14),
(104, 'Boleto de tren ida y vuelta (Aguas Calientes - Ollantaytambo)', '', 14),
(105, 'Traslados estación de Ollantaytambo - Cusco.', '', 14),
(106, 'Guía profesional (español / inglés).', '', 14),
(107, 'Comidas y alojamiento del primer día en Machu Picchu (Aguas Calientes).', '', 14),
(108, 'Bus ida y vuelta a Machu Picchu en el segundo día del tour.', '', 14),
(109, 'Recogida en el hotel / hostal.', '', 16),
(110, 'Transporte en autobús.', '', 16),
(111, 'Guía profesional', '', 16),
(112, 'Todas las comidas', '', 16),
(113, 'Entrada a Machu Picchu.', '', 16),
(114, 'Boleto de tren de Aguas Calientes a Ollantaytambo.', '', 16),
(115, 'Primeros auxilios', '', 16),
(116, '5 Kilos por persona', '', 16),
(117, '2 Noches en campamento 1 Noche en hostal', '', 16),
(118, 'Ingreso a la Montaña de Salkantay', '', 16),
(119, 'Recogida en el hotel / hostal.', '', 17),
(120, 'Transporte en autobús.', '', 17),
(121, 'Guía profesional', '', 17),
(122, 'Todas las comidas', '', 17),
(123, 'Entrada a Machu Picchu.', '', 17),
(124, 'Boleto de tren de Aguas Calientes a Ollantaytambo.', '', 17),
(125, 'Primeros auxilios', '', 17),
(126, '5 Kilos por persona', '', 17),
(127, '3 Noches en campamento 1 Noche en hostal', '', 17),
(128, 'Ingreso a la Montaña de Salkantay', '', 17),
(129, 'Boleto de Tren Expedition a Ollantaytambo', '', 17),
(130, 'Private round trip transportation', '<p>Pick up from their respective lodgings; Transportation throughout the tour; </p>', 18),
(131, 'Ticket', '<p>Partial Ticket (Chinchero, Moray); Entrance Ticket to the Salt Mines</p>', 18),
(132, 'Guide', '<p>Bilingual guide; The guide is with the group throughout the trip</p>', 18),
(135, 'Private transport in and out', '<p>Pick up from your Lodging; All the way to Cusco</p>', 19),
(136, 'Tickets', '<p>Ticket to enter Pisac; Ticket to enter Ollantaytabo</p>', 19),
(137, 'Local Guide', '<p>Bilingual guide with experience in the route</p>', 19),
(138, 'Lunch', '<p>x1 buefett in Urubamba</p>', 19),
(139, 'Tourist bus', '', 20),
(140, 'Pick up from hotel', '', 20),
(141, 'Professional guide', '', 20),
(142, 'Tourist Ticket', '', 20),
(143, 'Expert driver in the area', '', 21),
(144, 'Hotel Pickup', '', 21),
(145, 'Accommodation', '', 21),
(146, 'Private transport (Cusco-Hidroelectrica)', '<p>(Cusco-Hydroelectrica) (hydroelectrica-Cusco)</p>', 21),
(147, 'Food', '<p>x1 Breakfast; x1 Lunch; x1 Dinner</p>', 21),
(148, 'Guided tour of the Machu Picchu Sanctuary', '', 21),
(150, 'Guided in the Sanctuary Machu Picchu', '', 21),
(151, 'Expert driver in the area', '', 22),
(152, 'Hotel Pickup', '', 22),
(153, 'Transfer Cusco - Ollantaytambo (Private transportation)', '', 22),
(154, 'Return train tickets (Ollantaytambo - Machu picchu) : Expedition', '', 22),
(155, 'Entrance ticket to Machu Picchu Sanctuary', '', 22),
(156, 'Bus Ticket in Machu Picchu (Ascent and Descent)', '', 22),
(157, 'Pick ups from the train station', '', 22),
(158, 'Transport service from Ollantaytambo to near the hotel', '', 22),
(159, 'Private transport', '', 24),
(160, 'Boleto Parcia ( Pisaq, Ollantaytambo )', '', 24),
(161, 'Local Guide', '', 24),
(162, 'Private room in Aguas Calientes', '', 24),
(163, 'Food', 'x1 breakfast;\r\nx1 Lunch buefett', 24),
(164, 'Ticket of Entrance to the Sanctuary of Machu Pìcchu', '', 24),
(165, 'Ollantaytambo Train Ticket - Aguas Calientes', '', 24),
(166, 'Water Train Ticket - Ollantatambo', '', 24),
(167, 'Private transport Ollantaytambo - cusco', '', 24),
(168, 'Private transport to and from the airport', '', 25),
(169, 'Visit of Moray and Maras (entrance fee not included)', '', 25),
(170, 'Personal ATVs ', '<p>250cc Honda / Yamaha</p>', 25),
(171, 'Safety equipment', '<p>helmet; lentes</p>', 25),
(172, 'Professional mechanical guide in English / Spanish', '', 25),
(173, 'First aid kit and a bottle of mineral water', '', 25),
(174, 'Pick up at the hotel / hostel.', '', 27),
(175, 'Transportation by bus.', '', 27),
(176, 'Bicycles with safety equipment.', '', 27),
(177, 'Professional bilingual guide.', '', 27),
(178, 'All the foods', '', 27),
(179, 'Train ticket from Aguas Calientes to Ollantaytambo.', '', 27),
(180, 'Free admission to Machu Picchu.', '', 27),
(181, 'First aid', '', 27),
(182, 'Transport', '<p>Pick up from Hotel the first day; <strong>Transfer Cusco</strong> - <strong>Abra Malaga</strong>; <strong>Transfer Santa Maria</strong> - <strong>Santa Teresa</strong>; Transfer the third day of the station ollantaytambo to <strong>Cusco </strong></p>', 28),
(183, 'Activities', '<h4>Mountain Biking</h4>\r\n<ul>\r\n<li>Helmet</li>\r\n<li>Glove</li>\r\n<li>Body (safety vest)</li>\r\n<li>Kneepads</li>\r\n<li>Bicycle</li>\r\n</ul>\r\n<p><strong>Rafting</strong></p>\r\n<ul>\r\n<li>Bicycle</li>\r\n<li>Helmet</li>\r\n<li>Rowing</li>\r\n<li>Life Saving Vest</li>\r\n</ul>\r\n<p><strong>Zip Line</strong></p>\r\n<ul>\r\n<li>Rowing</li>\r\n<li>Helmet</li>\r\n<li>Safety pulley</li>\r\n<li>Gloves</li>\r\n</ul>', 28),
(185, 'Bilingual professional guide.', '<p>Guide berlingue and has more than 5 years of experience on the route </p>', 28),
(186, 'All meals', '<p>Guide berlingue and has more than 5 years of experience on the route </p>', 28),
(187, 'Free entrance to Machu Picchu.', '<p>x2 Breakfast; x2 Lunch; x2 Dinner; Vegetarian option available</p>', 28),
(188, 'Return Train to Ollantaytambo', '', 28),
(189, 'First aid', '', 28),
(190, 'Expert driver in the area', '', 29),
(191, 'Food', 'x1 Breakfast; x1 Lunch with vegetarian option\r\n', 29),
(192, 'Tickets to the town of Mollepata.', '', 29),
(193, 'First aid box.', '', 29),
(194, 'Professional guide.', '', 29),
(195, 'Trasponte Cusco - Mollepata - Cusco', '', 29),
(196, 'Expert driver in the area', '', 30),
(197, 'Food', 'x1 Breakfast; x1 Lunch with vegetarian option\r\n', 30),
(198, 'Entries to the village of Mollepata.', '', 30),
(199, 'First aid kit.', '', 30),
(200, 'Professional guide', '', 30),
(201, 'Ticket of entrance to the Mountain of colors', '', 30),
(202, 'Trasponte Cusco -Cusipata - Cusco', '', 30),
(203, 'Entrance to the Inca Trail and Machu Picchu.', '', 31),
(204, 'Informative talk from the guide the night before the trip .', '', 31),
(205, 'Transfers Hotel / Train Station (Cusco - Ollantaytambo)', '', 31),
(206, 'Round trip train ticket (Aguas Calientes - Ollantaytambo)', '', 31),
(207, 'Transfer from Ollantaytambo station - Cusco.', '', 31),
(208, 'Professional guide (Spanish / English).', '', 31),
(209, 'Meals and overnight accommodation in Machu Picchu (Aguas Calientes).', '', 31),
(210, 'Round trip bus to Machu Picchu on the second day of the tour.', '', 31),
(211, 'Pick up at the hotel / hostal.', '', 33),
(212, 'Bus transportation.', '', 33),
(213, 'Professional guide', '', 33),
(214, 'All meals', '', 33),
(215, 'Entrance to Machu Picchu.', '', 33),
(216, 'Train ticket from Aguas Calientes to Ollantaytambo.', '', 33),
(217, 'First aid', '', 33),
(218, '2 Nights in camp 1 Night in hostel', '', 33),
(219, '5 Kilos per person', '', 33),
(220, 'Entrance to Salkantay Mountain', '', 33),
(221, 'Pick up at the hotel / hostal.', '', 34),
(222, 'Bus transportation.', '', 34),
(223, 'Professional guide', '', 34),
(224, 'All meals', '', 34),
(225, 'Entrance to Machu Picchu.', '', 34),
(226, 'Train ticket from Aguas Calientes to Ollantaytambo.', '', 34),
(227, 'First aid', '', 34),
(228, '5 Kilos per person', '', 34),
(229, '2 Nights in camp 1 Night in hostel', '', 34),
(230, 'Entrance to Salkantay Mountain', '', 34),
(231, 'Entrance to Salkantay Mountain', '', 34),
(232, 'Entrance ticket to the citadel of Machupicchu.', '', 32),
(233, 'Chef', '', 32),
(234, 'Expert driver in the area', '', 23),
(235, 'Hotel Pickup', '', 23),
(236, 'Transfer Cusco - Ollantaytambo (Private transportation)', '', 23),
(237, 'Return train tickets (Ollantaytambo - Machu picchu) : Expedition', '', 23),
(238, 'Entrance ticket to Machu Picchu Sanctuary', '', 23),
(239, 'Bus Ticket in Machu Picchu (Ascent and Descent)', '', 23),
(240, 'Pick ups from the train station', '', 23),
(241, 'Transport service from Ollantaytambo to near the hotel', '', 23),
(242, 'Protective equipment (helmet, knee brace, body and gloves)', '', 26),
(243, ' Bilingual guided tour:', '', 26),
(244, 'Guided visit to the archeological centers (la salineras and moray)', '', 26),
(245, 'Transfer by regular bus from Cusco to Cruspata', '', 26),
(246, 'Transfer of Cruspata to the center of Cusco', '', 26),
(247, 'Porters to carry cooking and camping equipment', '', 32),
(248, '4 seater tent for 2 passengers', '', 32),
(249, 'Inflatable mattress', '', 32),
(250, ' Bilingual guided tour:', 'Entrance to the Inca Trail;\r\nGuided tour for two hours in the citadel of Machu Picchu', 32),
(251, ' Transfers:', 'Transfer by regular bus from Cusco to Ollantaytambo;\r\nTransfer back in tren (EXPEDITION | VOYAGER) from Aguas Calientes to Ollantaytambo;\r\nTransfer from Ollantaytambo station to downtown Cusco\r\n', 32),
(252, ' Feeding:', 'Dinner in hot waters on the first day;\r\nBuffet breakfast on the second day before heading to Machu Picchu', 32),
(253, 'Transporte de Ida – Retorno', '', 9),
(254, 'Guía profesional Español – English', '', 9),
(255, 'Equipo completo (Bicicleta profesional de montaña, casco, guantes, coderas y rodilleras).', '', 9),
(256, 'Box lunch. ', '', 9),
(257, 'Boleto de ingreso a la ciudadela de Machupicchu', '', 15),
(258, 'Visita guiada por dos horas en la ciudadela de Machu Picchu', '', 15),
(259, 'Traslado en bus regular de Cusco a Ollantaytambo', '', 15),
(260, 'Traslado de la estación de Ollantaytambo al centro de Cusco', '', 15),
(261, 'Transferencia vuelta en tren (EXPEDITION | VOYAGER) de Aguas Calientes a Ollantaytambo', '', 15),
(262, 'Entrada al Camino Inca', '', 15),
(263, 'Porteadores para llevar equipo de cocina y campamento', '', 15),
(264, 'Tienda de 4 plazas para 2 pasajeros', '', 15),
(265, 'Colchón inflable', '', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tournoincluded`
--

CREATE TABLE `tournoincluded` (
  `IdTourNoIncluded` int(11) NOT NULL,
  `Title` text NOT NULL,
  `Details` text NOT NULL,
  `IdTour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tournoincluded`
--

INSERT INTO `tournoincluded` (`IdTourNoIncluded`, `Title`, `Details`, `IdTour`) VALUES
(218, 'Comidas', '<p>*Desayuno (Puedes aprovecha mientras visitamos el centro textil de chinchero); Almuezo; Cena</p>', 1),
(221, 'Propinas', '<p><strong>Las propinas</strong>, son a voluntad de cada uno</p>', 2),
(222, 'Comidas', '<p>Desayuno;Cena</p>', 2),
(225, 'Comidas', '', 3),
(226, 'Gastos Personales', '', 4),
(227, 'El Desayuno del primer dia', '', 4),
(228, 'El Almuerzo, Cena del ultimo dia', '', 4),
(229, 'Productos de higiene personal', '', 4),
(230, 'Bus de Subida ', '( USD 12,00 Aguas Calinetes - Santuario Machu Picchu) ( Disponible Como extra)', 4),
(231, 'Machu Picchu Montaña', '( USD 20,00 ) ( Disponible Como extra)', 4),
(232, 'Huayna Picchu', ' ( USD 20,00 ) ( Disponible Como extra)', 4),
(233, 'Alimentacion', '', 5),
(234, 'Gastos mensionados en el Itinerario', '', 5),
(235, 'Productos de higiene personal', '', 5),
(236, 'Ingreso a Huayna Picchu Montaña', ' ( USD 20,00) ( Disponible Como extra )', 5),
(237, 'Ingreso a Machu Picchu Montaña', '( USD 20,00) ( Disponible Como extra)', 5),
(238, 'Alimentacion', '', 6),
(239, 'Gastos mensionados en el Itinerario', '', 6),
(240, 'Productos de higiene personal', '', 6),
(241, 'Ingreso a Huayna Picchu Montaña ', ' ( USD 20,00) ( Disponible Como extra )', 6),
(242, 'Ingreso a Machu Picchu Montaña', '( USD 20,00) ( Disponible Como extra)', 6),
(243, 'Propinas', '', 7),
(244, 'Desayuno , Cena del primer dia', '', 7),
(245, 'Almuezo y cena del Ultimo dia', '', 7),
(246, 'Otros servicios no mencionados', '', 7),
(247, 'Bus subida de Subida O bajada', ' (Aguas Calientes - Santuario de Machu Picchu 24,00 USD) ( Disponible Como extra)', 7),
(248, 'Seguro contra accidente personal', '', 8),
(249, 'Propinas', '', 8),
(250, 'Alimentos', '', 8),
(251, 'Ingresos a los lugares a visitar en caso que no tengas el boleto turístico (deberás pagarlo en el lugar mismo):', '<p>Moray (+PEN 70 ), (+PEN 40) para peruanos; si tienen el boleto turístico no tienen que pagar por la entrada; Maras (+PEN 15), (+PEN 10) para peruanos</p>', 8),
(252, 'Desayuno el primer día.', '', 10),
(253, 'Almuerzo el último día.', '', 10),
(254, 'Aperitivos y guarniciones.', '', 10),
(255, 'Agua en botellas.', '', 10),
(256, 'Santa Teresa Cocalmayo', '( USD 10.00) ( Disponible Como Extra).', 10),
(257, 'Zip Line (Tiroleza)', ' ( USD 30,00 )(  Disponible Como Extra).', 10),
(258, 'Rafting (Canotaje)', '( USD 30,00 )(  Disponible Como Extra).', 10),
(259, 'Bus de subida a Machu Picchu', ' ( USD 12,00 )(  Disponible Como Extra).', 10),
(260, 'Almuerzo y cena del último día.', '', 11),
(261, 'Aperitivos y guarniciones.', '', 11),
(262, 'Aguas termales en Santa Teresa', '( USD 10,00) ( Disponible Como extra).', 11),
(263, 'Agua en botellas.', '', 11),
(264, 'Consejos para guía', '', 11),
(265, 'Rafting (Canotaje )', ' ( USD 30,00) (  Disponible Como extra).\r\n', 11),
(266, 'Zip Line (Tiroleza )', ' ( USD 30,00) (  Disponible Como extra).', 11),
(267, 'Bus a Machupicchu', '( USD 12,00 )(  Disponible Como extra).', 11),
(268, 'Caballos.', '( USD 30,00 )( Disponible Como extra)', 12),
(269, 'Cena', '', 12),
(270, 'Productos de higiene personal', '', 12),
(271, 'Caballos', '( USD 30,00 ) ( Disponible Como extra).', 13),
(272, 'Cena', '', 13),
(273, 'Productos de higiene personal', '', 13),
(274, 'Ingreso a los baños termales en Aguas Calientes.', '', 14),
(275, 'Boleto de bus adicional', '', 14),
(276, 'El primer desayuno y la última comida.', '', 14),
(277, 'Productos de higiene personal', '', 14),
(278, 'Montaña de Machu Picchu (adicional)', ' ( USD 20,00 ) ( Disponible Como extra)', 14),
(279, 'Tren Vistadome / 360º ', ' (de Aguas Calientes a Ollantaytambo, primera clase) (adicional) ( USD 30,00 ) ( Disponible Como extra)', 14),
(280, 'Bus a Machupicchu', '( USD 12,00 )(  Disponible Como extra).', 16),
(281, 'Desayuno el primer día.', '', 16),
(282, 'Almuerzo el último día.', '', 16),
(283, 'Aperitivos y guarniciones.', '', 16),
(284, 'Aguas termales en Santa Teresa', ' ( USD 10,00) ( Disponible Como extra).', 16),
(285, 'Agua en botellas.', '', 16),
(286, 'Bolsa de Dormir', ' ( USD 20,00) ( Disponible Como extra)', 16),
(287, 'Machu Picchu Montaña', '( USD 20,00) ( Disponible Como extra)', 16),
(288, 'Tren De Hidroelectrica a Aguas Calientes', ' ( USD 40,00) ( Disponible Como extra)', 16),
(289, 'Tren De Primera Clase A Ollantaytambo', ' ( USD 30,00) ( Disponible Como extra)', 16),
(290, 'Desayuno el primer día.', '', 17),
(291, 'Almuerzo el último día.', '', 17),
(292, 'Aperitivos y guarniciones.', '', 17),
(293, 'Agua en botellas.', '', 17),
(294, 'Bolsa de Dormir ', '( USD 20,00 )(  Disponible Como extra)', 17),
(295, 'Bus de subida a Machu picchu', '( USD 12,00 )(  Disponible Como extra).', 17),
(296, 'Aguas termales en Santa Teresa', '( USD 10,00) ( Disponible Como extra).', 17),
(297, 'Tren de Hidroelectrica a Aguas Calientes', '( USD 40,00) ( Disponible Como extra).', 17),
(298, 'Machu Picchu Montaña ', '( USD 20,00) ( Disponible Como extra).', 17),
(299, 'Tren de Primera Clase a Ollantaytambo', '( USD 30,00) ( Disponible Como extra).', 17),
(300, 'Gratuities', '<p>Tips are at everyone\'s will.</p>', 19),
(301, 'Breakfast, Dinner', '<p>Breakfast;Dinner</p>', 19),
(304, 'Foods', '', 20),
(305, 'Personal expenses', '', 21),
(306, 'The Breakfast of the first day', '', 21),
(307, 'Lunch, last day dinner', '', 21),
(308, 'Personal hygiene products', '', 21),
(309, 'Bus of Rise ', '( USD 12,00 Aguas Calientes - Machu Picchu Santuary) ( Available as extra )', 21),
(310, 'Machu Picchu Mountain', '( USD 20,00 ) ( Available as extra )', 21),
(311, 'Huayna Picchu ', '( USD 20,00 ) ( Available as extra )', 21),
(312, 'Food', '', 22),
(313, 'Expenses mentioned in the Itinerary', '', 22),
(314, 'Personal hygiene products', '', 22),
(315, 'Mountain entrance Huayna Picchu', ' ( USD 20,00) ( Available As extra )', 22),
(316, 'Mountain entrance Machupicchu', ' ( USD 20,00) ( Available As extra)', 22),
(317, 'Propins', '', 24),
(318, 'Breakfast , Dinner of the first day', '', 24),
(319, 'Lunch and dinner of the last day', '', 24),
(320, 'Other services not mentioned', '', 24),
(321, 'Up Ascent or Descent Bus ', '(Aguas Calientes - Santuario de Machu Picchu 24,00 USD) (  Available As extra)', 24),
(322, 'Personal accident insurance', '', 25),
(323, 'Tips', '', 25),
(324, 'Foods .', '', 25),
(325, 'Income to the places to visit in case you do not have the tourist ticket (you will have to pay it in the same place):', '<p>Moray (+PEN 70 ), (+PEN 40) for Peruvians; if they have the tourist ticket they do not have to pay for the entrance.; Maras (+PEN 15), (+PEN 10) for Peruvians</p>', 25),
(326, 'Breakfast the first day.', '', 27),
(327, 'Lunch on the last day.', '', 27),
(328, 'Appetizers and garnishes.', '', 27),
(329, 'Water in bottles.', '', 27),
(330, 'Santa Teresa Thermal Waters', 'Santa Teresa Thermal Waters ( USD 10.00) ( Available As extra).\r\n', 27),
(331, 'Zip Line Activitie', ' ( USD 30,00 )(  Available As extra).', 27),
(332, 'Rafting Activitie', ' ( USD 30,00 )(  Available As extra).', 27),
(333, 'Bus to Machu picchu', ' ( USD 12,00 )(  Available As extra).', 27),
(334, 'Lunch and dinner of the last day.', '', 28),
(335, 'Aperitifs and garnishes.', '', 28),
(336, 'Water in bottles.', '', 28),
(337, 'Guide tips', '', 28),
(338, 'Santa Teresa Thermal Waters', '<p>( USD 10.00) ( Available As extra).</p>', 28),
(339, 'Zip Line Activitie', '<p>( USD 30,00 )( Available As extra).</p>', 28),
(340, 'Rafting Activitie', '<p>( USD 30,00 )( Available As extra).</p>', 28),
(341, 'Bus to Machu picchu ', '<p>( USD 12,00 )( Available As extra).</p>', 28),
(342, 'Horses ', '.( USD 30,00 )( Available as extra )', 29),
(343, 'Dinner.', '', 29),
(344, 'Personal hygiene products', '', 29),
(345, 'Horses', '( USD 30,00 ) ( Available As extra).', 30),
(346, 'Dinner.', '', 30),
(347, 'Personal hygiene products', '', 30),
(348, 'Admission to the thermal baths in Aguas Calientes.', '', 31),
(349, 'Additional bus ticket', '', 31),
(350, 'The first breakfast and the last meal.', '', 31),
(351, 'personal hygiene products', '', 31),
(352, 'Mountain of Machu Picchu (additional) ( USD 20,00 ) ( Available As an extra )', '( USD 20,00 ) ( Available As an extra )', 31),
(353, 'Vistadome / 360º train ', '(from Aguas Calientes to Ollantaytambo, first class) (additional) ( USD 30,00 ) ( Available as an extra)', 31),
(354, 'Bus up to Machupicchu', ' ( USD 12,00 )(  Available As extra).', 33),
(355, 'Breakfast on the first day.', '', 33),
(356, 'Lunch on the last day.', '', 33),
(357, 'Aperitifs and garnishes.', '', 33),
(358, 'Water in bottles.', '', 33),
(359, 'Sleeping bag', '', 33),
(360, 'Santa Teresa Thermal Waters ', '( USD 10.00) ( Available As extra).', 33),
(361, 'Sleeping Bag ', '( USD 20.00) ( Available As extra).', 33),
(362, 'Machu Picchu Mountain ', '( USD 20.00) ( Available As extra).', 33),
(363, 'Cocalmayo Hot Spring', ' ( USD 10.00) ( Available As extra).', 33),
(364, 'Train From Hydro Electric to Aguas Calientes', ' ( USD 40.00) ( Available As extra).', 33),
(365, 'Fist Class Train To Ollantaytambo', '( USD 30.00) ( Available As extra).', 33),
(366, 'Bus up to Machupicchu ', '( USD 12,00 )(  Available As extra).', 34),
(367, 'Breakfast on the first day.', '', 34),
(368, 'Lunch on the last day.', '', 34),
(369, 'Aperitifs and garnishes.', '', 34),
(370, 'Water in bottles.', '', 34),
(371, 'Sleeping bag', '', 34),
(372, 'Santa Teresa Thermal Waters', ' ( USD 10.00) ( Available As extra).', 34),
(373, 'Sleeping Bag', ' ( USD 20.00) ( Available As extra).', 34),
(374, 'Machu Picchu Mountain ', '( USD 20.00) ( Available As extra).', 34),
(375, 'Cocalmayo Hot Spring', ' ( USD 10.00) ( Available As extra).', 34),
(376, 'Train From Hydro Electric to Aguas Calientes', ' ( USD 40.00) ( Available As extra).', 34),
(377, 'Fist Class Train To Ollantaytambo', '( USD 30.00) ( Available As extra).', 34),
(378, 'Food', '', 23),
(379, 'Expenses mentioned in the Itinerary', '', 23),
(380, 'Personal hygiene products', '', 23),
(381, 'Mountain entrance Huayna Picchu', '<p>( USD 20,00) ( Available As extra )</p>', 23),
(382, 'Mountain entrance Machupicchu', '<p>( USD 20,00) ( Available As extra)</p>', 23),
(383, ' Feeding:', '<p>Personal Lunch Box</p>', 26),
(384, ' Feeding:', '<p>Personal Lunch Box</p>', 26),
(385, 'Hot water lunch on the second day', '', 32),
(386, 'Alimentacion excepto lo mesionado', '', 9),
(387, 'Seguro de viajes', '', 9),
(388, 'Vuelos aereos', '', 9),
(389, 'Hotel en cusco', '', 9),
(390, 'Ultimo almuerzo despues de la visita a machupicchu', '', 15),
(391, 'Meals', 'Breakfast (You can take advantage while we visit the textile center of chinchero); Lunch; Dinner', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tourprices`
--

CREATE TABLE `tourprices` (
  `IdTourPrice` int(11) NOT NULL,
  `PriceMount` float(16,2) NOT NULL,
  `IdTour` int(11) NOT NULL,
  `IdPriceCategory` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tourprices`
--

INSERT INTO `tourprices` (`IdTourPrice`, `PriceMount`, `IdTour`, `IdPriceCategory`) VALUES
(1, 25.00, 1, 1),
(2, 70.00, 2, 2),
(4, 170.00, 4, 1),
(5, 320.00, 5, 2),
(6, 340.00, 6, 3),
(7, 320.00, 7, 1),
(8, 50.00, 8, 2),
(9, 40.00, 9, 3),
(10, 345.00, 10, 1),
(11, 170.00, 4, 2),
(12, 170.00, 4, 3),
(13, 50.00, 8, 1),
(14, 330.00, 8, 2),
(16, 40.00, 9, 1),
(17, 40.00, 9, 2),
(18, 110.00, 9, 3),
(19, 340.00, 6, 1),
(20, 340.00, 6, 2),
(21, 330.00, 6, 3),
(22, 330.00, 7, 1),
(23, 330.00, 7, 2),
(24, 320.00, 7, 3),
(25, 40.00, 12, 1),
(26, 40.00, 12, 2),
(27, 40.00, 12, 3),
(28, 40.00, 13, 1),
(29, 40.00, 13, 2),
(30, 40.00, 13, 3),
(31, 330.00, 14, 1),
(32, 330.00, 14, 2),
(33, 330.00, 14, 3),
(34, 680.00, 15, 1),
(35, 680.00, 15, 2),
(36, 680.00, 15, 3),
(37, 330.00, 16, 1),
(38, 330.00, 16, 2),
(39, 320.00, 16, 3),
(40, 330.00, 17, 1),
(41, 330.00, 17, 2),
(42, 330.00, 17, 3),
(43, 25.00, 18, 6),
(44, 25.00, 18, 5),
(45, 25.00, 18, 4),
(46, 70.00, 19, 6),
(48, 70.00, 19, 4),
(49, 50.00, 25, 6),
(50, 50.00, 25, 5),
(51, 50.00, 25, 4),
(52, 40.00, 26, 6),
(53, 40.00, 26, 5),
(54, 40.00, 26, 4),
(55, 330.00, 27, 6),
(56, 330.00, 27, 5),
(57, 330.00, 27, 4),
(58, 330.00, 34, 6),
(59, 330.00, 34, 5),
(60, 330.00, 34, 4),
(61, 20.00, 20, 6),
(62, 20.00, 20, 5),
(63, 20.00, 20, 4),
(64, 170.00, 21, 6),
(65, 170.00, 21, 5),
(66, 170.00, 21, 4),
(67, 290.00, 28, 6),
(68, 290.00, 28, 5),
(69, 290.00, 28, 4),
(73, 320.00, 5, 1),
(74, 320.00, 5, 2),
(75, 320.00, 5, 3),
(76, 310.00, 10, 1),
(77, 340.00, 10, 2),
(78, 340.00, 10, 3),
(79, 310.00, 10, 1),
(80, 310.00, 10, 2),
(81, 310.00, 10, 3),
(82, 290.00, 11, 1),
(83, 290.00, 11, 2),
(84, 290.00, 11, 3),
(85, 320.00, 22, 6),
(86, 320.00, 22, 5),
(87, 310.00, 22, 4),
(88, 325.00, 23, 6),
(89, 325.00, 23, 5),
(90, 325.00, 23, 4),
(91, 320.00, 24, 6),
(92, 320.00, 24, 5),
(93, 320.00, 24, 4),
(94, 40.00, 29, 6),
(95, 40.00, 29, 5),
(96, 40.00, 29, 4),
(97, 40.00, 30, 6),
(98, 40.00, 30, 5),
(99, 40.00, 30, 4),
(100, 330.00, 31, 6),
(101, 330.00, 31, 5),
(102, 330.00, 31, 4),
(103, 680.00, 32, 6),
(104, 680.00, 32, 5),
(105, 680.00, 32, 4),
(106, 330.00, 33, 6),
(107, 330.00, 33, 5),
(108, 330.00, 33, 4),
(109, 70.00, 19, 5),
(110, 70.00, 2, 1),
(111, 70.00, 2, 3),
(112, 20.00, 3, 1),
(113, 20.00, 3, 2),
(114, 20.00, 3, 3),
(115, 50.00, 8, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tour_has_extra`
--

CREATE TABLE `tour_has_extra` (
  `IdTour` int(11) NOT NULL,
  `IdExtra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tour_has_extra`
--

INSERT INTO `tour_has_extra` (`IdTour`, `IdExtra`) VALUES
(1, 7),
(1, 8),
(2, 7),
(2, 8),
(3, 7),
(3, 8),
(4, 1),
(4, 2),
(4, 4),
(4, 5),
(4, 6),
(4, 8),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 8),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 8),
(6, 17),
(6, 18),
(6, 19),
(6, 20),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 8),
(7, 17),
(7, 18),
(7, 19),
(7, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `travelersbooking`
--

CREATE TABLE `travelersbooking` (
  `IdTraveler` varchar(255) NOT NULL,
  `NameTraveler` varchar(50) NOT NULL,
  `LastNameTraveler` varchar(50) NOT NULL,
  `AgeTraveler` int(11) NOT NULL,
  `CountryTraveler` varchar(50) NOT NULL,
  `PassaportTraveler` varchar(50) NOT NULL,
  `IdBooking` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `travelersbooking`
--

INSERT INTO `travelersbooking` (`IdTraveler`, `NameTraveler`, `LastNameTraveler`, `AgeTraveler`, `CountryTraveler`, `PassaportTraveler`, `IdBooking`, `updated_at`, `created_at`) VALUES
('TV-1K5D5S75', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-22A2F7L9', 'asdasd', 'asd', 54, 'asdasd', 'asdas', 'TP-DZDXCYKT', '2019-11-03 19:56:31', '2019-11-03 19:56:31'),
('TV-26U1X61D', 'sad', 'fdg', 34, 'fdg', 'fgd', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-2AQ1D9CO', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-2HE49CDR', 'fdg', 'dfg', 34, 'sad', 'fdg', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-31DH9O79', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-3X52IQ61', 'sdf', 'dsf', 34, 'sad', 'dsf', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-48G449NN', 'Lizandro', 'Conde Mescco', 18, 'Perú', 'asdsa', 'TP-VUAVB9UO', '2019-11-03 18:42:17', '2019-11-03 18:42:17'),
('TV-4G7Y11QL', 'asdas', 'sadsadas', 45, 'sadasd', 'sadsad', 'TP-4RRYDQH3', '2019-11-03 19:57:36', '2019-11-03 19:57:36'),
('TV-4ILVYQMV', 'asdas', 'sadas', 34, 'asfdas', 'sadasdf', 'TP-VUAVB9UO', '2019-11-03 18:42:17', '2019-11-03 18:42:17'),
('TV-6K3BLA88', 'klñkjljk', 'jklljkljkl', 56, 'jklklj', 'jkjkl', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-6OF69ENX', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-9873AI5S', 'sad', 'sad', 23, 'sad', 'sad', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-9CYXQSFI', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-9T4K3XWS', 'sadasd', 'sadasd', 43, 'asdsa', 'dsad', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-AE9IXVZO', 'sadsa', 'asd', 23, 'dasd', 'asdasd', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-AP3XELBJ', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-B7AWQ6P4', 'asdas', 'asd', 54, 'sad', 'sad', 'TP-F8G4KJCS', '2019-11-03 19:46:19', '2019-11-03 19:46:19'),
('TV-BTHMZ9NJ', 'asdasd', 'asdsad', 32, 'sad', 'sadas', 'TP-VUAVB9UO', '2019-11-03 18:42:17', '2019-11-03 18:42:17'),
('TV-C2NWSC24', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-CHEQP6M7', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-CNF1LSSZ', 'sadkjjkl', 'klñkjk', 88, 'klklñ', 'kojñkñ', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-GG84JARV', 'sadasd', 'asdasd', 23, 'asd', 'sada', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-I9D7JHYU', 'asd', 'sad', 23, 'sadsa', 'das', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-IHEIMIGS', 'Lizandro', 'Conde Mescco', 56, 'Perú', 'sadsad', 'TP-F8G4KJCS', '2019-11-03 19:46:19', '2019-11-03 19:46:19'),
('TV-ILI8CCT3', 'sad', 'sad', 0, 'Perú', 'fsfdsa', 'TP-DZDXCYKT', '2019-11-03 19:56:31', '2019-11-03 19:56:31'),
('TV-IN8Q1XQR', 'Lizandro', 'Conde Mescco', 45, 'Perú', 'sadsad', 'TP-3PL1YW6W', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-IRRAKZK3', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-IYC8RFBM', 'asdas', 'asdasd', 45, 'Perú', 'sadasd', 'TP-4RRYDQH3', '2019-11-03 19:57:36', '2019-11-03 19:57:36'),
('TV-IZDFUXSX', 'sadsad', 'sadsa', 23, 'asd', 'asdas', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-JXC7TLI1', 'sadasd', 'sadasd', 56, 'sasad', 'sadasd', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-KK3M9MOH', 'kpokok', 'klñlñkklñ', 89, 'sdfsd', 'mjkjk', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-KU4H6BXD', 'juan', 'peres', 15, 'asd', 'asd', 'TP-3PL1YW6W', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-L389LKST', 'saas', 'asd', 23, 'asd', 'sad', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-L9SKEAMB', 'asd', 'lkk', 3, 'asdasd', 'kadwdas', 'TP-4ERSZ2T2', '2019-11-03 20:10:08', '2019-11-03 20:10:08'),
('TV-LHJ5ADFA', 'sad', 'asdsa', 3, 'asd', 'sad', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-LJ977DPQ', 'fdg', 'dfgdf', 34, 'asd', 'asdgdf', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-MTZBXIYV', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-QXBU6KMD', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-TMVCPVHQ', '', '', 0, '', '', 'TP-G5IV8FF4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('TV-VA3664C6', 'Lizandro', 'Conde Mescco', 15, 'Perú', 'asdsad', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-X7IBUO2D', 'sadas', 'asdasd', 23, 'asd', 'sadsa', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-XTGXX2O4', 'sadasd', 'asdas', 32, 'asdsa', 'sadasd', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12'),
('TV-YMFPBFU8', 'wqeqw', 'pkk4', 44, 'dsfs', 'dsasfas', 'TP-4ERSZ2T2', '2019-11-03 20:10:08', '2019-11-03 20:10:08'),
('TV-Z1DSMI4G', 'Lizandro', 'Conde Mescco', 23, 'Perú', 'asd', 'TP-4ERSZ2T2', '2019-11-03 20:10:08', '2019-11-03 20:10:08'),
('TV-Z48QKSE1', 'asd', 'asd', 23, 'sad', 'asd', 'TP-1C6S8R6M', '2019-11-03 18:47:12', '2019-11-03 18:47:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_usuario`
--

CREATE TABLE `t_usuario` (
  `IdUsuario` int(11) NOT NULL,
  `Nombre` text DEFAULT NULL,
  `Apellidos` text DEFAULT NULL,
  `Gmail` text DEFAULT NULL,
  `Contrasena` text DEFAULT NULL,
  `Foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_usuario`
--

INSERT INTO `t_usuario` (`IdUsuario`, `Nombre`, `Apellidos`, `Gmail`, `Contrasena`, `Foto`) VALUES
(23, 'Napoleon', 'Arcondo Saico', 'napoleonarcondosaico@gmail.com', '$2y$10$dv40sCFQQdr.gd7OdidVvekCXmZ86DPEhAUvDiA6TM9L3C7/PiTi2', 'https://i.postimg.cc/t4PvCPm4/napoleon-typicaltrips.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `associatons`
--
ALTER TABLE `associatons`
  ADD PRIMARY KEY (`IdAssociation`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`IdBooking`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`IdCategory`),
  ADD KEY `IdLang` (`IdLang`);

--
-- Indices de la tabla `extra`
--
ALTER TABLE `extra`
  ADD PRIMARY KEY (`IdExtra`),
  ADD KEY `IdLang` (`IdLang`);

--
-- Indices de la tabla `galeryimage`
--
ALTER TABLE `galeryimage`
  ADD PRIMARY KEY (`IdGaleryImage`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `hightext`
--
ALTER TABLE `hightext`
  ADD PRIMARY KEY (`IdHight`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `itinerary`
--
ALTER TABLE `itinerary`
  ADD PRIMARY KEY (`IdItinerary`,`IdTour`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `itineraryday`
--
ALTER TABLE `itineraryday`
  ADD PRIMARY KEY (`IdItineraryDay`),
  ADD KEY `IdItinerary` (`IdItinerary`);

--
-- Indices de la tabla `itinerarydescription`
--
ALTER TABLE `itinerarydescription`
  ADD PRIMARY KEY (`IdItinerarioDescription`),
  ADD KEY `IdItineraryDay` (`IdItineraryDay`);

--
-- Indices de la tabla `lang`
--
ALTER TABLE `lang`
  ADD PRIMARY KEY (`IdLang`);

--
-- Indices de la tabla `pg_sliderhome`
--
ALTER TABLE `pg_sliderhome`
  ADD PRIMARY KEY (`IdSliderHome`),
  ADD KEY `IdLang` (`IdLang`);

--
-- Indices de la tabla `pricecategory`
--
ALTER TABLE `pricecategory`
  ADD PRIMARY KEY (`IdPriceCategory`),
  ADD KEY `IdLang` (`IdLang`);

--
-- Indices de la tabla `recommendations`
--
ALTER TABLE `recommendations`
  ADD PRIMARY KEY (`IdRecommendation`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `recommendationslists`
--
ALTER TABLE `recommendationslists`
  ADD PRIMARY KEY (`IdRecommendationList`),
  ADD KEY `IdLang` (`IdLang`);

--
-- Indices de la tabla `recommendationslist_has_tour`
--
ALTER TABLE `recommendationslist_has_tour`
  ADD PRIMARY KEY (`IdRecommendationList`,`IdTour`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`IdTour`),
  ADD UNIQUE KEY `IdTour` (`IdTour`),
  ADD KEY `IdCategory` (`IdCategory`);

--
-- Indices de la tabla `tourbookings`
--
ALTER TABLE `tourbookings`
  ADD PRIMARY KEY (`IdTourBooking`),
  ADD KEY `IdBooking` (`IdBooking`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `tourincluded`
--
ALTER TABLE `tourincluded`
  ADD PRIMARY KEY (`IdTourIncluded`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `tournoincluded`
--
ALTER TABLE `tournoincluded`
  ADD PRIMARY KEY (`IdTourNoIncluded`),
  ADD KEY `IdTour` (`IdTour`);

--
-- Indices de la tabla `tour_has_extra`
--
ALTER TABLE `tour_has_extra`
  ADD PRIMARY KEY (`IdTour`,`IdExtra`),
  ADD KEY `IdExtra` (`IdExtra`);

--
-- Indices de la tabla `travelersbooking`
--
ALTER TABLE `travelersbooking`
  ADD PRIMARY KEY (`IdTraveler`),
  ADD KEY `IdBooking` (`IdBooking`);

--
-- Indices de la tabla `t_usuario`
--
ALTER TABLE `t_usuario`
  ADD PRIMARY KEY (`IdUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `associatons`
--
ALTER TABLE `associatons`
  MODIFY `IdAssociation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `IdCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `extra`
--
ALTER TABLE `extra`
  MODIFY `IdExtra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `galeryimage`
--
ALTER TABLE `galeryimage`
  MODIFY `IdGaleryImage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=278;

--
-- AUTO_INCREMENT de la tabla `hightext`
--
ALTER TABLE `hightext`
  MODIFY `IdHight` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `itinerary`
--
ALTER TABLE `itinerary`
  MODIFY `IdItinerary` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `itineraryday`
--
ALTER TABLE `itineraryday`
  MODIFY `IdItineraryDay` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT de la tabla `itinerarydescription`
--
ALTER TABLE `itinerarydescription`
  MODIFY `IdItinerarioDescription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT de la tabla `lang`
--
ALTER TABLE `lang`
  MODIFY `IdLang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pg_sliderhome`
--
ALTER TABLE `pg_sliderhome`
  MODIFY `IdSliderHome` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pricecategory`
--
ALTER TABLE `pricecategory`
  MODIFY `IdPriceCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `recommendations`
--
ALTER TABLE `recommendations`
  MODIFY `IdRecommendation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `recommendationslists`
--
ALTER TABLE `recommendationslists`
  MODIFY `IdRecommendationList` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT de la tabla `tour`
--
ALTER TABLE `tour`
  MODIFY `IdTour` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `tourincluded`
--
ALTER TABLE `tourincluded`
  MODIFY `IdTourIncluded` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT de la tabla `tournoincluded`
--
ALTER TABLE `tournoincluded`
  MODIFY `IdTourNoIncluded` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;

--
-- AUTO_INCREMENT de la tabla `t_usuario`
--
ALTER TABLE `t_usuario`
  MODIFY `IdUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `associatons`
--
ALTER TABLE `associatons`
  ADD CONSTRAINT `associatons_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`IdLang`) REFERENCES `lang` (`IdLang`);

--
-- Filtros para la tabla `extra`
--
ALTER TABLE `extra`
  ADD CONSTRAINT `extra_ibfk_1` FOREIGN KEY (`IdLang`) REFERENCES `lang` (`IdLang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `galeryimage`
--
ALTER TABLE `galeryimage`
  ADD CONSTRAINT `galeryimage_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `hightext`
--
ALTER TABLE `hightext`
  ADD CONSTRAINT `hightext_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `itinerary`
--
ALTER TABLE `itinerary`
  ADD CONSTRAINT `itinerary_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `itineraryday`
--
ALTER TABLE `itineraryday`
  ADD CONSTRAINT `itineraryday_ibfk_1` FOREIGN KEY (`IdItinerary`) REFERENCES `itinerary` (`IdItinerary`);

--
-- Filtros para la tabla `itinerarydescription`
--
ALTER TABLE `itinerarydescription`
  ADD CONSTRAINT `itinerarydescription_ibfk_1` FOREIGN KEY (`IdItineraryDay`) REFERENCES `itineraryday` (`IdItineraryDay`);

--
-- Filtros para la tabla `pg_sliderhome`
--
ALTER TABLE `pg_sliderhome`
  ADD CONSTRAINT `pg_sliderhome_ibfk_1` FOREIGN KEY (`IdLang`) REFERENCES `lang` (`IdLang`);

--
-- Filtros para la tabla `pricecategory`
--
ALTER TABLE `pricecategory`
  ADD CONSTRAINT `pricecategory_ibfk_1` FOREIGN KEY (`IdLang`) REFERENCES `lang` (`IdLang`);

--
-- Filtros para la tabla `recommendations`
--
ALTER TABLE `recommendations`
  ADD CONSTRAINT `recommendations_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recommendationslists`
--
ALTER TABLE `recommendationslists`
  ADD CONSTRAINT `recommendationslists_ibfk_1` FOREIGN KEY (`IdLang`) REFERENCES `lang` (`IdLang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recommendationslist_has_tour`
--
ALTER TABLE `recommendationslist_has_tour`
  ADD CONSTRAINT `recommendationslist_has_tour_ibfk_1` FOREIGN KEY (`IdRecommendationList`) REFERENCES `recommendationslists` (`IdRecommendationList`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `recommendationslist_has_tour_ibfk_2` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tour`
--
ALTER TABLE `tour`
  ADD CONSTRAINT `tour_ibfk_1` FOREIGN KEY (`IdCategory`) REFERENCES `category` (`IdCategory`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tourbookings`
--
ALTER TABLE `tourbookings`
  ADD CONSTRAINT `TourBookings_ibfk_1` FOREIGN KEY (`IdBooking`) REFERENCES `bookings` (`IdBooking`),
  ADD CONSTRAINT `TourBookings_ibfk_2` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `tourincluded`
--
ALTER TABLE `tourincluded`
  ADD CONSTRAINT `tourincluded_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `tournoincluded`
--
ALTER TABLE `tournoincluded`
  ADD CONSTRAINT `tournoincluded_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`);

--
-- Filtros para la tabla `tour_has_extra`
--
ALTER TABLE `tour_has_extra`
  ADD CONSTRAINT `tour_has_extra_ibfk_1` FOREIGN KEY (`IdTour`) REFERENCES `tour` (`IdTour`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tour_has_extra_ibfk_2` FOREIGN KEY (`IdExtra`) REFERENCES `extra` (`IdExtra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `travelersbooking`
--
ALTER TABLE `travelersbooking`
  ADD CONSTRAINT `TravelersBooking_ibfk_1` FOREIGN KEY (`IdBooking`) REFERENCES `bookings` (`IdBooking`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
